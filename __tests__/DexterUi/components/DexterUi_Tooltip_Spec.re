open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Renders", () => {
    render(
      <DexterUi_Tooltip
        text={"Snapshot text" |> React.string} width={`px(100)}>
        {"Snapshot" |> React.string}
      </DexterUi_Tooltip>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  })
});
