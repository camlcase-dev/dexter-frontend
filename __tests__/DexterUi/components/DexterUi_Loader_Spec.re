open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Default", () => {
    render(<DexterUi_Loader />) |> container |> expect |> toMatchSnapshot
  });
  test("Red; Size 24px", () => {
    render(<DexterUi_Loader loaderColor=Colors.red loaderSize=24 />)
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
