open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Renders", () => {
    render(<DexterUi_Rate> {"Snapshot" |> React.string} </DexterUi_Rate>)
    |> container
    |> expect
    |> toMatchSnapshot
  })
});
