open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Renders", () => {
    render(<DexterUi_MagmaLink />) |> container |> expect |> toMatchSnapshot
  })
});
