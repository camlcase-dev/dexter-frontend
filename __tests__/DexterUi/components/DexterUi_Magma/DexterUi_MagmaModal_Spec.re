open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Renders", () => {
    render(<DexterUi_MagmaModal onClose={() => ()} />)
    |> container
    |> expect
    |> toMatchSnapshot
  })
});
