open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Renders", () => {
    render(
      <DexterUi_TokenSearch
        balances=Fixtures_Balances.balances1
        hideTokenSearch={_ => ()}
        side=Side.Left
        onTokenChange={_ => ()}
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  })
});
