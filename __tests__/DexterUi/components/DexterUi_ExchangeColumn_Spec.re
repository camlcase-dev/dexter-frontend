open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("MTZ; No tokenSearchBalances;", () => {
    render(
      <DexterUi_ExchangeColumn
        balance=Fixtures_Balances.mtzBalance
        balanceInfo=["Snapshot balanceInfo 1", "Snapshot balanceInfo 2"]
        title={"Snapshot title" |> React.string}>
        {"Snapshot" |> React.string}
      </DexterUi_ExchangeColumn>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("XTZ/MTZ; No tokenSearchBalances; No title;", () => {
    render(
      <DexterUi_ExchangeColumn
        balance=Fixtures_Balances.mtzBalance
        balanceInfo=["Snapshot balanceInfo 1", "Snapshot balanceInfo 2"]
        isPoolToken=true>
        {"Snapshot" |> React.string}
      </DexterUi_ExchangeColumn>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("XTZ/TZG; With tokenSearchBalances;", () => {
    render(
      <DexterUi_ExchangeColumn
        balance=Fixtures_Balances.tzgBalance
        balanceInfo=["Snapshot balanceInfo 1", "Snapshot balanceInfo 2"]
        title={"Snapshot title" |> React.string}
        tokenSearchBalances=Fixtures_Balances.balances1>
        {"Snapshot" |> React.string}
      </DexterUi_ExchangeColumn>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
