open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Light mode;", () => {
    render(
      <Helpers_Context>
        <DexterUi_AddressInput
          setValue={_ => ()}
          value="tz1fsLQFv5YV1JKxKbTY9d4YFVjSVVV35HgG"
        />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Dark mode; Invalid address", () => {
    render(
      <Helpers_Context darkMode=true>
        <DexterUi_AddressInput setValue={_ => ()} value="tz1fsLQFv5YV1JKx" />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Light mode; Disabled;", () => {
    render(
      <Helpers_Context>
        <DexterUi_AddressInput setValue={_ => ()} value="" isDisabled=true />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Dark mode; With usd value; Invalid by prop;", () => {
    render(
      <Helpers_Context darkMode=true>
        <DexterUi_AddressInput
          setValue={_ => ()}
          value="tz1fsLQFv5YV1JKxKbTY9d4YFVjSVVV35HgG"
          isValid=false
        />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
