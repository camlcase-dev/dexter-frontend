open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Light mode", () => {
    render(
      <Helpers_Context balances=Fixtures_Balances.balances1>
        <DexterUi_Layout />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Dark mode", () => {
    render(
      <Helpers_Context balances=Fixtures_Balances.balances1 darkMode=true>
        <DexterUi_Layout />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
