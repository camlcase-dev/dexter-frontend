open Jest;
open Expect;
open DexterUi_ExchangeControls_Utils;

let account = Some(Fixtures_Account.account1);
let xtzInput: Valid.t(InputType.t) = Valid(Mutez(Tezos.Mutez.oneTez), "");
let xtz: Dexter_Balance.t = Fixtures_Balances.xtzOneTezBalance;
let tokenInput: Valid.t(InputType.t) = Valid(Token(Tezos.Token.one), "");
let token: Dexter_Balance.t =
  Dexter_Balance.ExchangeBalance({
    name: "",
    symbol: "",
    iconDark: "",
    iconLight: "",
    decimals: 0,
    tokenContract: Tezos.Contract.Contract(""),
    dexterContract: Tezos.Contract.Contract(""),
    dexterBaker: None,
    tokenBalance: Tezos.Token.one,
    lqtBalance: Tezos.Token.one,
    exchangeTotalLqt: Tezos.Token.one,
    exchangeXtz: Tezos.Mutez.oneTez,
    exchangeTokenBalance: Tezos.Token.one,
    exchangeAllowanceForAccount: Tezos.Token.zero,
  });

describe("checkInputsInvalid", () => {
  test("xtz to token and values are less than limits", () => {
    expect(checkInputsInvalid(account, xtzInput, xtz, tokenInput, token))
    |> toEqual(false)
  });

  test("xtz to token and is xtz is greater than limit", () => {
    let xtzInput: Valid.t(InputType.t) =
      Valid(Mutez(Tezos.Mutez.Mutez(200000000L)), "");

    expect(checkInputsInvalid(account, xtzInput, xtz, tokenInput, token))
    |> toEqual(true);
  });

  test("xtz to token and is token is greater than limit", () => {
    let xtzInput: Valid.t(InputType.t) =
      Valid(
        Token(Tezos.Token.mkToken(Bigint.of_int64(20000000000L), 0)),
        "",
      );

    expect(checkInputsInvalid(account, xtzInput, xtz, tokenInput, token))
    |> toEqual(true);
  });

  test("xtz to token but left input is zero", () => {
    let xtzInput: Valid.t(InputType.t) = Valid(Mutez(Tezos.Mutez.zero), "");

    expect(checkInputsInvalid(account, xtzInput, xtz, tokenInput, token))
    |> toEqual(true);
  });

  test("xtz to token but right input is zero", () => {
    let tokenInput: Valid.t(InputType.t) =
      Valid(Token(Tezos.Token.zero), "");

    expect(checkInputsInvalid(account, xtzInput, xtz, tokenInput, token))
    |> toEqual(true);
  });

  test("token to xtz and values are less than limits", () => {
    expect(checkInputsInvalid(account, tokenInput, token, xtzInput, xtz))
    |> toEqual(false)
  });

  test("token to xtz and is xtz is greater than limit", () => {
    let xtzInput: Valid.t(InputType.t) =
      Valid(Mutez(Tezos.Mutez.Mutez(200000000L)), "");

    expect(checkInputsInvalid(account, tokenInput, token, xtzInput, xtz))
    |> toEqual(true);
  });

  test("token to xtz and is token is greater than limit", () => {
    let tokenInput: Valid.t(InputType.t) =
      Valid(
        Token(Tezos.Token.mkToken(Bigint.of_int64(20000000000L), 0)),
        "",
      );

    expect(checkInputsInvalid(account, tokenInput, token, xtzInput, xtz))
    |> toEqual(true);
  });

  test("token to xtz and token is zero", () => {
    let tokenInput: Valid.t(InputType.t) = Dexter_Value.tokenZeroInputValue;

    expect(checkInputsInvalid(account, tokenInput, token, xtzInput, xtz))
    |> toEqual(true);
  });
});

// describe("checkExceedsSlippageRate", () => {
//   test("is below slippageRate", () => {
//     expect(checkExceedsSlippageRate(Some(0.5), Dexter_Slippage.One))
//     |> toEqual(false)
//   });
//   test("is above slippageRate", () => {
//     expect(checkExceedsSlippageRate(Some(2.), Dexter_Slippage.One))
//     |> toEqual(true)
//   });
//   test("is ge 50%", () => {
//     expect(
//       checkExceedsSlippageRate(
//         Some(50.),
//         Dexter_Slippage.Custom(Valid(100., "100")),
//       ),
//     )
//     |> toEqual(true)
//   });
// });
