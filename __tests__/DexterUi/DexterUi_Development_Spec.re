open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Light mode", () => {
    render(<Helpers_Context> <DexterUi_Development /> </Helpers_Context>)
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Dark mode", () => {
    render(
      <Helpers_Context darkMode=true>
        <DexterUi_Development />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
