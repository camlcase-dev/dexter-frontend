open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Loading", () => {
    render(<Helpers_Context> <DexterUi_Content /> </Helpers_Context>)
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Tezos node unavailable", () => {
    render(
      <Helpers_Context unavailableServices=[|TezosNode|]>
        <DexterUi_Content />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Exchange", () => {
    render(
      <Helpers_Context balances=Fixtures_Balances.balances1>
        <DexterUi_Content />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Liquidity Pool", () => {
    render(
      <Helpers_Context
        balances=Fixtures_Balances.balances1
        route={Dexter_Route.LiquidityPool(AddLiquidity)}>
        <DexterUi_Content />
      </Helpers_Context>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
