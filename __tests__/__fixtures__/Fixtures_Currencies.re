let currencyUSD: Currency.t = {
  decimals: 2,
  name: "United States Dollar",
  prefixed: true,
  rate: 1.92,
  sign: Some({js|$|js}),
  symbol: "USD",
};

let currencyJPY: Currency.t = {
  decimals: 2,
  name: "Japanese Yen",
  prefixed: true,
  rate: 200.98,
  sign: Some({js|¥|js}),
  symbol: "JPY",
};

let currencyBTC: Currency.t = {
  decimals: 8,
  name: "Bitcoin",
  prefixed: true,
  rate: 0.00014214,
  sign: Some({js|₿|js}),
  symbol: "BTC",
};

let currencyLKR: Currency.t = {
  decimals: 2,
  name: "Sri Lankan Rupee",
  prefixed: true,
  rate: 348.04,
  sign: Some({js|රු.|js}),
  symbol: "LKR",
};

let currencyNOK: Currency.t = {
  decimals: 2,
  name: "Norwegian Krone",
  prefixed: false,
  rate: 17.77,
  sign: Some({js| kr|js}),
  symbol: "NOK",
};

let currencyXDR: Currency.t = {
  decimals: 2,
  name: "Special Drawing Rights",
  prefixed: false,
  rate: 1.34,
  sign: None,
  symbol: "XDR",
};

let currencyBHD: Currency.t = {
  decimals: 3,
  name: "Bahraini Dinar",
  prefixed: true,
  rate: 0.711918,
  sign: Some({js|BD |js}),
  symbol: "BHD",
};

let currencies = [
  currencyUSD,
  currencyJPY,
  currencyBTC,
  currencyLKR,
  currencyNOK,
  currencyXDR,
  currencyBHD,
];
