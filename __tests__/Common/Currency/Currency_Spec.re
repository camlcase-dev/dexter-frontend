open Jest;
open Expect;

describe("toStringWithCommas", () => {
  test("USD, 1, 1.92", () =>
    expect(Currency.toStringWithCommas(Fixtures_Currencies.currencyUSD, 1.))
    |> toEqual({js|$1.92|js})
  );
  test("BTC, 10, 0.00142140", () =>
    expect(Currency.toStringWithCommas(Fixtures_Currencies.currencyBTC, 10.))
    |> toEqual({js|₿0.00142140|js})
  );
  test("NOK, 100, 1,777", () =>
    expect(
      Currency.toStringWithCommas(Fixtures_Currencies.currencyNOK, 100.),
    )
    |> toEqual({js|1,777.00 kr|js})
  );
  test("XDR, 5, 6.70", () =>
    expect(Currency.toStringWithCommas(Fixtures_Currencies.currencyXDR, 5.))
    |> toEqual({js|6.70 XDR|js})
  );
  test("BHD, 100, 71.192", () =>
    expect(
      Currency.toStringWithCommas(Fixtures_Currencies.currencyBHD, 100.),
    )
    |> toEqual({js|BD 71.192|js})
  );
});

describe("findBySymbolOrDefault", () => {
  test("BTC", () =>
    expect(
      Currency.findBySymbolOrDefault("BTC", Fixtures_Currencies.currencies),
    )
    |> toEqual(Some(Fixtures_Currencies.currencyBTC))
  );
  test("ABC", () =>
    expect(
      Currency.findBySymbolOrDefault("ABC", Fixtures_Currencies.currencies),
    )
    |> toEqual(Some(Fixtures_Currencies.currencyUSD))
  );
  test("USD; empty", () =>
    expect(Currency.findBySymbolOrDefault("USD", [])) |> toEqual(None)
  );
});
