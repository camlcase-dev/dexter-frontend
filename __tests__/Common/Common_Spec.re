open Jest;
open Expect;

describe("getLowestValueWithDecimals", () => {
  test("3, 0.001", () =>
    expect(Common.getLowestValueWithDecimals(3)) |> toEqual("0.001")
  );
  test("2, 0.01", () =>
    expect(Common.getLowestValueWithDecimals(2)) |> toEqual("0.01")
  );
  test("0, 1", () =>
    expect(Common.getLowestValueWithDecimals(0)) |> toEqual("1")
  );
});

describe("getDecimalsCount", () => {
  test("10, 0", () =>
    expect(Common.getDecimalsCount("10")) |> toEqual(0)
  );
  test("10.10, 2", () =>
    expect(Common.getDecimalsCount("10.10")) |> toEqual(2)
  );
  test("10.101010, 6", () =>
    expect(Common.getDecimalsCount("10.101010")) |> toEqual(6)
  );
  test("1.030, 3", _ =>
    expect(Common.getDecimalsCount("1.030")) |> toEqual(3)
  );
  test("1.0, 1", _ =>
    expect(Common.getDecimalsCount("1.0")) |> toEqual(1)
  );
  test("1., 0", _ =>
    expect(Common.getDecimalsCount("1.")) |> toEqual(0)
  );
  test("1.0301, 4", _ =>
    expect(Common.getDecimalsCount("1.0301")) |> toEqual(4)
  );
});

describe("removeRedundantZeros", () => {
  test("10, 10", () =>
    expect(Common.removeRedundantZeros("10")) |> toEqual("10")
  );
  test("10.10, 10.1", () =>
    expect(Common.removeRedundantZeros("10.10")) |> toEqual("10.1")
  );
  test("10.101010, 10.10101", () =>
    expect(Common.removeRedundantZeros("10.101010")) |> toEqual("10.10101")
  );
  test("1.03, 1.03", _ =>
    expect(Common.removeRedundantZeros("1.03")) |> toEqual("1.03")
  );
  test("1.0, 1", _ =>
    expect(Common.removeRedundantZeros("1.0")) |> toEqual("1")
  );
  test("1., 0", _ =>
    expect(Common.removeRedundantZeros("1.")) |> toEqual("1")
  );
});
