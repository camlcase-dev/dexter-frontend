[@react.component]
let make =
    (
      ~account: option(Dexter_Account.t)=?,
      ~balances: list(Dexter_Balance.t)=[],
      ~children: React.element,
      ~currencies: list(Currency.t)=Fixtures_Currencies.currencies,
      ~currentCurrency: option(Currency.t)=?,
      ~unavailableServices: array(Service.t)=[||],
      ~darkMode: bool=false,
      ~route: Dexter_Route.t=Dexter_Route.Exchange,
      ~transactions: option(list(Dexter_Transaction.t))=?,
      ~transactionStatus: TZKT.Status.t=TZKT.Status.Ready,
    ) =>
  <DexterUiContext.Services.ProviderInternal value=unavailableServices>
    <DexterUiContext.Theme.ProviderInternal
      value={...DexterUiContext.Theme.defaultValue, darkMode}>
      <DexterUiContext.Route.ProviderInternal
        value={...DexterUiContext.Route.defaultValue, route}>
        <DexterUiContext.Currencies.ProviderInternal
          value={
            ...DexterUiContext.Currencies.defaultValue,
            currencies,
            currentCurrency:
              currentCurrency |> Belt.Option.isSome
                ? currentCurrency
                : Currency_Dict.list
                  |> List.find_opt((currency: Currency.t) =>
                       currency.symbol === Currency.default
                     ),
          }>
          <DexterUiContext.Account.ProviderInternal
            value={...DexterUiContext.Account.defaultValue, account, balances}>
            <DexterUiContext.Transactions.ProviderInternal
              value={
                ...DexterUiContext.Transactions.defaultValue,
                transactions,
                transactionStatus,
              }>
              children
            </DexterUiContext.Transactions.ProviderInternal>
          </DexterUiContext.Account.ProviderInternal>
        </DexterUiContext.Currencies.ProviderInternal>
      </DexterUiContext.Route.ProviderInternal>
    </DexterUiContext.Theme.ProviderInternal>
  </DexterUiContext.Services.ProviderInternal>;
