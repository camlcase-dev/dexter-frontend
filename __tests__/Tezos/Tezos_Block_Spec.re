open Jest;
open Expect;

describe("Tezos_Block.decode", () => {
  test("golden", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync("__tests__/golden/block.json"),
      );
    expect(Tezos_Block.decode(json) |> Belt.Result.isOk) |> toEqual(true);
  })
});

describe("Tezos_Block.decode", () => {
  test("golden", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync("__tests__/golden/block2.json"),
      );
    expect(Tezos_Block.decode(json) |> Belt.Result.isOk) |> toEqual(true);
  })
});
