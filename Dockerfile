FROM tiangolo/node-frontend:10 as build-stage

ARG stage
ARG prod
ARG pub 
ARG maintanace
ARG sentry_dsn

ENV DEXTER_FRONTEND_PUB_DEV=$pub
ENV DEXTER_FRONTEND_STAG=$stage
ENV DEXTER_FRONTEND_PROD=$prod
ENV DEXTER_FRONTEND_MAINTENANCE_MODE=$maintanace
ENV SENTRY_DSN=$sentry_dsn

WORKDIR /app
COPY package*.json /app/
RUN yarn
COPY ./ /app/
RUN yarn build

FROM nginx:latest
COPY --from=build-stage /app/build/ /usr/share/nginx/html