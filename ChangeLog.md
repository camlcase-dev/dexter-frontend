## 2021-06-06 -- v1.4.4
* Version bump

## 2021-06-02 -- v1.4.1
* No longer maintained message.

## 2021-05-11 -- v1.4.0
* Prepare for florence.

## 2021-04-16 -- v1.3.0
* Drop indexter.
* Use baking bad API for token balances.
* Always assume 

## 2021-04-07 -- v1.2.12
* Sort tokens by xtzPool value.

## 2021-04-07 -- v1.2.9
* Bring back tokenToToken with a batch of two operations.

## 2021-04-06 -- v1.2.4
* Use bs-css-emotion version 2.2.0

## 2021-03-26 -- v1.2.3
* Change liquidity percentage display (add <0.01 case) in "My pool tokens".
* Rename sentry_dns to sentry_dsn.

## 2021-03-18 -- v1.2.2
* Fix spelling of "Tessellated Geometry".
* Change Sentry errors reporting strategy.

## 2021-03-03 -- v1.2.1
* Add @emotion/jest/serializer for snapshot tests.
* Change number of decimals for balances in pool data table.

## 2021-03-01 -- v1.2.0
* Drop support for contracts with vulneraiblities.
* Point to patched Dexter contracts (version 1.1) for tzBTC, USDtz, ETHtz, wXTZ and Kolibri.

## 2021-02-17 -- v1.1.24
* Add Kolibri to production.

## 2021-02-17 -- v1.1.23
* Add semi-functional mobile version with modal with Magma links.

## 2021-02-16 -- v1.1.22
* Drop all delphinet options.
* Drop support for TezBridge because it has not been updated for Edo.
* Fix the way networks are selected for Beacon.
* Update test urls to edo2net.

## 2021-02-05 -- v1.1.21
* Unavailable services handling.
* DexterUiContext refactoring.

## 2021-02-05 -- v1.1.20
* Update beacon-sdk to version 2.2.1-beta.0.

## 2021-02-04 -- v1.1.19
* Add support for Edo.

## 2021-02-02 -- v1.1.18
* Add SlippageLimitExceeded status.
* Change ofTezString.

## 2021-01-28 -- v1.1.17
* Add maintenance mode page.
* Unlock UI for non-connected user.

## 2021-01-28 -- v1.1.16
* Fix parameter for tokenToXtzExchangeRateJs.

## 2021-01-27 -- v1.1.15
* Fix minor bugs.

## 2021-01-12 -- v1.1.14
* Add price impact panel.

## 2021-01-11 -- v1.1.13
* Support token to token.
* Improve slippage calculation.
* Use dexter-calculations library for most calculations.

## 2021-01-11 -- v1.1.12
* Change pool token icons order.

## 2021-01-08 -- v1.1.11
* Support public and private development versions.

## 2021-01-05 -- v1.1.10
* Display dark and light tokens in the correct context.

## 2021-01-05 -- v1.1.9
* Add ETHtz token.

## 2021-01-04 -- v1.1.8
* Add Kolibri Delphinet support.

### 2020-12-30 -- v1.1.7
* Add wXTZ token.
* Add StakeNow to list of known bakers.

### 2020-12-18 -- v1.1.6
* Same currencyValue on both sides for liquidity operations
* Hide redundant decimals from currencyValue

### 2020-12-17 -- v1.1.5
* Add Dexter Stats links
* Update currencies list

### 2020-12-11 -- v1.1.4
* Update beacon-sdk to version 2.0.0.
* Remove taquito as a dependency. Handle Thanos directly with beacon sdk.
* Remove all Thanos and Taquito specific files.
* Remove Connect Wallet dialog. This is handled by beacon-sdk now.
* Connect Wallet opens the beacon sdk window.
* Add Connect via TezBridge below the Connect Wallet button.
* Fix style in Connect via TezBridge.
* Remove reference to Beacon, Thanos or TezBridge address.

### 2020-11-30 -- v1.1.3
* Add Footer.
* Update mobile landing page copy.

### 2020-11-25 -- v1.1.2
* Add knownBakers in Dexter_Settings.
* Update second paragraph of liquidity pool explainer.

### 2020-11-10 -- v1.1.1
* Update dev version for delphinet. (no version bump since it is irrelevant to production).

### 2020-11-10 -- v1.1.1
* Get transaction history from TZKT.

### 2020-11-03 -- v1.1.0
* Add local currency handling.

### 2020-10-23 -- v1.0.11
* Create a separate function to format timestamp in WalletInfo section.

### 2020-10-23 -- v1.0.10
* Update bs-platform to 8.3.1.
* Update bs-zarith to version 3.1.0 (it is compatible with the latest bs-platform
  and internally drops support for nativeint which is no longer in bs-platform).

### 2020-10-23 -- v1.0.9
* Change required XTZ reserve from 1 to 0.3

### 2020-10-22 -- v1.0.8
* Remember token selected by user
* Change the timestamp format
* Add external link icon to recent transaction row
* Add percentage info to add liquidity
* Add a fee info to exchange description
* Remove redundant zeros from decimals
* Update Liquidity Pool explainer copy

### 2020-10-21 -- v1.0.7
* Add TZKT/TZKT.re to support TZKT api operations.
* Add Dexter_TransactionHistory. It gets transcation history from TZKT api operations.
* Add __tests__/TZKT/TZKT_Spec.re.

### 2020-10-12 -- v1.0.6
* Fix history handling.
* Fix Add Liquidity Max rounding problem.

### 2020-10-06 -- v1.0.5
* Prepare project to work on IPFS.

### 2020-10-02 -- v1.0.4
* Add favicon.

### 2020-10-02 -- v1.0.3
* Add zoom property to app body.

### 2020-10-02 -- v1.0.2
* Use dummy version number in navbar.
* Move actual version number to footer.

### 2020-10-02 -- v1.0.1
* Use beacon-sdk instead of taquito for interoperating with Beacon.
* When disconnecting from Beacon, set the active account to undefined.

### 2020-09-30 -- v1.0.0
* Release to production.
* Add Sentry reporting for error in balances fetching.
* Remove disabled prop from mobile landing page "About Dexter" button.
* Show Beacon connect option back.
* Add `Please deactivate X` message on connect modal.

### 2020-09-29 -- v0.17.15
* Use slippage from state in DexterUi_ExchangeDescription.

### 2020-09-29 -- v0.17.14
* Only render DexterUi_Development in Development server.
* Update LiquidityExplainer copy
* Make About Dexter point to https://dexter.exchange

### 2020-09-29 -- v0.17.13
* Add links to dexter.exchange
* Hide footer
* Hide Beacon from connect modal

### 2020-09-29 -- v0.17.12
* Add more error catching to promises based on Sentry data.

### 2020-09-29 -- v0.17.11
* Fix documentation links.
* Add placeholder document for Thanos.
* Fix mobile links.

### 2020-09-26 -- v0.17.10
* Use different url to get balances bypassing cache after transaction succeeds.

### 2020-09-26 -- v0.17.9
* Use https for mainnet node.
* Direct staging to mainnet for taquito wallets.

### 2020-09-26 -- v0.17.8
* Fix camlcase mainnet address.
* Add more error logging when loading data from a node.
* Change baker type from Tezos_Contract to Tezos_Address.

### 2020-09-26 -- v0.17.7
* Update details for exchange to include more details about slippage and
  earned values.

### 2020-09-26 -- v0.17.6
* Add production indexter link.

### 2020-09-26 -- v0.17.5
* Add custom snapshotResolver.

### 2020-09-26 -- v0.17.4
* Add moonTZ and moonTZExchange from mainnet for staging.

### 2020-09-26 -- v0.17.3
* Use taquito for Beacon.
* Support Production, Staging and Development environments.
* Use USDtz and tzBTC contracts for testing.
* Calculate token for add liquidity using cdiv.

### 2020-09-25 -- v0.17.2
* Add insufficient xtz message on Exchange page.
* Change sent to address validation.
* Set automatic connection to Thanos on Development.
* Show only transaction of the wallet's owner.

### 2020-09-25 -- v0.17.1
* Address Trail of Bits security audit issue TOB-DEXTER-012:
  Create three types: Tezos_Contract that checks for correct KT1 addresses,
  Tezos_ImplicitAccount that checks for correct tz1, tz2 and tz3 addresses,
  and a sum type Tezos_Address that can be either a Tezos_Contract or a
  Tezos_ImplicitAccount. Use bs58check.decodeUnsafe to check if checksum is
  valid: "Decode a base58-check encoded string to a buffer, no result if
  checksum is wrong".
* Add Tezos_ImplicitAccount.
* Make Tezos_Address safe and correctly match the type from Michelson.
  It is a sum type of Tezos_ImplicitAccount and Tezos_Contract.
* Remove link to TezBridge tutorial.

### 2020-09-24 -- v0.17.0
* Add support for the Thanos wallet.

### 2020-09-23 -- 0.16.6
* Insufficient XTZ message in Add Liquidity
* Added "Connect to Thanos" option

### 2020-09-23 -- v0.16.5
* After a transaction is complete, update the amount of XTZ purchased from
  Token to Xtz (when pending it shows the minimal value purchased).
* Change the type model for Dexter_TransactionType to include more data for the
  token to xtz transaction.

### 2020-09-23 -- v0.16.4
* After a transaction is complete, update the amount of token purchased from
  Xtz to Token (when pending it shows the minimal value purchased).
* Change the type model for Dexter_TransactionType to include more data.

### 2020-09-23 -- v0.16.3
* Fix market exchange rate. Split into two functions: one for precision and one
  for display.
* Split toFloat from Tezos_Mutez and Tezos_Token into two functions: one for
  with decimal (less precise) and one for without (more precise).
* Start preliminary work for getting transaction result.

### 2020-09-23 -- v0.16.2
* Handle <1Xtz in remove liquidity
* Handle zero liquidity in add liquidity

### 2020-09-23 -- v0.16.1
* Remove Messari API and replace with CoinGecko API for getting the current
  USD value of XTZ.

### 2020-09-22 -- v0.16.0
* Get account FA1.2 token balance from Indexter.
* Get dexter FA1.2 allowance for account tokens from Indexter.
* Update tokens and dexter contracts for testnet.

### 2020-09-17 -- v0.15.14
* Fix $ values on remove liquidity
* Fix default token logic for add liquidity

### 2020-09-17 -- v0.15.13
* Make using removeLiquidity function more readable with named parameters.

### 2020-09-17 -- v0.15.12
* Hide max buttons for no balance
* Change plus icon color to blue for

### 2020-09-17 -- v0.15.11
* Fix the way of getting maximum_slippage value

### 2020-09-17 -- v0.15.10
* Handle first add liquidity case
* Change display for pending/failed transactions

### 2020-09-17 -- v0.15.9
* Mobile landing page

### 2020-09-17 -- v0.15.8
* Added operation title to status indicator
* Handling for rate below 0.00000001 case
* Limit number of allowed decimals in inputs
* Hide operation descriptions when button is disabled in Add/Remove liquidity

### 2020-09-16 -- v0.15.7
* Get Exchange XTZ and token balances from the dexter contract's storage.

### 2020-09-16 -- v0.15.6
* Add react testing library
* Add snapshots folder
* Inputs refactoring
* Changed inputs validation logic
* Add message about 1 XTZ reserve for transactions

### 2020-09-11 -- v0.15.5
* Fix for output to input exchange calculation
* Valid.Valid -> Valid
* Added InputType

### 2020-09-07 -- v0.15.4
* Change the exchange inputs logic
* Disallow custom slippage greater or equal 50%
* Store tokens pair for remove liquidity transactions

### 2020-09-07 -- v0.15.3
* Fix Micheline parameters removeLiquidity.

### 2020-09-04 -- v0.15.2
* Address Trail of Bits security audit issue TOB-DEXTER-012:
  Don't allow users to insert commas. This can be confusing and cause errors.
  On blur it inserts commas where necesary.
* Disallow commas in token exchange input
* Display commas in token blurred exchange input
* Red border for invalid address input

### 2020-09-04 -- v0.15.1
* Address Trail of Bits security audit issue TOB-DEXTER-014:
  Add Advanced options where user can set the deadline time for the contract
  operation to complete. Default is 20 minutes. They can set it from 1 to
  1440 minutes.
* Advanced options
* Reload account after successful transaction

### 2020-09-03 -- v0.15.0
* Transaction status bar
* Disable buttons when the transaction is pending
* Handle invalidated inputs case

### 2020-08-25 -- v0.14.2
* Added token balances placeholder

### 2020-08-25 -- v0.14.1
* Done pool data table
* Done darkmode

### 2020-08-25 -- v0.14.0
* Progress on darkmode and new design implementation

### 2020-08-13 -- v0.13.1
* Handle not connected wallet/empty wallet
* Add static routing
* Add Loader

### 2020-08-13 -- v0.13.0
* Separate account and balances states in context.

### 2020-08-11 -- v0.12.2
* Add nginx.conf.

### 2020-08-10 -- v0.12.1
* Add frontend routing.
* Fix Safari UI bugs.
* Add ExchangeAndSave TransactionType.
* Display two icons for pool tokens.

### 2020-08-06 -- v0.12.0
* Parse more information from the blockchain.
* Add function to get transactions for a contract and operation id from a block.

### 2020-08-06 -- v0.11.2
* Fix Max button on Add Liquidity
* Fix validation on Add Liquidity token input
* Fix decimal display for zero
* Display 0 on blurred empty input

### 2020-08-05 -- v0.11.1
* Search token logic and design update
* Implement Add/Remove Liquidity proper columns

### 2020-08-03 -- v0.11.0
* Main layout reorganization
* Added Liquidity Pool tab
* Add/Remove Liquidity code refactoring


### 2020-07-28 -- v0.10.3
* Removed svg icons replaceable with fontello.
* Added three states of slippage info.
* Added slippage info popover.
* Added liquidity explainer panel.

### 2020-07-28 -- v0.10.2
* Connect Wallet redesign.
* Wallet info redesign.
* Recent transactions redesign.
* Pool tokens redesign.
* Added fontello.

### 2020-07-23 -- v0.10.1
* Code restructurisation.
* Extract styles to separate modules.
* Extract common components to  separate modules.
* Added DexterUi_Context to avoid prop-drilling.
* Extracted Colors, TextStyles to separate modules.
* Layout adjustments to new design.

### 2020-07-21 -- v0.9.1
* Fix token input so it includes decimal information.
* Fix encodeXtzToToken and tokenToXtz.

### 2020-07-21 -- v0.9.0
* Default mainnet node is camlCase.
* Update dev tokens.
* Fix dexter storage and bigmap parse.

### 2020-07-21 -- v0.8.15
* Default testnet node is camlCase.
* Add tests for disableConfirmNodeChange.
* Fix bug with Confirm Selection button.

### 2020-06-30 -- v0.8.14
* Migrate project from dexter repo to dexter-frontend.

### 2020-06-30 -- v0.8.13
* Add infrastructure and testing to support querying data from view entrypoints.

### 2020-06-19 -- v0.8.12
* Display token balances below user address.

### 2020-06-19 -- v0.8.11
* Update color pallete for some values to match design in Sketch.
* Add disconnect button.

### 2020-06-19 -- v0.8.10
* Make reloads automatic only. Initially every two minutes.
* Remove the reload button.

### 2020-06-18 -- v0.8.9
* Add Beacon and TezBridge tutorial links.

### 2020-06-18 -- v0.8.8
* Update beacon-sdk to version 1.1.0.

### 2020-06-18 -- v0.8.7
* Add a button to the settings page to improve usability.

### 2020-06-18 -- v0.8.6
* Use dialog box in Sentry when there is an error.
* Fix logic for updating node url in settings.

### 2020-06-17 -- v0.8.5
* Update default URLs.
* Update README.md. Add detail about third party dependencies.

### 2020-06-15 -- v0.8.4
* Improve token representation by adding a decimal value.

### 2020-06-14 -- v0.8.3
* Improve token representation by adding a decimal value.
* Update bs-zarith to version 3.0.0.
* Fix some exchange rate tests.

### 2020-06-02 -- v0.8.2
* Add icon for MoonTZ.

### 2020-06-02 -- v0.8.1
* Use new tokens for Beta.

### 2020-05-30 -- v0.8.0
* Remove DexterUi_Settings.re.
* Update beacon-sdk to version 1.0.0.
* Set a default node. When user logins into beacon, use the beacon node.
* Dexter no longer sets the node for beacon. Beacon user must do this
  directly in Beacon.
* Add function to get chain name of Tezos node.
* Add deployment env variable for certain options depending on if it is
  production or testing.

### 2020-05-26 -- v0.7.0
* Put logic from DexterUi_Exchange.re into Swap, AddLiquidity and RemoveLiquidity.
* Share style for inputs in DexterUi_Style.
* Rename DexterUi_Swap to DexterUi_Exchange.
* Make style friendlier for different screen sizes.

### 2020-05-20 -- v0.6.6
* Change "Exchange from:" to "Exchange:".
* Change "Output (estimate)" to "To receive (estimate)".
* Change "pool tokens" to "liquidity tokens".
* Set default font as Source Sans Pro san serif.
* Update add liquidity message at top.

### 2020-05-19 -- v0.6.5
* Clearer faucet errors.

### 2020-05-19 -- v0.6.4
* Faucet button now says 500 XTZ.

### 2020-05-19 -- v0.6.3
* Improve camlCase Faucet.
* Add Beta to header.
* Update Beta TZG dexter contract with a lower exchange rate.

### 2020-05-19 -- v0.6.2
* Provide 100 XTZ for users on carthagenet through the UI.

### 2020-05-15 -- v0.6.1
* Update beacon-sdk to version 0.5.0.

### 2020-05-15 -- v0.6.0
* Update beacon-sdk to version 0.4.4.
* Improve 'Connect to Beacon' button interaction with Beacon.
* Add error reporting for wallet interactions.
* Support 'Reload' button for Beacon.
* Begin documentation on how to use Beacon.

### 2020-05-13 -- v0.5.20
* Remove unused bs-css functions in DexterUi_Swap.re
* Setup tests for exchange rates.
* Make exchange rate functions for displaying in the UI.

### 2020-05-13 -- v0.5.19
* Clear input values after succeful blockchain response in Exchange, Add
  Liquidity and Remove Liquidity.
* Use sentry npm package instead of script in html.

### 2020-05-13 -- v0.5.18
* Prevent remove liquidity if either output xtz or token is zero.

### 2020-05-13 -- v0.5.17
* Fix compile error in Dexter_AddLiquidity.
* Add sentry error reporting.

### 2020-05-13 -- v0.5.16
* Add slippage test data from dexter-integration.
* Simplify slippage calculations to reflect Python referenc version.
* Comment out out-of-date parsing data tests in Dexter_Spec.re. Need to update these later.

### 2020-05-12 -- v0.5.15
* Upgrade beacon-sdk to version 0.3.0.

### 2020-05-11 -- v0.5.14
* Remove Amplitude analytics.

### 2020-05-09 -- v0.5.13
* Cleanup account info design: Display wallet name and fix style

### 2020-05-08 -- v0.5.12
* Update to latest bs-css-emotion.
* Update slippage visual element to match design.
* Update slippage calculation functions.
* Disable exchange if slippage is higher than 50%.
* Display warning message if slippage is greater than user's preferred maximum.

### 2020-05-08 -- v0.5.11
* Update block link to carthagenet.tezblock.io.

### 2020-05-08 -- v0.5.10
* Bring back transaction history for add liquidity.

### 2020-05-08 -- v0.5.9
* Update block link to carthagenet.tzstats.com.

### 2020-05-08 -- v0.5.8
* Fix exchange rate calculations.

### 2020-05-08 -- v0.5.7
* Fix RemoveLiquidity token bugs.

### 2020-05-08 -- v0.5.6
* Fix AddLiquidity token bugs.

### 2020-05-08 -- v0.5.5
* Fix beacon-sdk integration error.

### 2020-05-08 -- v0.5.4
* Fix bug in Exchange screen when changing tokens.

### 2020-05-07 -- v0.5.3
* Improve settings UI. Remove slippage and retries from the settings page.

### 2020-05-07 -- v0.5.2
* Display version number in upper left corner.
* Improve input behavior for exchange and improve code naming in DexterUi_Swap.

### 2020-05-07 -- v0.5.1
* Add message to top of add liquidity describing what it does.
* Update Add Liquidity copy.
* Add RPC route to get contract baker.
* Display baker information.

### 2020-05-06 -- v0.5.0
* Change terminoloyg swap to exchange.
* Change tutorials/about link to Share feedback.

### 2020-05-06 -- v0.4.14
* Support sending to another address in XTZ to token and token to xtz.

### 2020-05-06 -- v0.4.13
* Improve permission requests for Beacon.

### 2020-05-05 -- v0.4.12
* Support Beacon wallet.
* Remove hardcoded support for TezBridge and split it to support multiple wallets (currently Beacon and TezBridge).

### 2020-04-22 -- v0.4.11
* Fix auto reload. Only called once at a time now.

### 2020-04-22 -- v0.4.10
* Reload automatically.
* Allow the user to control the reload time.

### 2020-04-20 -- v0.4.9
* Fix disable removeLiquidity logic.

### 2020-04-20 -- v0.4.8
* Fix disable addLiquidity logic.

### 2020-04-20 -- v0.4.7
* Fix disable swap logic.

### 2020-04-15 -- v0.4.6
* Improve analytics.

### 2020-04-15 -- v0.4.5
* Show symbols and USD outputs in right hand side of remove liquidity.

### 2020-04-13 -- v0.4.4
* Calculate price slippage and allower user to cap the amount of acceptable
  price slippage.

### 2020-04-13 -- v0.4.3
* Add message about swap fee in the swap screen.

### 2020-04-09 -- v0.4.3
* Add amplitude analytics to Dexter.

### 2020-04-08 -- v0.4.2
* Add MAX button to Swap, Add Liquidity and Remove Liquidity.

### 2020-04-08 -- v0.4.1
* Support tz2 and tz3 addresses in the script expr conversion function.

### 2020-04-07 -- v0.4.0
* Create node type that is saved to the user's browser local storage.
* Insert node at the top data structure in of Index.re.
* Add settings page in which the user can change the node.
* Update to bs-platform 7.2.2.

### 2020-04-06 -- v0.3.8
* Use contracts in Carthagenet for development.
* Update Dexter store parser to reflect changes in Dexter contract.

### 2020-03-20 -- v0.3.7
* Point to Carthagenet contracts in Index.re.

### 2020-03-06 -- v0.3.6
* Add mobile message. Not supported yet.

### 2020-03-06 -- v0.3.5
* Improve the CSS style of the tokens displayed under "Your pool tokens".

### 2020-03-03 -- v0.3.4
* Add Dexter_TransactionType.
* Improve Recent Transactions style.

### 2020-03-03 -- v0.3.3
* Get the candidate bid amount.
* Subtract the candidate bid amount from the amount held by a contract when query a contracts data
  to properly reflect the xtz pool.

### 2020-03-03 -- v0.3.2
* Add icons for Tezos Gold, Tezos Silver and Tezos Tacos.

### 2020-02-27 -- v0.3.1
* Drop downs now include information about the origin of the values.

### 2020-02-20 -- v0.3.0
* Add bs-zarith as a dependency.
* Use Bigint.t in Tezos.Token.t instead of int64. Remove int64 from most places.
* Fix getXtzToTokenRate for swap.
* Create getTokensDeposited, getXtzDeposited and getLqtMinted for AddLiquidity.

### 2020-02-18 -- v0.2.2
* Load transactions right after sigining in TezBridge.

### 2020-02-18 -- v0.2.1
* Render reload button. It works correctly now.
* Remove flex wrap from DexterUi.
* Display block level, link block hash, and time from when account was loaded.

### 2020-02-18 -- v0.2.0
* Release new demo to dexter.camlcase.io
* Store transactions locally and display them as operation links to Better Call Dev.
* Remove commas after the decimal point.
* Add commas to USD values.
* Fix several value display errors.
* Reduce vertical distance between HTML elements.
* Display amount of XTZ and token held by an exchange.
* Clean up style for owned pool tokens.
* Block attempts to perform token to token for now.

### 2019-09-11 -- v0.1.1
* Update language to be more consistent: buy and sell.
* Add Checkers and Wrapped LTC tokens and exchanges.
* Move Dexter how to to the top.

### 2019-09-11 -- v0.1.0
* Release first alpha version of Dexter on the alphanet.
