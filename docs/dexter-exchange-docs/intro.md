# Dexter

Dexter is a decentralized exchange for XTZ and
[FA1.2](https://gitlab.com/tzip/tzip/-/tree/master/proposals/tzip-7)
tokens on the Tezos blockchain. It removes the need for centralized
liquidity providers by automating a market for XTZ and FA1.2 in the
logic of a smart contract. Each Dexter smart contract is associated with one 
FA1.2 token. Dexter is open-source software released under the [GPL](https://en.wikipedia.org/wiki/GNU_General_Public_License)
license.

Any owner of XTZ and FA1.2 can become a liquidity provider by depositing an 
equivalent amount of XTZ and FA1.2 into the smart contract. Once there is 
liquidity in a Dexter smart contract then owners of XTZ or FA1.2 may trade 
one asset for the other via Dexter.

The exchange rate is determined by the balance of XTZ and FA1.2 
tokens in the Dexter smart contract.

Liquidity providers are rewarded a 0.3% of each exchange, paid by the 
trader, of each exchange on the Dexter smart contract and they can

Liquidity providers are incentived by receiving a small fee for each
trade, 0.3%, paid by the trader. Liquidity providers may redeem their share of
XTZ and FA1.2 at anytime.

## Technical Details

Dexter is a single Tezos smart contract written in Morley, a Haskell library 
that outputs Michelson and has a property test framework. Dexter has a variety
of property tests on all of its entry points. Trail of Bits performed a 
security audit of the Morley code and Nomadic Labs wrote a formal proof of the 
output Michelson code.

For each FA1.2 token we support, we originate a single Dexter contract. The 
token it is paired with cannot change.
