**Each # represents a page, they will be linked on the side bar **

--- Basic ----

# Dexter

Dexter is a decentralized exchange for XTZ and
[FA1.2](https://gitlab.com/tzip/tzip/-/tree/master/proposals/tzip-7)
tokens on the Tezos blockchain. It removes the need for centralized
liquidity providers by providing an automated market maker in the
logic of the smart contract.  The exchange rate is determined by the
balance of XTZ and FA1.2 tokens in the Dexter smart contract. Dexter
is open-source software released under the
[GPL](https://en.wikipedia.org/wiki/GNU_General_Public_License)
license.

Each Dexter smart contract is associated with one FA1.2 token.

Anyone can become a liquidity provider by depositing an equivalent
amount of XTZ and FA1.2 into the smart contract. The value of
XTZ-to-FA1.2 is determined by the amount of each held in a Dexter
smart contract. The liquidity providers are rewarded a small
percentage of each exchange on the Dexter smart contract and they can
redeem their tokens at anytime.

The exchange rate remains constant after liquidity providers add or
removing liquidity.

The exchange rate fluctuates when users perform exchanges, but a the
product of both pools remains the same. The amount of XTZ held times
the amount of FA1.2 held by the dexter contract remains the same after
each exchange, `XTZ * FA1.2` remains constant.

Liquidity providers are incentived by receiving a small fee for each
trade, 0.3%, paid by the trader. The dexter contract is also delegated
to the baker. The rewards from baking will be evenly split among the
liquidity providers.

In order to prevent pools being drained by large trades, the larger
the trade relative to the pools execute the worse the exchange rate
(exponentially).

# Participants

Liquidity Providers Traders Developers


# Glossary 

Automated Market Maker

Constant Product Formula

FA1.2

XTZ

LQT

Liquidity

Pool

Smart Contract

Slippage

--- Advanced ---

# Math

Liquidity Provider Fee How Trades are calculated

# Delegation

Explain how the delegation works and is controlled by the manager
(emphasize that the manager does not receive any rewards). Also
mention that it can be controlled by a multi sig contract. Manager can
be changed and baker can be frozen (explain the possibility of
contract baker system from the agora post).

# Smart Contracts

Overview of the morley smart contracts, how they were proved and

# How to integrate dexter into your application

mention how to query data from the contract point to the
dexter-integration document point to magma and dexter-frontend

# Update Token Pool

Because of the way contract interaction works in Tezos, Dexter is
responsible for keeping track of the FA1.2 it owns. However, depending
on the behavior of the FA1.2 token that value may increase over time,
decrease over time or change for other reasons that dexter is not
aware of. The update token pool informs dexter of how much it owns and
updates its value.
