/**
 * get token_pool and lqt_total from a Dexter contract's storage.
 */
type t = {
  lqt: Tezos.Token.t,
  tokenPool: Tezos.Token.t,
  xtzPool: Tezos.Mutez.t,
};

/**
 * storage
 * (pair (big_map %accounts (address :owner)
 *                          (pair (nat :balance)
 *                                (map (address :spender)
 *                                     (nat :allowance))))
 *       (pair (pair (bool :calledByDexter)
 *                   (pair (bool :freezeBaker)
 *                         (nat :lqtTotal)))
 *             (pair (pair (address :manager)
 *                         (address :tokenAddress))
 *                   (pair (nat :tokenPool)
 *                         (mutez :xtzPool)))));
 *
 * storage example value
 *
 * Pair 10598
 *   (Pair (Pair False (Pair False 1000000000))
 *         (Pair (Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" "KT1P1Vmh3xktmEcP1LGCHbsBe31zC4epRA7h")
 *               (Pair 50000000 1000000000)))
 *
 */

let ofExpression =
    (decimals: int, expression: Tezos.Expression.t): Belt.Result.t(t, string) =>
  switch (expression) {
  | SingleExpression(
      Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
      Some([
        Tezos_Expression.IntExpression(_bigMapId), // %accounts
        SingleExpression(
          Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
          Some([
            SingleExpression(
              Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
              Some([
                SingleExpression(
                  Tezos_Primitives.PrimitiveData(_calledByDexter),
                  _,
                  _,
                ),
                SingleExpression(
                  Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
                  Some([
                    _freezeBaker,
                    Tezos_Expression.IntExpression(lqtTotal),
                  ]),
                  _,
                ),
              ]),
              _,
            ),
            SingleExpression(
              Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
              Some([
                SingleExpression(
                  Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
                  Some([_manager, _tokenAddress]),
                  _,
                ),
                SingleExpression(
                  Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
                  Some([
                    Tezos_Expression.IntExpression(tokenPool),
                    Tezos_Expression.IntExpression(xtzPool),
                  ]),
                  _,
                ),
              ]),
              _,
            ),
          ]),
          _,
        ),
      ]),
      _,
    ) =>
    Belt.Result.Ok(
      {
        lqt: Tezos.Token.mkToken(lqtTotal, 0),
        tokenPool: Tezos.Token.mkToken(tokenPool, decimals),
        xtzPool: Tezos.Mutez.ofBigintUnsafe(xtzPool),
      }: t,
    )
  | _ =>
    Belt.Result.Error(
      Tezos.Expression.encode(expression) |> Js.Json.stringify,
    )
  };

/** edo
 * pair big_map_id
 *      (pair (bool :calledByDexter) (bool :freezeBaker) (nat :lqtTotal))
 *      (pair (address :manager) (address :tokenAddress))
 *      (nat :tokenPool)
 *      (mutez :xtzPool)
 *
 * storage example value
 *
 * Pair 10598
 *      (Pair False False 1000000000)
 *      (Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" "KT1P1Vmh3xktmEcP1LGCHbsBe31zC4epRA7h")
 *      50000000
 *      1000000000
 *
 */

let ofEdoExpression =
    (decimals: int, expression: Tezos.Expression.t): Belt.Result.t(t, string) =>
  switch (expression) {
  | SingleExpression(
      Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
      Some([
        Tezos_Expression.IntExpression(_bigMapId), // %accounts
        SingleExpression(
          Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
          Some([
            _calledByDexter,
            _freezeBaker,
            Tezos_Expression.IntExpression(lqtTotal),
          ]),
          _,
        ),
        SingleExpression(
          Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
          Some([_manager, _tokenAddress]),
          _,
        ),
        Tezos_Expression.IntExpression(tokenPool),
        Tezos_Expression.IntExpression(xtzPool),
      ]),
      _,
    ) =>
    Belt.Result.Ok(
      {
        lqt: Tezos.Token.mkToken(lqtTotal, 0),
        tokenPool: Tezos.Token.mkToken(tokenPool, decimals),
        xtzPool: Tezos.Mutez.ofBigintUnsafe(xtzPool),
      }: t,
    )
  | _ =>
    Belt.Result.Error(
      Tezos.Expression.encode(expression) |> Js.Json.stringify,
    )
  };
