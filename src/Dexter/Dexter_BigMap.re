type t = {lqt: Tezos.Token.t};

let ofExpression = (expression: Tezos.Expression.t): Belt.Result.t(t, string) =>
  switch (expression) {
  | SingleExpression(
      Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
      Some([IntExpression(lqt), _]),
      _,
    ) =>
    Belt.Result.Ok(
      {
        lqt: Tezos.Token.mkToken(lqt,0)
      }: t,
    )
  | _ =>
    Belt.Result.Error(
      Tezos_Expression.encode(expression) |> Js.Json.stringify,
    )
  };

/** Pair (allowances: map(address, nat)) balance) */;
/** (Pair {} 1000000) */;
