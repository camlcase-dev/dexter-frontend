open Dexter_TransactionUtils;

type t = {
  block: string,
  contractId: string,
  hash: string,
  status: TZKT.Status.t,
  timestamp: Tezos.Timestamp.t,
  transactionType: Dexter_TransactionType.t,
  walletAddress: Tezos.Address.t,
};

let encode = (t: t): Js.Json.t =>
  Json.Encode.(
    object_([
      ("block", t.block |> string),
      ("contractId", t.contractId |> string),
      ("hash", t.hash |> string),
      ("status", t.status |> TZKT.Status.encode),
      ("timestamp", t.timestamp |> Tezos.Timestamp.encode),
      ("transactionType", t.transactionType |> Dexter_TransactionType.encode),
      ("walletAddress", t.walletAddress |> Tezos_Address.encode),
    ])
  );

let decode = (json: Js.Json.t): Belt.Result.t(t, string) =>
  Json.Decode.(
    switch (
      json |> field("block", string),
      json |> field("contractId", string),
      json |> field("hash", string),
      json |> field("status", TZKT.Status.decode),
      json |> field("timestamp", Tezos_Timestamp.decode),
      json |> field("transactionType", Dexter_TransactionType.decode),
      json |> field("walletAddress", Tezos_Address.decode),
    ) {
    | (
        block,
        contractId,
        hash,
        Belt.Result.Ok(status),
        Belt.Result.Ok(timestamp),
        Belt.Result.Ok(transactionType),
        Belt.Result.Ok(walletAddress),
      ) =>
      Belt.Result.Ok({
        block,
        contractId,
        hash,
        status,
        timestamp,
        transactionType,
        walletAddress,
      })
    | (_, _, _, Belt.Result.Error(err), _, _, _)
    | (_, _, _, _, Belt.Result.Error(err), _, _)
    | (_, _, _, _, _, Belt.Result.Error(err), _)
    | (_, _, _, _, _, _, Belt.Result.Error(err)) => Belt.Result.Error(err)
    | exception (DecodeError(error)) => Belt.Result.Error(error)
    }
  );

let getLatestTransaction = (transactions: option(list(t))): option(t) =>
  transactions |> Utils.flatMap(transactions => List.nth_opt(transactions, 0));

let ofTransactionResponse =
    (
      contractId: Tezos_Contract.t,
      response: Tezos_Wallet.Operation.Response.t,
      walletAddress: Tezos.Address.t,
      transactionType: Dexter_TransactionType.t,
    )
    : Js.Promise.t(Belt.Result.t(t, string)) => {
  Tezos_RPC.getHeadBlockId(Dexter_Settings.nodeUrl, "main")
  |> Js.Promise.then_(result =>
       switch (result) {
       | Belt.Result.Ok(block) =>
         Belt.Result.Ok({
           block,
           contractId: contractId |> Tezos_Contract.toString,
           hash:
             response |> Tezos_Wallet.Operation.Response.getTransactionHash,
           status: Pending,
           transactionType,
           timestamp: Timestamp(MomentRe.momentNow()),
           walletAddress,
         })
         |> Js.Promise.resolve
       | Belt.Result.Error(err) =>
         Belt.Result.Error(err) |> Js.Promise.resolve
       }
     );
};

let getBlockToHeadLevelsDiff =
    (blockId: string): Js.Promise.t(Belt.Result.t(int, string)) => {
  Js.Promise.all2((
    Tezos.RPC.getHeadBlock(Dexter_Settings.nodeUrl, "main"),
    Tezos.RPC.getBlock(Dexter_Settings.nodeUrl, "main", blockId),
  ))
  |> Js.Promise.then_(((headBlock, block)) => {
       (
         switch (headBlock, block) {
         | (
             Belt.Result.Ok((headBlock: Tezos_Block.t)),
             Belt.Result.Ok((block: Tezos_Block.t)),
           ) =>
           Belt.Result.Ok(headBlock.header.level - block.header.level)
         | (Belt.Result.Ok(_), _) => Belt.Result.Error("Block not found")
         | (_, Belt.Result.Ok(_)) =>
           Belt.Result.Error("Head block not found")
         | _ => Belt.Result.Error("Blocks not found")
         }
       )
       |> Js.Promise.resolve
     });
};

let rec getTransactionResultRec =
        (blockId: string, tr: t)
        : Js.Promise.t((TZKT.Status.t, option(Dexter_TransactionType.t))) => {
  Tezos.RPC.getBlock(Dexter_Settings.nodeUrl, "main", blockId)
  |> Js.Promise.then_(result =>
       switch (result) {
       | Belt.Result.Ok((block: Tezos_Block.t)) =>
         let transactions =
           Tezos_Block.getContractTransactions(tr.contractId, tr.hash, block);
         if (transactions |> List.length > 0) {
           let transactionResult =
             List.nth(transactions, 0) |> Tezos_Block.getTransactionResult;

           (
             switch (
               (
                 transactionResult:
                   option(
                     Tezos_Operation_Alpha.Contents.Result.Transaction.Metadata.t,
                   )
               )
             ) {
             | Some(metadata) => (
                 TZKT.Status.ofOperationAlphaOperationResult(
                   Some(metadata.operationResult),
                 ),
                 Some(
                   Dexter_TransactionType.updateValueAfterOperation(
                     tr.transactionType,
                     tr.contractId,
                     metadata,
                   ),
                 ),
               )
             | None => (TZKT.Status.Ready, None)
             }
           )
           |> Js.Promise.resolve;
         } else if (blockId !== tr.block) {
           getTransactionResultRec(block.header.predecessor, tr);
         } else {
           (TZKT.Status.Pending, None) |> Js.Promise.resolve;
         };
       | Belt.Result.Error(error) =>
         Js.log(error);
         (TZKT.Status.Ready, None) |> Js.Promise.resolve;
       }
     );
};

let getTransactionStatus =
    (tr: t)
    : Js.Promise.t((TZKT.Status.t, option(Dexter_TransactionType.t))) =>
  tr.block
  |> getBlockToHeadLevelsDiff
  |> Js.Promise.then_(result => {
       switch (result) {
       | Belt.Result.Ok(levelDiff) =>
         levelDiff <= 10
           ? Tezos.RPC.getHeadBlockId(Dexter_Settings.nodeUrl, "main")
             |> Js.Promise.then_(result =>
                  switch (result) {
                  | Belt.Result.Ok(blockId) =>
                    getTransactionResultRec(blockId, tr)
                  | Belt.Result.Error(error) =>
                    Js.log(error);
                    (TZKT.Status.Ready, None) |> Js.Promise.resolve;
                  }
                )
           : (TZKT.Status.Ready, None) |> Js.Promise.resolve
       | Belt.Result.Error(error) =>
         Js.log(error);
         (TZKT.Status.Ready, None) |> Js.Promise.resolve;
       }
     });

let ofOperations =
    (
      walletAddress: Tezos_Address.t,
      exchanges: list(Dexter_Exchange.t),
      operations: list(TZKT.Operation.t),
    )
    : option(t) =>
  switch (getNamedEntrypoint(operations, "xtzToToken")) {
  | Some(xtzToTokenOperation) =>
    Belt.Option.map(
      Dexter_TransactionType.Exchange.ofOperationsToXtzToToken(
        exchanges,
        xtzToTokenOperation,
        operations,
      ),
      x =>
      {
        block: xtzToTokenOperation.block,
        contractId: "",
        hash: xtzToTokenOperation.hash,
        status: xtzToTokenOperation.status,
        timestamp: Timestamp(MomentRe.moment(xtzToTokenOperation.timestamp)),
        transactionType: Exchange(x),
        walletAddress,
      }
    )
  | None =>
    switch (getNamedEntrypoint(operations, "tokenToXtz")) {
    | Some(tokenToXtzOperation) =>
      Belt.Option.map(
        Dexter_TransactionType.Exchange.ofOperationsToTokenToXtz(
          exchanges,
          tokenToXtzOperation,
          operations,
        ),
        x =>
        {
          block: tokenToXtzOperation.block,
          contractId: "",
          hash: tokenToXtzOperation.hash,
          status: tokenToXtzOperation.status,
          timestamp:
            Timestamp(MomentRe.moment(tokenToXtzOperation.timestamp)),
          transactionType: Exchange(x),
          walletAddress,
        }
      )
    | None =>
      switch (getNamedEntrypoint(operations, "addLiquidity")) {
      | Some(addLiquidityOperation) =>
        Belt.Option.map(
          Dexter_TransactionType.AddLiquidity.ofOperations(
            exchanges,
            addLiquidityOperation,
            operations,
          ),
          x =>
          {
            block: addLiquidityOperation.block,
            contractId: "",
            hash: addLiquidityOperation.hash,
            status: addLiquidityOperation.status,
            timestamp:
              Timestamp(MomentRe.moment(addLiquidityOperation.timestamp)),
            transactionType: AddLiquidity(x),
            walletAddress,
          }
        )
      | None =>
        switch (getNamedEntrypoint(operations, "removeLiquidity")) {
        | Some(removeLiquidityOperation) =>
          Belt.Option.map(
            Dexter_TransactionType.RemoveLiquidity.ofOperations(
              exchanges,
              removeLiquidityOperation,
              operations,
            ),
            x =>
            {
              block: removeLiquidityOperation.block,
              contractId: "",
              hash: removeLiquidityOperation.hash,
              status: removeLiquidityOperation.status,
              timestamp:
                Timestamp(
                  MomentRe.moment(removeLiquidityOperation.timestamp),
                ),
              transactionType: RemoveLiquidity(x),
              walletAddress,
            }
          )
        | None => None
        }
      }
    }
  };

/*
 * for a list Dexter_Exchange.t and TZKT.Operation.t, return operations that are
 * related to dexter
 */
let filterDexterOperations =
    (
      walletAddress: Tezos_Address.t,
      exchanges: list(Dexter_Exchange.t),
      operations: list(TZKT.Operation.t),
    )
    : list(TZKT.Operation.t) => {
  /* let tokenContracts = */
  /*   Belt.List.map(exchanges, exchange => */
  /*     Tezos_Address.ofContract(exchange.tokenContract) */
  /*   ); */
  let dexterContracts =
    Belt.List.map(exchanges, exchange =>
      Tezos_Address.ofContract(exchange.dexterContract)
    );

  List.filter(
    (operation: TZKT.Operation.t) => {
      switch (operation.parameters) {
      | Some(parameters) =>
        /* the target is a dexter contract and the entrypoint is xtzToToken, tokenToXtz, addLiquidity or removeLiquidity */
        (
          parameters.entrypoint
          == Tezos_Operation_Alpha.Entrypoint.Named("xtzToToken")
          || parameters.entrypoint
          == Tezos_Operation_Alpha.Entrypoint.Named("tokenToXtz")
          || parameters.entrypoint
          == Tezos_Operation_Alpha.Entrypoint.Named("addLiquidity")
          || parameters.entrypoint
          == Tezos_Operation_Alpha.Entrypoint.Named("removeLiquidity")
          && List.mem(operation.target, dexterContracts)
        )
        /* the target is a token contract and the entrypoint is approve */
        || parameters.entrypoint
        == Tezos_Operation_Alpha.Entrypoint.Named("approve")
        && operation.sender == walletAddress
        && List.mem(operation.target, dexterContracts)
        /* the sender is dexter or the walletAddress, the target is a token contract and the entrypoint is transfer */
        || parameters.entrypoint
        == Tezos_Operation_Alpha.Entrypoint.Named("transfer")
        && (
          /* walletAddress sends to dexter */
          operation.sender == walletAddress
          && List.mem(operation.target, dexterContracts)
          /* dexter sends to walletAddress or third party */
          || List.mem(operation.sender, dexterContracts)
        )

      | None => List.mem(operation.sender, dexterContracts) /* In this operation, dexter sends XTZ to an address */
      }
    },
    operations,
  );
};

/*
 * group a list of operations by their hash
 */
let groupByHash =
    (operations: list(TZKT.Operation.t))
    : Belt.Map.String.t(list(TZKT.Operation.t)) => {
  let m = ref(Belt.Map.String.empty);
  Belt.List.map(operations, (x: TZKT.Operation.t) => {
    m :=
      Belt.Map.String.update(m^, x.hash, o => {
        switch (o) {
        | Some(vs) => Some(List.append(vs, [x]))
        | None => Some([x])
        }
      })
  })
  |> ignore;
  m^;
};

let ofMap =
    (
      walletAddress: Tezos_Address.t,
      exchanges: list(Dexter_Exchange.t),
      m: Belt.Map.String.t(list(TZKT.Operation.t)),
    )
    : list(t) => {
  Belt.List.map(
    Belt.Map.String.toList(m),
    ((_k: string, operations: list(TZKT.Operation.t))) => {
    ofOperations(walletAddress, exchanges, operations)
  })
  |> Common.catSomes;
};

let ofQuery =
    (
      walletAddress: Tezos_Address.t,
      exchanges: list(Dexter_Exchange.t),
      operations: list(TZKT.Operation.t),
    )
    : list(t) => {
  filterDexterOperations(walletAddress, exchanges, operations)
  |> groupByHash
  |> ofMap(walletAddress, exchanges);
};
