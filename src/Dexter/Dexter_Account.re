/**
 * A collection of xtz and dexter exchange information as it relates to a single address in tezos.
 * This should be collected after fetching the address from a wallet.
 */

/**
 * keep track of the following:
 * the address of an account
 * how much the account owns of each token and the total amount of each token
 * how much the account owns of an exchange and the total liquidity of each exchange
 */

// Dexter_Setting.indexter

type t = {
  address: Tezos.Address.t,
  xtz: Tezos.Mutez.t,
  headBlock: Tezos_Block.t,
  wallet: Tezos_Wallet.t,
};

/**
If given the same list
Token.fetch
Dexter.fetch
should return lists of the same size, if they do not, then there might be a network error.
if an account does not have a key in a particular big map, it should return a default value
 */

let fetch =
    (address: Tezos.Address.t, wallet: Tezos_Wallet.t)
    : Js.Promise.t((option(t), list(Dexter_Balance.t))) => {
  let url = Dexter_Settings.nodeUrl;
  let tokenBalancesPromise =
    Dexter_Settings.exchanges
    |> List.map((exchange: Dexter.Exchange.t) =>
         Token.Query.fetch(url, address, exchange)
       )
    |> Array.of_list
    |> Js.Promise.all;

  let balancePromise =
    address
    |> Tezos.Address.toString
    |> Tezos.RPC.getBalance(url, "main", "head");

  let headBlockPromise = Tezos.RPC.getHeadBlock(url, "main");

  Js.Promise.all4((
    Dexter_Query.getBalances(),
    tokenBalancesPromise,
    balancePromise,
    headBlockPromise,
  ))
  |> Js.Promise.then_(((balances, tokenResults, xtzResult, headBlockResult)) =>
       switch (
         balances |> Array.to_list |> Common.sequence,
         tokenResults |> Array.to_list |> Common.sequence,
         xtzResult,
         headBlockResult,
       ) {
       | (
           Belt.Result.Ok(balances),
           Belt.Result.Ok(tokens),
           Belt.Result.Ok(xtz),
           Belt.Result.Ok(headBlock),
         ) =>
         Dexter_LocalStorage.WalletType.set(wallet);

         let account = Some({address, xtz, headBlock, wallet});

         let balances =
           balances
           |> Belt.List.zip(tokens)
           |> List.map(
                (
                  (
                    token: Token_Query.response,
                    balances: Dexter_ExchangeBalance.t,
                  ),
                ) =>
                Dexter_Balance.ExchangeBalance({
                  ...balances,
                  lqtBalance: token.dexterBigMap.lqt,
                  tokenBalance: token.accountTokenBalance,
                  exchangeAllowanceForAccount: token.dexterAllowanceForAccount,
                })
              )
           |> List.append([Dexter_Balance.XtzBalance(xtz)]);

         (account, balances) |> Js.Promise.resolve;
       | (Belt.Result.Error(_err), _, _, _)
       | (_, Belt.Result.Error(_err), _, _)
       | (_, _, Belt.Result.Error(_err), _)
       | (_, _, _, Belt.Result.Error(_err)) =>
         (None, []) |> Js.Promise.resolve
       }
     );
};
