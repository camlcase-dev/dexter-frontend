type t = {
  name: string,
  symbol: string,
  iconDark: string,
  iconLight: string,
  decimals: int,
  tokenContract: Tezos.Contract.t,
  dexterContract: Tezos.Contract.t,
  dexterBigMapId: Tezos.BigMapId.t,
};

let getExchangeAddresses = (xs: list(t)): list(Tezos.Address.t) => {
  Belt.List.map(xs, x => x.dexterContract |> Tezos_Address.ofContract);
};

let getContractAddresses = (xs: list(t)): list(Tezos.Address.t) => {
  Belt.List.map(xs, x => x.tokenContract |> Tezos_Address.ofContract);
};

/**
 * Calculate how many XTZ a single token is worth at the market rate.
 * Treat 1 XTZ or 1,000,000 mutez as 1.0
 * Treat 1.05 token as 1.05
 */

let xtzToTokenMarketRateJs: (string, string, int) => Js.Nullable.t(float) = [%bs.raw
  {|
   function (xtzPool, tokenPool, decimals) {
     const dc = require('dexter-calculations');
     return dc.xtzToTokenMarketRate(xtzPool, tokenPool, decimals);
   }
  |}
];

let xtzToTokenMarketRateForDisplay =
    (xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t): float =>
  switch (
    Js.Nullable.toOption(
      xtzToTokenMarketRateJs(
        Tezos_Mutez.toString(xtzPool),
        Tezos_Token.toStringPlain(tokenPool),
        tokenPool.decimals,
      ),
    )
  ) {
  | Some(p) => p
  | None => 0.0
  };

/**
 * This should only be called on non-zero numbers.
 */
let toToken = (m: Tezos.Mutez.t) => {
  switch (m) {
  | Mutez(t) => Tezos.Token.mkToken(Bigint.of_int64(t), 0)
  };
};

/**
 * This should only be called on non-zero numbers.
 */
let toMutez = (m: Tezos.Token.t) => {
  Tezos.Mutez.Mutez(Bigint.to_int64(m.value));
};

/**
 * Calculate the maximum amount of tokens the user will receive.
 **/
let xtzToTokenTokenOutputJs: (string, string, string) => Js.Nullable.t(string) = [%bs.raw
  {|
   function (xtzIn, xtzPool, tokenPool) {
     const dc = require('dexter-calculations');
     const b  = dc.xtzToTokenTokenOutput(xtzIn, xtzPool, tokenPool);
     if (b === null) {
       return null;
     };
     return b.toString();
   }
  |}
];

let xtzToTokenTokenOutput =
    (xtzIn: Tezos.Mutez.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : option(Tezos.Token.t) =>
  switch (
    Js.Nullable.toOption(
      xtzToTokenTokenOutputJs(
        Tezos_Mutez.toString(xtzIn),
        Tezos_Mutez.toString(xtzPool),
        Tezos_Token.toStringPlain(tokenPool),
      ),
    )
  ) {
  | Some(str) =>
    switch (Tezos_Token.ofString(str, 0)) {
    | Belt.Result.Ok(t) => Some({...t, decimals: tokenPool.decimals})
    | Belt.Result.Error(_) => None
    }
  | None => None
  };

let xtzToTokenXtzInputJs:
  (string, string, string, int) => Js.Nullable.t(string) = [%bs.raw
  {|
   function (tokenOut, xtzPool, tokenPool, decimals) {
     const dc = require('dexter-calculations');
     const b  = dc.xtzToTokenXtzInput(tokenOut, xtzPool, tokenPool, decimals);
     if (b === null) {
       return null;
     };
     return b.toString();
   }
  |}
];

let xtzToTokenXtzInput =
    (
      tokenOut: Tezos.Token.t,
      xtzPool: Tezos.Mutez.t,
      tokenPool: Tezos.Token.t,
    )
    : option(Tezos.Mutez.t) =>
  switch (
    Js.Nullable.toOption(
      xtzToTokenXtzInputJs(
        Tezos_Token.toStringPlain(tokenOut),
        Tezos_Mutez.toString(xtzPool),
        Tezos_Token.toStringPlain(tokenPool),
        tokenPool.decimals,
      ),
    )
  ) {
  | Some(str) =>
    switch (Tezos_Mutez.ofString(str)) {
    | Belt.Result.Ok(t) => Some(t)
    | Belt.Result.Error(_) => None
    }
  | None => None
  };

/**
 * Calculate the exchange rate of XTZ to token with fees included.
 */
let xtzToTokenExchangeRateJs: (string, string, string) => Js.Nullable.t(float) = [%bs.raw
  {|
   function (xtzIn, xtzPool, tokenPool) {
     const dc = require('dexter-calculations');
     return dc.xtzToTokenExchangeRate(xtzIn, xtzPool, tokenPool);
   }
  |}
];

let xtzToTokenExchangeRate =
    (xtzIn: Tezos.Mutez.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : float => {
  switch (
    Js.Nullable.toOption(
      xtzToTokenExchangeRateJs(
        Tezos_Mutez.toString(xtzIn),
        Tezos_Mutez.toString(xtzPool),
        Tezos_Token.toStringPlain(tokenPool),
      ),
    )
  ) {
  | Some(result) => result
  | None => 0.0
  };
};

let xtzToTokenExchangeRateForDisplayJs:
  (string, string, string, int) => Js.Nullable.t(float) = [%bs.raw
  {|
   function (xtzIn, xtzPool, tokenPool, decimals) {
     const dc = require('dexter-calculations');
     return dc.xtzToTokenExchangeRateForDisplay(xtzIn, xtzPool, tokenPool, decimals);
   }
  |}
];

let xtzToTokenExchangeRateForDisplay =
    (xtzIn: Tezos.Mutez.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : float => {
  switch (
    Js.Nullable.toOption(
      xtzToTokenExchangeRateForDisplayJs(
        Tezos_Mutez.toString(xtzIn),
        Tezos_Mutez.toString(xtzPool),
        Tezos_Token.toStringPlain(tokenPool),
        tokenPool.decimals,
      ),
    )
  ) {
  | Some(result) => result
  | None => 0.0
  };
};

let xtzToTokenPriceImpactJs: (string, string, string) => Js.Nullable.t(float) = [%bs.raw
  {|
   function (xtzIn, xtzPool, tokenPool) {
     const dc = require('dexter-calculations');
     return dc.xtzToTokenPriceImpact(xtzIn, xtzPool, tokenPool);
   }
  |}
];

let xtzToTokenPriceImpact =
    (xtzIn: Tezos.Mutez.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : option(float) =>
  Js.Nullable.toOption(
    xtzToTokenPriceImpactJs(
      Tezos_Mutez.toString(xtzIn),
      Tezos_Mutez.toString(xtzPool),
      Tezos_Token.toStringPlain(tokenPool),
    ),
  );

/**
 *  Multiply priceImpact result by 100 for percentage display
 */
let xtzToTokenPriceImpactForDisplay =
    (xtzIn: Tezos.Mutez.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : option(float) => {
  xtzToTokenPriceImpact(xtzIn, xtzPool, tokenPool)
  |> Utils.map(priceImpact => priceImpact *. 100.0);
};

/**
 * Token to XTZ
 */
let tokenToXtzXtzOutputJs: (string, string, string) => Js.Nullable.t(string) = [%bs.raw
  {|
   function (tokenIn, xtzPool, tokenPool) {
     const dc = require('dexter-calculations');
     const b  = dc.tokenToXtzXtzOutput(tokenIn, xtzPool, tokenPool);
     if (b === null) {
       return null;
     };
     return b.toString();
   }
  |}
];

let tokenToXtzXtzOutput =
    (tokenIn: Tezos.Token.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : option(Tezos.Mutez.t) =>
  switch (
    Js.Nullable.toOption(
      tokenToXtzXtzOutputJs(
        Tezos_Token.toStringPlain(tokenIn),
        Tezos_Mutez.toString(xtzPool),
        Tezos_Token.toStringPlain(tokenPool),
      ),
    )
  ) {
  | Some(str) =>
    switch (Tezos_Mutez.ofString(str)) {
    | Belt.Result.Ok(t) => Some(t)
    | Belt.Result.Error(_) => None
    }
  | None => None
  };

let tokenToXtzTokenInputJs:
  (string, string, string, int) => Js.Nullable.t(string) = [%bs.raw
  {|
   function (xtzOut, xtzPool, tokenPool, decimals) {
     const dc = require('dexter-calculations');
     const b  = dc.tokenToXtzTokenInput(xtzOut, xtzPool, tokenPool, decimals);
     if (b === null) {
       return null;
     };
     return b.toString();
   }
  |}
];

let tokenToXtzTokenInput =
    (xtzOut: Tezos_Mutez.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : option(Tezos.Token.t) =>
  switch (
    Js.Nullable.toOption(
      tokenToXtzTokenInputJs(
        Tezos_Mutez.toString(xtzOut),
        Tezos_Mutez.toString(xtzPool),
        Tezos_Token.toStringPlain(tokenPool),
        tokenPool.decimals,
      ),
    )
  ) {
  | Some(str) =>
    switch (Tezos_Token.ofString(str, 0)) {
    | Belt.Result.Ok(t) => Some({...t, decimals: tokenPool.decimals})
    | Belt.Result.Error(_) => None
    }
  | None => None
  };

let tokenToXtzExchangeRateJs: (string, string, string) => Js.Nullable.t(float) = [%bs.raw
  {|
   function (xtzIn, xtzPool, tokenPool) {
     const dc = require('dexter-calculations');
     return dc.tokenToXtzExchangeRate(xtzIn, xtzPool, tokenPool);
   }
  |}
];

let tokenToXtzExchangeRate =
    (tokenIn: Tezos.Token.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : float => {
  switch (
    Js.Nullable.toOption(
      tokenToXtzExchangeRateJs(
        Tezos_Token.toStringPlain(tokenIn),
        Tezos_Mutez.toString(xtzPool),
        Tezos_Token.toStringPlain(tokenPool),
      ),
    )
  ) {
  | Some(result) => result
  | None => 0.0
  };
};

let tokenToXtzExchangeRateForDisplayJs:
  (string, string, string, int) => Js.Nullable.t(float) = [%bs.raw
  {|
   function (tokenIn, xtzPool, tokenPool, decimals) {
     const dc = require('dexter-calculations');
     return dc.tokenToXtzExchangeRateForDisplay(tokenIn, xtzPool, tokenPool, decimals);
   }
  |}
];

let tokenToXtzExchangeRateForDisplay =
    (tokenIn: Tezos.Token.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t) => {
  switch (
    Js.Nullable.toOption(
      tokenToXtzExchangeRateForDisplayJs(
        Tezos_Token.toStringPlain(tokenIn),
        Tezos_Mutez.toString(xtzPool),
        Tezos_Token.toStringPlain(tokenPool),
        tokenPool.decimals,
      ),
    )
  ) {
  | Some(result) => result
  | None => 0.0
  };
};

let tokenToXtzMarketRateJs: (string, string, int) => Js.Nullable.t(float) = [%bs.raw
  {|
   function (xtzPool, tokenPool, decimals) {
     const dc = require('dexter-calculations');
     return dc.tokenToXtzMarketRate(xtzPool, tokenPool, decimals);
   }
  |}
];

let tokenToXtzMarketRateForDisplay =
    (xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t): float =>
  switch (
    Js.Nullable.toOption(
      tokenToXtzMarketRateJs(
        Tezos_Mutez.toString(xtzPool),
        Tezos_Token.toStringPlain(tokenPool),
        tokenPool.decimals,
      ),
    )
  ) {
  | Some(p) => p
  | None => 0.0
  };

/**
 * Calculate the market rate of XTZ to token.
 */

let tokenToXtzPriceImpactJs: (string, string, string) => Js.Nullable.t(float) = [%bs.raw
  {|
   function (tokenIn, xtzPool, tokenPool) {
     const dc = require('dexter-calculations');
     return dc.tokenToXtzPriceImpact(tokenIn, xtzPool, tokenPool);
   }
  |}
];

let tokenToXtzPriceImpact =
    (tokenIn: Tezos.Token.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : option(float) =>
  Js.Nullable.toOption(
    tokenToXtzPriceImpactJs(
      Tezos_Token.toStringPlain(tokenIn),
      Tezos_Mutez.toString(xtzPool),
      Tezos_Token.toStringPlain(tokenPool),
    ),
  );

let tokenToXtzPriceImpactForDisplay =
    (tokenIn: Tezos.Token.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : option(float) => {
  switch (tokenToXtzPriceImpact(tokenIn, xtzPool, tokenPool)) {
  | Some(priceImpact) => Some(priceImpact *. 100.0)
  | None => None
  };
};

/**
 * Calculate the minimum amount of token the user should receive from an
 * exchange given the maximum amount of slippage they are willing to accept.
 */
let xtzToTokenMinimumTokenOutputJs: (string, float) => Js.Nullable.t(string) = [%bs.raw
  {|
   function (tokenOut, allowedSlippage) {
     const dc = require('dexter-calculations');
     const b  = dc.xtzToTokenMinimumTokenOutput(tokenOut, allowedSlippage);
     if (b === null) {
       return b.toString();
     };
     return b.toString();
   }
  |}
];

let xtzToTokenMinimumTokenOutput =
    (tokenOut: Tezos.Token.t, allowedSlippage: float) => {
  switch (
    Js.Nullable.toOption(
      xtzToTokenMinimumTokenOutputJs(
        Tezos_Token.toStringPlain(tokenOut),
        allowedSlippage *. 0.01,
      ),
    )
  ) {
  | Some(str) =>
    switch (Tezos_Token.ofString(str, 0)) {
    | Belt.Result.Ok(token) => {...token, decimals: tokenOut.decimals}
    | _ => Tezos.Token.one
    }
  | None => Tezos.Token.one
  };
};

/**
 * Calculate the minimum amount of XTZ the user should receive from an
 * exchange given the maximum amount of slippage they are willing to accept.
 */
let tokenToXtzMinimumXtzOutputJs: (string, float) => Js.Nullable.t(string) = [%bs.raw
  {|
   function (xtzOut, allowedSlippage) {
     const dc = require('dexter-calculations');
     const b  = dc.tokenToXtzMinimumXtzOutput(xtzOut, allowedSlippage);
     if (b === null) {
       return b.toString();
     };
     return b.toString();
   }
  |}
];

let tokenToXtzMinimumXtzOutput =
    (xtzOut: Tezos.Mutez.t, allowedSlippage: float) => {
  switch (
    Js.Nullable.toOption(
      tokenToXtzMinimumXtzOutputJs(
        Tezos_Mutez.toString(xtzOut),
        allowedSlippage *. 0.01,
      ),
    )
  ) {
  | Some(str) =>
    switch (Tezos_Mutez.ofString(str)) {
    | Belt.Result.Ok(token) => token
    | _ => Tezos.Mutez.one
    }
  | None => Tezos.Mutez.one
  };
};

/**
 * Calculate the transaction fee for token
 **/
let tokenFee = (tokenOut: Tezos.Token.t) => {
  Tezos.Token.div(
    Tezos.Token.mul(
      tokenOut,
      Tezos.Token.mkToken(Bigint.of_int(3), tokenOut.decimals),
    ),
    Tezos.Token.mkToken(Bigint.of_int(1000), tokenOut.decimals),
  );
};

/**
 * Calculate the transaction fee for xtz
 **/
let xtzFee = (xtzOut: Tezos.Mutez.t) => {
  Tezos.Mutez.div(
    Tezos.Mutez.mul(xtzOut, Tezos.Mutez.ofIntUnsafe(3)),
    Tezos.Mutez.ofIntUnsafe(1000),
  );
};

/**
 * Calculate the maximum amount of tokens the user will receive.
 **/
let tokenToTokenTokenOutputJs:
  (string, string, string, string, string) => Js.Nullable.t(string) = [%bs.raw
  {|
   function (tokenIn, xtzPool, tokenPool, xtzPool2, tokenPool2) {
     const dc = require('dexter-calculations');
     const b  = dc.tokenToTokenTokenOutput(tokenIn, xtzPool, tokenPool, xtzPool2, tokenPool2);
     if (b === null) {
       return null;
     };
     return b.toString();
   }
  |}
];

let tokenToTokenTokenOutput =
    (
      tokenIn: Tezos.Token.t,
      xtzPool: Tezos.Mutez.t,
      tokenPool: Tezos.Token.t,
      xtzPool2: Tezos.Mutez.t,
      tokenPool2: Tezos.Token.t,
    )
    : option(Tezos.Token.t) =>
  switch (
    Js.Nullable.toOption(
      tokenToTokenTokenOutputJs(
        Tezos_Token.toStringPlain(tokenIn),
        Tezos_Mutez.toString(xtzPool),
        Tezos_Token.toStringPlain(tokenPool),
        Tezos_Mutez.toString(xtzPool2),
        Tezos_Token.toStringPlain(tokenPool2),
      ),
    )
  ) {
  | Some(str) =>
    switch (Tezos_Token.ofString(str, 0)) {
    | Belt.Result.Ok(t) => Some({...t, decimals: tokenPool2.decimals})
    | Belt.Result.Error(_) => None
    }
  | None => None
  };

let tokenToTokenTokenInputJs:
  (string, string, string, int, string, string, int) => Js.Nullable.t(string) = [%bs.raw
  {|
   function (tokenOut, xtzPool, tokenPool, decimals, xtzPool2, tokenPool2, decimals2) {
    const dc = require('dexter-calculations');
    const b  = dc.tokenToTokenTokenInput(tokenOut, xtzPool, tokenPool, decimals, xtzPool2, tokenPool2, decimals2);
     if (b === null) {
       return null;
     };
     return b.toString();
   }
  |}
];

let tokenToTokenTokenInput =
    (
      tokenOut: Tezos.Token.t,
      xtzPool: Tezos.Mutez.t,
      tokenPool: Tezos.Token.t,
      xtzPool2: Tezos.Mutez.t,
      tokenPool2: Tezos.Token.t,
    )
    : option(Tezos.Token.t) =>
  switch (
    Js.Nullable.toOption(
      tokenToTokenTokenInputJs(
        Tezos_Token.toStringPlain(tokenOut),
        Tezos_Mutez.toString(xtzPool),
        Tezos_Token.toStringPlain(tokenPool),
        tokenPool.decimals,
        Tezos_Mutez.toString(xtzPool2),
        Tezos_Token.toStringPlain(tokenPool2),
        tokenPool2.decimals,
      ),
    )
  ) {
  | Some(str) =>
    switch (Tezos_Token.ofString(str, 0)) {
    | Belt.Result.Ok(t) => Some({...t, decimals: tokenPool.decimals})
    | Belt.Result.Error(_) => None
    }
  | None => None
  };

/**
 * Calculate the exchange rate of token to token with fees included.
 */
let tokenToTokenExchangeRateJs:
  (string, string, string, string, string) => Js.Nullable.t(float) = [%bs.raw
  {|
   function (tokenIn, xtzPool, tokenPool, xtzPool2, tokenPool2) {
     const dc = require('dexter-calculations');
     return dc.tokenToTokenExchangeRate(tokenIn, xtzPool, tokenPool, xtzPool2, tokenPool2);
   }
  |}
];

let tokenToTokenExchangeRate =
    (
      tokenIn: Tezos.Token.t,
      xtzPool: Tezos.Mutez.t,
      tokenPool: Tezos.Token.t,
      xtzPool2: Tezos.Mutez.t,
      tokenPool2: Tezos.Token.t,
    )
    : float => {
  switch (
    Js.Nullable.toOption(
      tokenToTokenExchangeRateJs(
        Tezos_Token.toStringPlain(tokenIn),
        Tezos_Mutez.toString(xtzPool),
        Tezos_Token.toStringPlain(tokenPool),
        Tezos_Mutez.toString(xtzPool2),
        Tezos_Token.toStringPlain(tokenPool2),
      ),
    )
  ) {
  | Some(result) => result
  | None => 0.0
  };
};

let tokenToTokenExchangeRateForDisplayJs:
  (string, string, string, int, string, string, int) => Js.Nullable.t(float) = [%bs.raw
  {|
   function (tokenIn, xtzPool, tokenPool, decimals, xtzPool2, tokenPool2, decimals2) {
     const dc = require('dexter-calculations');
     return dc.tokenToTokenExchangeRateForDisplay(tokenIn, xtzPool, tokenPool, decimals, xtzPool2, tokenPool2, decimals2);
   }
  |}
];

let tokenToTokenExchangeRateForDisplay =
    (
      tokenIn: Tezos.Token.t,
      xtzPool: Tezos.Mutez.t,
      tokenPool: Tezos.Token.t,
      xtzPool2: Tezos.Mutez.t,
      tokenPool2: Tezos.Token.t,
    )
    : float => {
  switch (
    Js.Nullable.toOption(
      tokenToTokenExchangeRateForDisplayJs(
        Tezos_Token.toStringPlain(tokenIn),
        Tezos_Mutez.toString(xtzPool),
        Tezos_Token.toStringPlain(tokenPool),
        tokenPool.decimals,
        Tezos_Mutez.toString(xtzPool2),
        Tezos_Token.toStringPlain(tokenPool2),
        tokenPool2.decimals,
      ),
    )
  ) {
  | Some(result) => result
  | None => 0.0
  };
};

let tokenToTokenExchangeRateJs:
  (string, string, string, string, string) => Js.Nullable.t(float) = [%bs.raw
  {|
   function (tokenIn, xtzPool, tokenPool, xtzPool2, tokenPool2) {
     const dc = require('dexter-calculations');
     return dc.tokenToTokenExchangeRate(tokenIn, xtzPool, tokenPool, xtzPool2, tokenPool2);
   }
  |}
];

let tokenToTokenPriceImpactJs:
  (string, string, string, string, string) => Js.Nullable.t(float) = [%bs.raw
  {|
   function (tokenIn, xtzPool, tokenPool, xtzPool2, tokenPool2) {
     const dc = require('dexter-calculations');
     return dc.tokenToTokenPriceImpact(tokenIn, xtzPool, tokenPool, xtzPool2, tokenPool2);
   }
  |}
];

let tokenToTokenPriceImpact =
    (
      tokenIn: Tezos.Token.t,
      xtzPool: Tezos.Mutez.t,
      tokenPool: Tezos.Token.t,
      xtzPool2: Tezos.Mutez.t,
      tokenPool2: Tezos.Token.t,
    )
    : option(float) =>
  Js.Nullable.toOption(
    tokenToTokenPriceImpactJs(
      Tezos_Token.toStringPlain(tokenIn),
      Tezos_Mutez.toString(xtzPool),
      Tezos_Token.toStringPlain(tokenPool),
      Tezos_Mutez.toString(xtzPool2),
      Tezos_Token.toStringPlain(tokenPool2),
    ),
  );

let tokenToTokenPriceImpactForDisplay =
    (
      tokenIn: Tezos.Token.t,
      xtzPool: Tezos.Mutez.t,
      tokenPool: Tezos.Token.t,
      xtzPool2: Tezos.Mutez.t,
      tokenPool2: Tezos.Token.t,
    )
    : option(float) => {
  tokenToTokenPriceImpact(tokenIn, xtzPool, tokenPool, xtzPool2, tokenPool2)
  |> Utils.map(priceImpact => priceImpact *. 100.0);
};

let tokenToTokenMarketRateJs:
  (string, string, int, string, string, int) => Js.Nullable.t(float) = [%bs.raw
  {|
   function (xtzPool, tokenPool, decimals, xtzPool2, tokenPool2, decimals2) {
     const dc = require('dexter-calculations');
     return dc.tokenToTokenMarketRate(xtzPool, tokenPool, decimals, xtzPool2, tokenPool2, decimals2);
   }
  |}
];

let tokenToTokenMarketRateForDisplay =
    (
      xtzPool: Tezos.Mutez.t,
      tokenPool: Tezos.Token.t,
      xtzPool2: Tezos.Mutez.t,
      tokenPool2: Tezos.Token.t,
    )
    : float =>
  switch (
    Js.Nullable.toOption(
      tokenToTokenMarketRateJs(
        Tezos_Mutez.toString(xtzPool),
        Tezos_Token.toStringPlain(tokenPool),
        tokenPool.decimals,
        Tezos_Mutez.toString(xtzPool2),
        Tezos_Token.toStringPlain(tokenPool2),
        tokenPool2.decimals,
      ),
    )
  ) {
  | Some(p) => p
  | None => 0.0
  };
