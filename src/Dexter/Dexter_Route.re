type liquidityRoute =
  | AddLiquidity
  | RemoveLiquidity
  | PoolData;

type t =
  | Exchange
  | LiquidityPool(liquidityRoute)
  | NotFound;

let toUrl = (t: t) =>
  switch (t) {
  | Exchange => "exchange"
  | LiquidityPool(liquidityRoute) =>
    switch (liquidityRoute) {
    | AddLiquidity => "liquidity/add"
    | RemoveLiquidity => "liquidity/remove"
    | PoolData => "liquidity/pool"
    }
  | NotFound => "404"
  };

let fromPath = (path: list(string)): t => {
  switch (path) {
  | []
  | ["exchange"] => Exchange
  | ["liquidity", submodule] =>
    switch (submodule) {
    | "add" => LiquidityPool(AddLiquidity)
    | "remove" => LiquidityPool(RemoveLiquidity)
    | "pool" => LiquidityPool(PoolData)
    | _ => NotFound
    }
  | _ => NotFound
  };
};

let getTokensHash = (t1: Dexter_Balance.t, t2: Dexter_Balance.t) => {
  (t1 |> Dexter_Balance.getSymbol) ++ "_" ++ (t2 |> Dexter_Balance.getSymbol);
};

let redirectToTokensHash = (t1: Dexter_Balance.t, t2: Dexter_Balance.t) => {
  "#" ++ getTokensHash(t1, t2) |> ReasonReactRouter.push;
};