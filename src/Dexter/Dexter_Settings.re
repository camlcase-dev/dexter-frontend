let exchanges: list(Dexter_Exchange.t) =
  switch (Deployment.get()) {
  | Production => [
      {
        name: "tzBTC",
        symbol: "tzBTC",
        decimals: 8,
        iconDark: "tzBTC-logo.png",
        iconLight: "tzBTC-logo.png",
        tokenContract:
          Tezos.Contract.ofString("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1BGQR7t4izzKZ7eRodKWTodAsM23P38v7N")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(541),
      },
      {
        name: "ETH Tez",
        symbol: "ETHtz",
        decimals: 18,
        iconDark: "ethtz-dark.png",
        iconLight: "ethtz-light.png",
        tokenContract:
          Tezos.Contract.ofString("KT19at7rQUvyjxnZ2fBv7D9zc8rkyG7gAoU8")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1PDrBE59Zmxnb8vXRgRAG1XmvTMTs5EDHU")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(542),
      },
      {
        name: "USD Tez",
        symbol: "USDtz",
        decimals: 6,
        iconDark: "usdtz-dark.png",
        iconLight: "usdtz-light.png",
        tokenContract:
          Tezos.Contract.ofString("KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1Tr2eG3eVmPRbymrbU2UppUmKjFPXomGG9")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(543),
      },
      {
        name: "Wrapped Tezos",
        symbol: "wXTZ",
        decimals: 6,
        iconDark: "wXTZ-logo.png",
        iconLight: "wXTZ-logo.png",
        tokenContract:
          Tezos.Contract.ofString("KT1VYsVfmobT7rsMVivvZ4J8i3bPiqz12NaH")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1D56HQfMmwdopmFLTwNHFJSs6Dsg2didFo")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(544),
      },
      {
        name: "Kolibri",
        symbol: "KUSD",
        decimals: 18,
        iconDark: "kolibri.png",
        iconLight: "kolibri.png",
        tokenContract:
          Tezos.Contract.ofString("KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1AbYeDbjjcAnV1QK7EZUUdqku77CdkTuv6")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(540),
      },
    ]
  | Development => [
      {
        name: "tzBTC",
        symbol: "tzBTC",
        decimals: 8,
        iconDark: "tzBTC-logo.png",
        iconLight: "tzBTC-logo.png",
        tokenContract:
          Tezos.Contract.ofString("KT1CUg39jQF8mV6nTMqZxjUUZFuz1KXowv3K")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1BYYLfMjufYwqFtTSYJND7bzKNyK7mjrjM")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(14986),
      },
      {
        name: "USD Tez",
        symbol: "USDtz",
        decimals: 6,
        iconDark: "USDtz-logo.png",
        iconLight: "USDtz-logo.png",
        tokenContract:
          Tezos.Contract.ofString("KT1FCMQk44tEP9fm9n5JJEhkSk1TW3XQdaWH")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1RfTPvrfAQDGAJ7wB71EtwxLQgjmfz59kE")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(14963),
      },
    ]
  | Next => [
      {
        name: "tzBTC",
        symbol: "tzBTC",
        decimals: 8,
        iconDark: "tzBTC-logo.png",
        iconLight: "tzBTC-logo.png",
        tokenContract:
          Tezos.Contract.ofString("KT1BVwiXfDdaXsvcmvSmBkpZt4vbGVhLmhBh")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1CUVF6urJ3zD2BjztL5vvneW1YiRfB2VdL")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(29238),
      },
      {
        name: "USD Tez",
        symbol: "USDtz",
        decimals: 6,
        iconDark: "USDtz-logo.png",
        iconLight: "USDtz-logo.png",
        tokenContract:
          Tezos.Contract.ofString("KT1AuVKbQZTAwA7mdS7TZSPZD9ZH1Eb62JFf")
          |> Common.unwrapResult,
        dexterContract:
          Tezos.Contract.ofString("KT1F77ZFv4cXu5nENdNvQVS42bw6ippkL4NR")
          |> Common.unwrapResult,
        dexterBigMapId: Tezos.BigMapId.ofInt(29237),
      },
    ]
  };

/** dependencies **/

let beaconNetwork: Beacon.Network.t =
  switch (Deployment.get()) {
  | Production => {
      type_: Beacon.Network.Type.Mainnet,
      name: None,
      rpcUrl: None,
    }
  | Development => {
      type_: Beacon.Network.Type.Custom,
      name: Some("edonet giganode"),
      rpcUrl: Some("https://edonet-tezos.giganode.io"),
    }
  | Next => {
      type_: Beacon.Network.Type.Custom,
      name: Some("florencenet giganode"),
      rpcUrl: Some("https://florencenet.smartpy.io"),
    }
  };

let bakingBadNetwork =
  switch (Deployment.get()) {
  | Production => "mainnet"
  | Development => "edo2net"
  | Next => "florencenet"
  };

let nodeUrl =
  switch (Deployment.get()) {
  | Production => "https://mainnet-tezos.giganode.io"
  | Development => "https://edonet-tezos.giganode.io"
  | Next => "https://florencenet.smartpy.io"
  };

let blockExplorerUrl =
  switch (Deployment.get()) {
  | Production => "https://tezblock.io/block/"
  | Development => "https://edonet.tezblock.io/block/"
  | Next => "https://florencenet.tezblock.io/block/"
  };

let transactionExplorerUrl =
  switch (Deployment.get()) {
  | Production => "https://tezblock.io/transaction/"
  | Development => "https://edonet.tezblock.io/transaction/"
  | Next => "https://florencenet.tezblock.io/transaction/"
  };

let tzktUrl =
  switch (Deployment.get()) {
  | Production => "https://tzkt.io/"
  | Development => "https://edo2net.tzkt.io/"
  | Next => "https://api.florencenet.tzkt.io/"
  };

let tzktApiUrl =
  switch (Deployment.get()) {
  | Production => "https://api.tzkt.io/v1/accounts/"
  | Development => "https://api.edo2net.tzkt.io/v1/accounts/"
  | Next => "https://api.florencenet.tzkt.io/v1/accounts/"
  };

type baker = {
  address: string,
  name: string,
};

let knownBakers: list(baker) = [
  {address: "tz1WCd2jm4uSt4vntk4vSuUWoZQGhLcDuR9q", name: "Happy Tezos"},
  {address: "tz1g8vkmcde6sWKaG2NN9WKzCkDM6Rziq194", name: "StakeNow"},
  {
    address: "tz1abmz7jiCV2GH2u81LRrGgAFFgvQgiDiaf",
    name: "Tessellated Geometry",
  },
];

let enableExchangeAndSend = false;
