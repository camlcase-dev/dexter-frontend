/**
 * A contract is a Tezos address that begins with KT1. It
 * may only contain bs58 characters and is 36 characters long. A contract
 * may hold XTZ and perform signed operations that are sent to it.
 * It cannot sign operations itself.
 */

type t =
  | Contract(string);

let encode = (t: t): Js.Json.t =>
  switch (t) {
  | Contract(c) => Json.Encode.string(c)
  };

let canDecodeBS58: string => bool = [%bs.raw
  {|
   function (value) {
     const bs58check = require('bs58check');
     if (bs58check.decodeUnsafe(value)) {
       return true;
     } else {
       return false;
     };
   }
  |}
];

let isContractString = (candidate: string): bool => {
  Js.String2.startsWith(candidate, "KT1")
  && String.length(candidate) == 36
  && canDecodeBS58(candidate);
};

let decode = (json: Js.Json.t): Belt.Result.t(t, string) =>
  switch (Json.Decode.string(json)) {
  | v =>
    if (isContractString(v)) {
      Belt.Result.Ok(Contract(v));
    } else {
      Belt.Result.Error(
        "Contract.decode failed: string is not an contract: " ++ v,
      );
    }

  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Contract.decode failed: " ++ error)
  };

let ofString = (candidate: string): Belt.Result.t(t, string) =>
  if (isContractString(candidate)) {
    Belt.Result.Ok(Contract(candidate));
  } else {
    Belt.Result.Error(
      "Contract.ofString: unexpected candidate string: " ++ candidate,
    );
  };

let toString = (t: t): string =>
  switch (t) {
  | Contract(str) => str
  };

let jsPack: string => string = [%bs.raw
  {|
   function (input) {
     // ed25519_public_key_hash
     const bs58check = require('bs58check');
     const elliptic  = require('elliptic');
     const prefix    = new Uint8Array([2, 90, 121]);
     const bytes     = '01' + elliptic.utils.toHex(bs58check.decode(input).slice(prefix.length)) + '00';
     const len = bytes.length / 2;
     const result = [];
     result.push('050a');
     result.push(len.toString(16).padStart(8, '0'));
     result.push(bytes);
     return result.join('');
   }
|}
];

let pack = (t: t): string =>
  switch (t) {
  | Contract(str) => jsPack(str)
  };

let jsToScriptExpr: string => string = [%bs.raw
  {|
   function (input) {
     const blake    = require('blakejs');
     const elliptic = require('elliptic');
     const bs58check = require('bs58check');

     const prefix = new Uint8Array([13, 44, 64, 27]);

     var a = [];
     for (var i = 0, len = input.length; i < len; i+=2) {
       a.push(parseInt(input.substr(i,2),16));
     }

     const hex2buf =  new Uint8Array(a);

     const blakeHash = blake.blake2b(hex2buf, null, 32);

     const payloadAr = typeof blakeHash === 'string' ? Uint8Array.from(Buffer.from(blakeHash, 'hex')) : blakeHash;

     const n = new Uint8Array(prefix.length + payloadAr.length);
     n.set(prefix);
     n.set(payloadAr, prefix.length);

     return bs58check.encode(Buffer.from(n.buffer));
   }
|}
];

let toScriptExpr = (t: t): string =>
  switch (t) {
  | Contract(str) => jsToScriptExpr(jsPack(str))
  };

type s = t;

module Comparable =
  Belt.Id.MakeComparable({
    type t = s;
    let cmp = (c0, c1) =>
      switch (c0, c1) {
      | (Contract(str0), Contract(str1)) => Pervasives.compare(str0, str1)
      };
  });

let equal = (t1: t, t2: t) => {
  switch (t1, t2) {
  | (Contract(t1), Contract(t2)) => t1 == t2
  };
};
