type t =
  | Beacon(Beacon.dappClient);

module Operation = {
  module Response = {
    type t =
      | Beacon(Beacon.Tezos.Operation.Response.t);

    let getTransactionHash = (t: t): string =>
      switch (t) {
      | Beacon(r) => r.transactionHash
      };
  };
};
