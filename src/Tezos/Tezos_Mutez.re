type t =
  | Mutez(Int64.t);

/**
 * common values
 */
let requiredXtzReserve = Mutez(300000L);
let zero = Mutez(Int64.zero);
let one = Mutez(Int64.one);
let succ = t =>
  switch (t) {
  | Mutez(t) => Mutez(Int64.add(t, 1L))
  };

let oneTez = Mutez(1000000L);
let succTez = t =>
  switch (t) {
  | Mutez(t) => Mutez(Int64.add(t, 1000000L))
  };

let icon = "tezos.png";

/**
 * of conversions
 */

let ofInt = (int: int): Belt.Result.t(t, string) =>
  if (int >= 0) {
    Belt.Result.Ok(Mutez(Int64.of_int(int)));
  } else {
    Belt.Result.Error("ofInt: Expected non-negative int.");
  };

let ofInt64 = (int: Int64.t): Belt.Result.t(t, string) =>
  if (Int64.compare(int, Int64.zero) >= 0) {
    Belt.Result.Ok(Mutez(int));
  } else {
    Belt.Result.Error("Mutez.mk");
  };

let ofFloat = (f: float): t => {
  let i = Int64.of_float(f);
  if (Int64.compare(i, 0L) >= 0) {
    Mutez(i);
  } else {
    Mutez(0L);
  };
};

let ofToken = (t: Tezos_Token.t): t =>
  if (t.decimals == 6) {
    Mutez(Bigint.to_int64(t.value));
  } else if (t.decimals < 6) {
    let diff = 6 - t.decimals;
    Mutez(Bigint.(to_int64(Bigint.mul(t.value, pow(of_int(10), diff)))));
  } else {
    let diff = t.decimals - 6;
    Mutez(Bigint.(to_int64(Bigint.mul(t.value, pow(of_int(10), diff)))));
  };

let ofString = (string: string): Belt.Result.t(t, string) =>
  switch (Tezos_Util.int64OfString(string)) {
  | Some(int64) =>
    switch (ofInt64(int64)) {
    | Belt.Result.Ok(mutez) => Belt.Result.Ok(mutez)
    | Belt.Result.Error(_error) =>
      Belt.Result.Error("ofString: expected a non-negative int64 value.")
    }
  | None =>
    Belt.Result.Error(
      "ofString: expected a non-negative int64 value encoded as a string.",
    )
  };

let ofTezString = (string: string): Belt.Result.t(t, string) =>
  switch (Tezos_Util.floatOfString(string)) {
  | Some(float) =>
    let decimalsCount = Common.getDecimalsCount(string);

    if (decimalsCount <= 6) {
      switch (
        ofString(
          float *. 1000000. |> Js.Float.toFixedWithPrecision(~digits=0),
        )
      ) {
      | Belt.Result.Ok(mutez) => Belt.Result.Ok(mutez)
      | Belt.Result.Error(_error) =>
        Belt.Result.Error("ofString: expected a non-negative int64 value.")
      };
    } else {
      Belt.Result.Error(
        "ofTezString: expected a non-negative float with precision up to 10^-6, received: "
        ++ string,
      );
    };
  | None =>
    Belt.Result.Error(
      "ofTezString: expected a non-negative float with precision up to 10^-6",
    )
  };

/**
 * UNSAFE of conversions
 */

let ofIntUnsafe = (int: int): t => Mutez(Int64.of_int(int));

let ofInt64Unsafe = (i: int64): t => Mutez(i);

let ofBigintUnsafe = (int: Bigint.t): t => Mutez(Bigint.to_int64(int));

/**
 * to conversions
 */

let toInt64 = (t: t): Int64.t =>
  switch (t) {
  | Mutez(int) => int
  };

let toBigint = (t: t): Bigint.t =>
  switch (t) {
  | Mutez(int) => Bigint.of_int64(int)
  };

/* the output value has zero decimal points */
let toMutezFloat = (t: t): float =>
  switch (t) {
  | Mutez(int) => Int64.to_float(int)
  };

/* the output value has six decimal points */
let toTezFloat = (t: t): float =>
  switch (t) {
  | Mutez(int) => Int64.to_float(int) /. 1000000.0
  };

let toString = (t: t): string =>
  switch (t) {
  | Mutez(int) => Int64.to_string(int)
  };

let toStringWithCommas = (t: t): string => Common.addCommas(toString(t));

let toTezString = (t: t): string =>
  switch (t) {
  | Mutez(int) => Js.Float.toString(Int64.to_float(int) /. 1000000.0)
  };

let toTezStringWithCommas = (t: t): string =>
  Common.addCommas(toTezString(t));

/**
 * arithmetic
 */

let add = (x, y) =>
  switch (x, y) {
  | (Mutez(x), Mutez(y)) => Mutez(Int64.add(x, y))
  };

let sub = (x, y) =>
  switch (x, y) {
  | (Mutez(x), Mutez(y)) => Mutez(Int64.sub(x, y))
  };

let mul = (x, y) =>
  switch (x, y) {
  | (Mutez(x), Mutez(y)) => Mutez(Int64.mul(x, y))
  };

let div = (x, y) =>
  switch (x, y) {
  | (Mutez(x), Mutez(y)) => Mutez(Int64.div(x, y))
  };

let calculateReward = (t: t): t => {
  mul(div(t, Mutez(100L)), Mutez(6L));
};

/**
 * comparison
 */

let compare = (x, y) =>
  switch (x, y) {
  | (Mutez(x), Mutez(y)) => Int64.compare(x, y)
  };

let equal = (x, y) => compare(x, y) == 0;

let leq = (x, y) => compare(x, y) < 1;

let geq = (x, y) => compare(x, y) > (-1);

let gt = (x, y) => compare(x, y) > 0;

let lt = (x, y) => compare(x, y) < 0;

let leqZero = x => compare(x, zero) <= 0;

let geqZero = x => compare(x, zero) >= 0;

let ltZero = x => compare(x, zero) < 0;

let gtZero = x => compare(x, zero) > 0;

let subPointThreePercent = x => {
  div(mul(mul(x, Mutez(1000L)), Mutez(997L)), Mutez(1000000L));
};

let addPointThreePercent = x => {
  div(mul(mul(x, Mutez(1000L)), Mutez(1003L)), Mutez(1000000L));
};

let userMax = x =>
  leqZero(sub(x, requiredXtzReserve)) ? zero : sub(x, requiredXtzReserve);

let min = (x, y): t =>
  switch (x, y) {
  | (Mutez(x), Mutez(y)) =>
    if (Int64.compare(x, y) < 0) {
      Mutez(x);
    } else {
      Mutez(y);
    }
  };

let max = (x, y): t =>
  switch (x, y) {
  | (Mutez(x), Mutez(y)) =>
    if (Int64.compare(x, y) > 0) {
      Mutez(x);
    } else {
      Mutez(y);
    }
  };

let toTezStringWithCommasTruncated = (decimals: int, t: t): string =>
  t |> toTezFloat |> Format.truncateDecimals(decimals);

/**
 * JSON encoding and decoding
 */

let encode = (t: t): Js.Json.t =>
  switch (t) {
  | Mutez(m) => Json.Encode.string(Int64.to_string(m))
  };

let decode = (json: Js.Json.t) =>
  switch (Json.Decode.string(json)) {
  | v =>
    switch (Tezos_Util.int64OfString(v)) {
    | Some(int) =>
      if (Int64.compare(int, Int64.zero) >= 0) {
        Belt.Result.Ok(Mutez(int));
      } else {
        Belt.Result.Error("Mutez.decode failed: " ++ v);
      }
    | None => Belt.Result.Error("Mutez.decode failed: " ++ v)
    }
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Mutez.decode failed: " ++ error)
  };
