type t =
  | Add
  | Le
  | Unit
  | Compare
  | Lambda
  | Loop
  | ImplicitAccount
  | None
  | Blake2B
  | Sha256
  | Xor
  | Rename
  | Map
  | SetDelegate
  | Dip
  | Pack
  | Size
  | IfCons
  | Lsr
  | TransferTokens
  | Update
  | Cdr
  | Swap
  | Some
  | Sha512
  | CheckSignature
  | Balance
  | EmptySet
  | Sub
  | Mem
  | Right
  | Address
  | Concat
  | Unpack
  | Not
  | Left
  | Amount
  | Drop
  | Abs
  | Ge
  | Push
  | Lt
  | Neq
  | Neg
  | Con
  | Exec
  | Apply
  | Cons
  | Nil
  | Isnat
  | Mul
  | LoopLeft
  | Ediv
  | Slice
  | StepsToQuota
  | Int
  | Source
  | Car
  | CreateAccount
  | Lsl
  | Or
  | IfNone
  | Self
  | If
  | Sender
  | Dup
  | Eq
  | Now
  | Get
  | Gt
  | IfLeft
  | Failwith
  | Pair
  | Iter
  | Cast
  | EmptyMap
  | CreateContract
  | HasKey
  | Contract
  | And;

let encode = (primitiveInstruction: t): Js.Json.t =>
  switch (primitiveInstruction) {
  | Add => Json.Encode.string("ADD")
  | Le => Json.Encode.string("LE")
  | Unit => Json.Encode.string("UNIT")
  | Compare => Json.Encode.string("COMPARE")
  | Lambda => Json.Encode.string("LAMBDA")
  | Loop => Json.Encode.string("LOOP")
  | ImplicitAccount => Json.Encode.string("IMPLICIT_ACCOUNT")
  | None => Json.Encode.string("NONE")
  | Blake2B => Json.Encode.string("BLAKE2B")
  | Sha256 => Json.Encode.string("SHA256")
  | Xor => Json.Encode.string("XOR")
  | Rename => Json.Encode.string("RENAME")
  | Map => Json.Encode.string("MAP")
  | SetDelegate => Json.Encode.string("SET_DELEGATE")
  | Dip => Json.Encode.string("DIP")
  | Pack => Json.Encode.string("PACK")
  | Size => Json.Encode.string("SIZE")
  | IfCons => Json.Encode.string("IF_CONS")
  | Lsr => Json.Encode.string("LSR")
  | TransferTokens => Json.Encode.string("TRANSFER_TOKENS")
  | Update => Json.Encode.string("UPDATE")
  | Cdr => Json.Encode.string("CDR")
  | Swap => Json.Encode.string("SWAP")
  | Some => Json.Encode.string("SOME")
  | Sha512 => Json.Encode.string("SHA512")
  | CheckSignature => Json.Encode.string("CHECK_SIGNATURE")
  | Balance => Json.Encode.string("BALANCE")
  | EmptySet => Json.Encode.string("EMPTY_SET")
  | Sub => Json.Encode.string("SUB")
  | Mem => Json.Encode.string("MEM")
  | Right => Json.Encode.string("RIGHT")
  | Address => Json.Encode.string("ADDRESS")
  | Concat => Json.Encode.string("CONTACT")
  | Unpack => Json.Encode.string("UNPACK")
  | Not => Json.Encode.string("NOT")
  | Left => Json.Encode.string("LEFT")
  | Amount => Json.Encode.string("AMOUNT")
  | Drop => Json.Encode.string("DROP")
  | Abs => Json.Encode.string("ABS")
  | Ge => Json.Encode.string("GE")
  | Push => Json.Encode.string("PUSH")
  | Lt => Json.Encode.string("LT")
  | Neq => Json.Encode.string("NEQ")
  | Neg => Json.Encode.string("NEG")
  | Con => Json.Encode.string("CON")
  | Exec => Json.Encode.string("EXEC")
  | Apply => Json.Encode.string("APPLY")
  | Cons => Json.Encode.string("CONS")
  | Nil => Json.Encode.string("NIL")
  | Isnat => Json.Encode.string("ISNAT")
  | Mul => Json.Encode.string("MUL")
  | LoopLeft => Json.Encode.string("LOOP_LEFT")
  | Ediv => Json.Encode.string("EDIV")
  | Slice => Json.Encode.string("SLICE")
  | StepsToQuota => Json.Encode.string("STEPS_TO_QUOTA")
  | Int => Json.Encode.string("INT")
  | Source => Json.Encode.string("SOURCE")
  | Car => Json.Encode.string("CAR")
  | CreateAccount => Json.Encode.string("CREATE_ACCOUNT")
  | Lsl => Json.Encode.string("LSL")
  | Or => Json.Encode.string("OR")
  | IfNone => Json.Encode.string("IF_NONE")
  | Self => Json.Encode.string("SELF")
  | If => Json.Encode.string("IF")
  | Sender => Json.Encode.string("SENDER")
  | Dup => Json.Encode.string("DUP")
  | Eq => Json.Encode.string("EQ")
  | Now => Json.Encode.string("NOW")
  | Get => Json.Encode.string("GET")
  | Gt => Json.Encode.string("GT")
  | IfLeft => Json.Encode.string("IF_LEFT")
  | Failwith => Json.Encode.string("FAILWITH")
  | Pair => Json.Encode.string("PAIR")
  | Iter => Json.Encode.string("ITER")
  | Cast => Json.Encode.string("CAST")
  | EmptyMap => Json.Encode.string("EMPTY_MAP")
  | CreateContract => Json.Encode.string("CREATE_CONTRACT")
  | HasKey => Json.Encode.string("HAS_KEY")
  | Contract => Json.Encode.string("CONTRACT")
  | And => Json.Encode.string("AND")
  };

let decode = (json: Js.Json.t): Belt.Result.t(t, string) =>
  switch (Json.Decode.string(json)) {
  | str =>
    switch (str) {
    | "ADD" => Belt.Result.Ok(Add)
    | "LE" => Belt.Result.Ok(Le)
    | "UNIT" => Belt.Result.Ok(Unit)
    | "COMPARE" => Belt.Result.Ok(Compare)
    | "LAMBDA" => Belt.Result.Ok(Lambda)
    | "LOOP" => Belt.Result.Ok(Loop)
    | "IMPLICIT_ACCOUNT" => Belt.Result.Ok(ImplicitAccount)
    | "NONE" => Belt.Result.Ok(None)
    | "BLAKE2B" => Belt.Result.Ok(Blake2B)
    | "SHA256" => Belt.Result.Ok(Sha256)
    | "XOR" => Belt.Result.Ok(Xor)
    | "RENAME" => Belt.Result.Ok(Rename)
    | "MAP" => Belt.Result.Ok(Map)
    | "SET_DELEGATE" => Belt.Result.Ok(SetDelegate)
    | "DIP" => Belt.Result.Ok(Dip)
    | "PACK" => Belt.Result.Ok(Pack)
    | "SIZE" => Belt.Result.Ok(Size)
    | "IF_CONS" => Belt.Result.Ok(IfCons)
    | "LSR" => Belt.Result.Ok(Lsr)
    | "TRANSFER_TOKENS" => Belt.Result.Ok(TransferTokens)
    | "UPDATE" => Belt.Result.Ok(Update)
    | "CDR" => Belt.Result.Ok(Cdr)
    | "SWAP" => Belt.Result.Ok(Swap)
    | "SOME" => Belt.Result.Ok(Some)
    | "SHA512" => Belt.Result.Ok(Sha512)
    | "CHECK_SIGNATURE" => Belt.Result.Ok(CheckSignature)
    | "BALANCE" => Belt.Result.Ok(Balance)
    | "EMPTY_SET" => Belt.Result.Ok(EmptySet)
    | "SUB" => Belt.Result.Ok(Sub)
    | "MEM" => Belt.Result.Ok(Mem)
    | "RIGHT" => Belt.Result.Ok(Right)
    | "ADDRESS" => Belt.Result.Ok(Address)
    | "CONCAT" => Belt.Result.Ok(Concat)
    | "UNPACK" => Belt.Result.Ok(Unpack)
    | "NOT" => Belt.Result.Ok(Not)
    | "LEFT" => Belt.Result.Ok(Left)
    | "AMOUNT" => Belt.Result.Ok(Amount)
    | "DROP" => Belt.Result.Ok(Drop)
    | "ABS" => Belt.Result.Ok(Abs)
    | "TE" => Belt.Result.Ok(Ge)
    | "PUSH" => Belt.Result.Ok(Push)
    | "LT" => Belt.Result.Ok(Lt)
    | "NEQ" => Belt.Result.Ok(Neq)
    | "NEG" => Belt.Result.Ok(Neg)
    | "CON" => Belt.Result.Ok(Con)
    | "EXEC" => Belt.Result.Ok(Exec)
    | "APPLY" => Belt.Result.Ok(Apply)
    | "CONS" => Belt.Result.Ok(Cons)
    | "NIL" => Belt.Result.Ok(Nil)
    | "ISNAT" => Belt.Result.Ok(Isnat)
    | "MUL" => Belt.Result.Ok(Mul)
    | "LOOP_LEFT" => Belt.Result.Ok(LoopLeft)
    | "EDIV" => Belt.Result.Ok(Ediv)
    | "SLICE" => Belt.Result.Ok(Slice)
    | "STEPS_TO_QUOTA" => Belt.Result.Ok(StepsToQuota)
    | "INT" => Belt.Result.Ok(Int)
    | "SOURCE" => Belt.Result.Ok(Source)
    | "CAR" => Belt.Result.Ok(Car)
    | "CREATE_ACCOUNT" => Belt.Result.Ok(CreateAccount)
    | "LSL" => Belt.Result.Ok(Lsl)
    | "OR" => Belt.Result.Ok(Or)
    | "IF_NONE" => Belt.Result.Ok(IfNone)
    | "SELF" => Belt.Result.Ok(Self)
    | "IF" => Belt.Result.Ok(If)
    | "SENDER" => Belt.Result.Ok(Sender)
    | "DUP" => Belt.Result.Ok(Dup)
    | "EQ" => Belt.Result.Ok(Eq)
    | "NOW" => Belt.Result.Ok(Now)
    | "GET" => Belt.Result.Ok(Get)
    | "GT" => Belt.Result.Ok(Gt)
    | "IF_LEFT" => Belt.Result.Ok(IfLeft)
    | "FAILWITH" => Belt.Result.Ok(Failwith)
    | "PAIR" => Belt.Result.Ok(Pair)
    | "ITER" => Belt.Result.Ok(Iter)
    | "CAST" => Belt.Result.Ok(Cast)
    | "EMPTY_MAP" => Belt.Result.Ok(EmptyMap)
    | "CREATE_CONTRACT" => Belt.Result.Ok(CreateContract)
    | "HAS_KEY" => Belt.Result.Ok(HasKey)
    | "CONTRACT" => Belt.Result.Ok(Contract)
    | "AND" => Belt.Result.Ok(And)
    | _ => Belt.Result.Error("PrimitiveInstruction.decode: " ++ str)
    }
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("PrimitiveInstruction.decode: " ++ error)
  };
