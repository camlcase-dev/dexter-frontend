// Use dry run to create an error and retrieve data from a contract

type t = {
  kind: string,
  id: string,
  location: int,
  with_: Tezos_Expression.t,
};

let decode = (json: Js.Json.t): Belt.Result.t(t, string) =>
  switch (
    Json.Decode.(
      field("kind", string, json),
      field("id", string, json),
      field("location", int, json),
      field(
        "with",
        a => Tezos_Util.unwrapResult(Tezos_Expression.decode(a)),
        json,
      ),
    )
  ) {
  | (kind, id, location, with_) =>
    Belt.Result.Ok({kind, id, location, with_})
  | exception (Json.Decode.DecodeError(err)) => Belt.Result.Error(err)
  };
