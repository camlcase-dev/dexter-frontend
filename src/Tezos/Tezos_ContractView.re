// perform a dry run call to get the view data from a contract
open Tezos_Expression;

let pushZeroMutez =
  Tezos_Primitives.(
    singleExpression(
      ~prim=instr(Tezos_PrimitiveInstruction.Push),
      ~args=[
        singleExpression(~prim=type_(Tezos_PrimitiveType.Mutez), ()),
        IntExpression(Bigint.zero),
      ],
      (),
    )
  );

let pushAddress = address =>
  Tezos_Primitives.(
    singleExpression(
      ~prim=instr(Tezos_PrimitiveInstruction.Push),
      ~args=[
        singleExpression(~prim=type_(Tezos_PrimitiveType.Address), ()),
        StringExpression(address),
      ],
      (),
    )
  );

let pushUnit = () =>
  Tezos_Primitives.(
    singleExpression(
      ~prim=instr(Tezos_PrimitiveInstruction.Push),
      ~args=[
        singleExpression(~prim=type_(Tezos_PrimitiveType.Unit), ()),
        singleExpression(~prim=data(Tezos_PrimitiveData.Unit), ()),
      ],
      (),
    )
  );

let ifNoneFail = message =>
  Tezos_Primitives.(
    singleExpression(
      ~prim=instr(Tezos_PrimitiveInstruction.IfNone),
      ~args=[
        expressions([
          singleExpression(
            ~prim=instr(Tezos_PrimitiveInstruction.Push),
            ~args=[
              singleExpression(~prim=type_(Tezos_PrimitiveType.String), ()),
              StringExpression(message),
            ],
            (),
          ),
          singleExpression(
            ~prim=instr(Tezos_PrimitiveInstruction.Failwith),
            (),
          ),
        ]),
        expressions([]),
      ],
      (),
    )
  );

let dipPushNilOperation =
  Tezos_Primitives.(
    singleExpression(
      ~prim=instr(Tezos_PrimitiveInstruction.Dip),
      ~args=[
        expressions([
          singleExpression(
            ~prim=instr(Tezos_PrimitiveInstruction.Nil),
            ~args=[
              singleExpression(
                ~prim=type_(Tezos_PrimitiveType.Operation),
                (),
              ),
            ],
            (),
          ),
        ]),
      ],
      (),
    )
  );

let dipPushUnit =
  Tezos_Primitives.(
    singleExpression(
      ~prim=instr(Tezos_PrimitiveInstruction.Dip),
      ~args=[
        expressions([
          singleExpression(~prim=instr(Tezos_PrimitiveInstruction.Unit), ()),
        ]),
      ],
      (),
    )
  );

let getView =
    (
      ~sourceContract: string,
      ~viewContract: string,
      ~entrypoint: string,
      ~viewQueryData: list(Tezos_Expression.t),
      ~viewQueryType: Tezos_Expression.t,
      ~viewReturnType: Tezos_Expression.t,
      (),
    ) =>
  Tezos_Primitives.(
    expressions([
      pushZeroMutez,
      singleExpression(
        ~prim=instr(Tezos_PrimitiveInstruction.None),
        ~args=[
          singleExpression(~prim=type_(Tezos_PrimitiveType.KeyHash), ()),
        ],
        (),
      ),
      singleExpression(
        ~prim=instr(Tezos_PrimitiveInstruction.CreateContract),
        ~args=[
          expressions([
            singleExpression(
              ~prim=type_(Tezos_PrimitiveType.Parameter),
              ~args=[
                singleExpression(~prim=type_(Tezos_PrimitiveType.Nat), ()),
              ],
              (),
            ),
            singleExpression(
              ~prim=type_(Tezos_PrimitiveType.Storage),
              ~args=[
                singleExpression(~prim=type_(Tezos_PrimitiveType.Unit), ()),
              ],
              (),
            ),
            singleExpression(
              ~prim=type_(Tezos_PrimitiveType.Code),
              ~args=[
                expressions([
                  singleExpression(
                    ~prim=instr(Tezos_PrimitiveInstruction.Failwith),
                    (),
                  ),
                ]),
              ],
              (),
            ),
          ]),
        ],
        (),
      ),
      singleExpression(
        ~prim=instr(Tezos_PrimitiveInstruction.Dip),
        ~args=[
          expressions([
            singleExpression(
              ~prim=instr(Tezos_PrimitiveInstruction.Dip),
              ~args=[
                expressions([
                  singleExpression(
                    ~prim=instr(Tezos_PrimitiveInstruction.Lambda),
                    ~args=[
                      singleExpression(
                        ~prim=type_(Tezos_PrimitiveType.Pair),
                        ~args=[
                          singleExpression(
                            ~prim=type_(Tezos_PrimitiveType.Address),
                            (),
                          ),
                          singleExpression(
                            ~prim=type_(Tezos_PrimitiveType.Unit),
                            (),
                          ),
                        ],
                        (),
                      ),
                      singleExpression(
                        ~prim=type_(Tezos_PrimitiveType.Pair),
                        ~args=[
                          singleExpression(
                            ~prim=type_(Tezos_PrimitiveType.List),
                            ~args=[
                              singleExpression(
                                ~prim=type_(Tezos_PrimitiveType.Operation),
                                (),
                              ),
                            ],
                            (),
                          ),
                          singleExpression(
                            ~prim=type_(Tezos_PrimitiveType.Unit),
                            (),
                          ),
                        ],
                        (),
                      ),
                      expressions(
                        List.concat([
                          [
                            singleExpression(
                              ~prim=instr(Tezos_PrimitiveInstruction.Car),
                              (),
                            ),
                            singleExpression(
                              ~prim=
                                instr(Tezos_PrimitiveInstruction.Contract),
                              ~args=[
                                singleExpression(
                                  ~prim=type_(Tezos_PrimitiveType.Nat),
                                  (),
                                ),
                              ],
                              (),
                            ),
                            ifNoneFail("a"),
                          ],
                          viewQueryData,
                          [
                            singleExpression(
                              ~prim=instr(Tezos_PrimitiveInstruction.Pair),
                              (),
                            ),
                            singleExpression(
                              ~prim=instr(Tezos_PrimitiveInstruction.Dip),
                              ~args=[
                                expressions([
                                  pushAddress(sourceContract),
                                  singleExpression(
                                    ~prim=
                                      instr(
                                        Tezos_PrimitiveInstruction.Contract,
                                      ),
                                    ~args=[
                                      singleExpression(
                                        ~prim=type_(Tezos_PrimitiveType.Pair),
                                        ~args=[viewQueryType, viewReturnType],
                                        (),
                                      ),
                                    ],
                                    ~annots=[entrypoint],
                                    (),
                                  ),
                                  ifNoneFail("b"),
                                  pushZeroMutez,
                                ]),
                              ],
                              (),
                            ),
                            singleExpression(
                              ~prim=
                                instr(
                                  Tezos_PrimitiveInstruction.TransferTokens,
                                ),
                              (),
                            ),
                            dipPushNilOperation,
                            singleExpression(
                              ~prim=instr(Tezos_PrimitiveInstruction.Cons),
                              (),
                            ),
                            dipPushUnit,
                            singleExpression(
                              ~prim=instr(Tezos_PrimitiveInstruction.Pair),
                              (),
                            ),
                          ],
                        ]),
                      ),
                    ],
                    (),
                  ),
                ]),
              ],
              (),
            ),
            /* body of first DIP */
            singleExpression(
              ~prim=instr(Tezos_PrimitiveInstruction.Apply),
              (),
            ),
            singleExpression(
              ~prim=instr(Tezos_PrimitiveInstruction.Dip),
              ~args=[
                expressions([
                  pushAddress(viewContract),
                  singleExpression(
                    ~prim=instr(Tezos_PrimitiveInstruction.Contract),
                    ~args=[
                      singleExpression(
                        ~prim=type_(Tezos_PrimitiveType.Lambda),
                        ~args=[
                          singleExpression(
                            ~prim=type_(Tezos_PrimitiveType.Unit),
                            (),
                          ),
                          singleExpression(
                            ~prim=type_(Tezos_PrimitiveType.Pair),
                            ~args=[
                              singleExpression(
                                ~prim=type_(Tezos_PrimitiveType.List),
                                ~args=[
                                  singleExpression(
                                    ~prim=
                                      type_(Tezos_PrimitiveType.Operation),
                                    (),
                                  ),
                                ],
                                (),
                              ),
                              singleExpression(
                                ~prim=type_(Tezos_PrimitiveType.Unit),
                                (),
                              ),
                            ],
                            (),
                          ),
                        ],
                        (),
                      ),
                    ],
                    (),
                  ),
                  ifNoneFail("c"),
                  pushZeroMutez,
                ]),
              ],
              (),
            ),
            singleExpression(
              ~prim=instr(Tezos_PrimitiveInstruction.TransferTokens),
              (),
            ),
            dipPushNilOperation,
            singleExpression(
              ~prim=instr(Tezos_PrimitiveInstruction.Cons),
              (),
            ),
          ]),
        ],
        (),
      ),
      singleExpression(~prim=instr(Tezos_PrimitiveInstruction.Cons), ()),
      dipPushUnit,
      singleExpression(~prim=instr(Tezos_PrimitiveInstruction.Pair), ()),
    ])
  );

let getAllowanceExpression =
    (
      ~sourceContract: string,
      ~viewContract: string,
      ~owner: string,
      ~sender: string,
      (),
    ) =>
  Tezos_Primitives.(
    getView(
      ~sourceContract,
      ~viewContract,
      ~entrypoint="%getAllowance",
      ~viewQueryData=[
        pushAddress(owner),
        pushAddress(sender),
        singleExpression(~prim=instr(Tezos_PrimitiveInstruction.Pair), ()),
      ],
      ~viewQueryType=
        singleExpression(
          ~prim=type_(Tezos_PrimitiveType.Pair),
          ~args=[
            singleExpression(~prim=type_(Tezos_PrimitiveType.Address), ()),
            singleExpression(~prim=type_(Tezos_PrimitiveType.Address), ()),
          ],
          (),
        ),
      ~viewReturnType=
        singleExpression(
          ~prim=type_(Tezos_PrimitiveType.Contract),
          ~args=[
            singleExpression(~prim=type_(Tezos_PrimitiveType.Nat), ()),
          ],
          (),
        ),
      (),
    )
  );

let getBalance =
    (~sourceContract: string, ~viewContract: string, ~owner: string, ()) =>
  Tezos_Primitives.(
    getView(
      ~sourceContract,
      ~viewContract,
      ~entrypoint="%getBalance",
      ~viewQueryData=[pushAddress(owner)],
      ~viewQueryType=
        singleExpression(~prim=type_(Tezos_PrimitiveType.Address), ()),
      ~viewReturnType=
        singleExpression(
          ~prim=type_(Tezos_PrimitiveType.Contract),
          ~args=[
            singleExpression(~prim=type_(Tezos_PrimitiveType.Nat), ()),
          ],
          (),
        ),
      (),
    )
  );

let getTotalSupply = (~sourceContract: string, ~viewContract: string, ()) =>
  Tezos_Primitives.(
    getView(
      ~sourceContract,
      ~viewContract,
      ~entrypoint="%getTotalSupply",
      ~viewQueryData=[pushUnit()],
      ~viewQueryType=
        singleExpression(~prim=type_(Tezos_PrimitiveType.Unit), ()),
      ~viewReturnType=
        singleExpression(
          ~prim=type_(Tezos_PrimitiveType.Contract),
          ~args=[
            singleExpression(~prim=type_(Tezos_PrimitiveType.Nat), ()),
          ],
          (),
        ),
      (),
    )
  );

let getAllowancePayload =
    (
      ~block: string,
      ~chainId: string,
      ~source: string,
      ~counter: Bigint.t,
      ~sourceContract: string,
      ~viewContract: string,
      ~owner: string,
      ~sender: string,
      (),
    )
    : Tezos_DryRun.dryRunTransaction => {
  chainId,
  operation: {
    branch: block,
    signature: "edsigtXomBKi5CTRf5cjATJWSyaRvhfYNHqSUGrn4SdbYRcGwQrUGjzEfQDTuqHhuA8b2d8NarZjz8TRf65WkpQmo423BtomS8Q",
    contents: [
      {
        kind: "transaction",
        source,
        fee: Tezos_Mutez.zero,
        counter,
        gasLimit: Bigint.of_string("1040000"),
        storageLimit: Bigint.of_string("60000"),
        amount: Tezos_Mutez.zero,
        destination: viewContract,
        parameters: {
          entrypoint: "default",
          value:
            getAllowanceExpression(
              ~sourceContract,
              ~viewContract,
              ~owner,
              ~sender,
              (),
            ),
        },
      },
    ],
  },
};

let decode = (json: Js.Json.t): Belt.Result.t('a, string) =>
  switch (Tezos_DryRun.decodeDryRunResponse(json)) {
  | Belt.Result.Ok(r) =>
    switch (Belt.List.head(r.contents)) {
    | Some(metadata) =>
      switch (Belt.List.head(metadata.internalOperationResults)) {
      | Some(internalOperationResult) =>
        switch (Belt.List.head(internalOperationResult.result.errors)) {
        | Some(error) =>
          switch (error.with_) {
          | SingleExpression(
              Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
              Some([Tezos_Expression.IntExpression(allowance), _]),
              _,
            ) =>
            Belt.Result.Ok(allowance)
          | _ => Belt.Result.Error("no balance")
          }

        | None => Belt.Result.Error("no response metadata")
        }

      | None => Belt.Result.Error("no response metadata")
      }
    | None => Belt.Result.Error("no response results")
    }
  | Belt.Result.Error(err) => Belt.Result.Error(err)
  };

let getAllowance =
    (
      ~nodeUrl: string,
      ~block: string,
      ~chainId: string,
      ~source: string,
      ~counter: Bigint.t,
      ~sourceContract: string,
      ~viewContract: string,
      ~owner: string,
      ~sender: string,
      (),
    )
    : Js.Promise.t(Belt.Result.t(Bigint.t, string)) =>
  Common.fetch(
    ~decode,
    ~identifier="Tezos_ContractView.getAllowance",
    ~url=nodeUrl ++ "/chains/main/blocks/head/helpers/scripts/run_operation",
    ~requestInit=
      Fetch.RequestInit.make(
        ~method_=Post,
        ~credentials=Include,
        ~mode=NoCORS,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~body=
          Tezos_DryRun.encodeDryRunTransaction(
            getAllowancePayload(
              ~block,
              ~chainId,
              ~source,
              ~counter,
              ~sourceContract,
              ~viewContract,
              ~owner,
              ~sender,
              (),
            ),
          )
          |> Js.Json.stringify
          |> Fetch.BodyInit.make,
        (),
      ),
    ~service=Service.TezosNode,
    (),
  );
