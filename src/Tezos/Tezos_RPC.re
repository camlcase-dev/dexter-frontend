type tezosNodeRoute =
  | Balance(string, string, string)
  | BigMap(string, string, Tezos_BigMapId.t, string)
  | HeadBlockId(string)
  | Block(string, string)
  | Checkpoint(string)
  | Storage(string, string, string)
  | Delegate(string, string, string)
  | NetworkVersion;

let toRoute = (baseUrl: string, route: tezosNodeRoute) =>
  baseUrl
  ++ (
    switch (route) {
    | Balance(chainId, blockId, contract) =>
      "/chains/"
      ++ chainId
      ++ "/blocks/"
      ++ blockId
      ++ "/context/contracts/"
      ++ contract
      ++ "/balance"
    | BigMap(chainId, blockId, BigMapId(bigMapId), scriptExpr) =>
      "/chains/"
      ++ chainId
      ++ "/blocks/"
      ++ blockId
      ++ "/context/big_maps/"
      ++ string_of_int(bigMapId)
      ++ "/"
      ++ scriptExpr
    | Checkpoint(chainId) => "/chains/" ++ chainId ++ "/checkpoint"
    | HeadBlockId(chainId) => "/chains/" ++ chainId ++ "/blocks"
    | Block(chainId, blockId) =>
      "/chains/" ++ chainId ++ "/blocks/" ++ blockId
    | Storage(chainId, blockId, contract) =>
      "/chains/"
      ++ chainId
      ++ "/blocks/"
      ++ blockId
      ++ "/context/contracts/"
      ++ contract
      ++ "/storage"

    | Delegate(chainId, blockId, contract) =>
      "/chains/"
      ++ chainId
      ++ "/blocks/"
      ++ blockId
      ++ "/context/contracts/"
      ++ contract
      ++ "/delegate"
    | NetworkVersion => "/network/version"
    }
  );

let getBalance =
    (baseUrl, chainId, blockId, contract)
    : Js.Promise.t(Belt.Result.t(Tezos_Mutez.t, string)) =>
  Common.fetch(
    ~decode=Tezos_Mutez.decode,
    ~identifier="Tezos_RPC.getBalance",
    ~url=toRoute(baseUrl, Balance(chainId, blockId, contract)),
    ~requestInit=
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        ~mode=NoCORS,
        (),
      ),
    ~service=Service.TezosNode,
    (),
  );

let decodeBigMapValue = json => {
  switch (Tezos_Expression.decode(json)) {
  | Belt.Result.Ok(expression) => Belt.Result.Ok(Some(expression))
  | Belt.Result.Error(err) => Belt.Result.Error(err)
  };
};

/**
 * Three things can occur:
 * the key has an entry in the big map, it returns the value with a 200 status
 * the key does not have an entry in the big map, it returns nothing with a 404 status
 * a network error or query (incorrect key or format) error.
 */
let getBigMapValue =
    (
      baseUrl,
      chainId,
      blockId,
      bigMapId: Tezos_BigMapId.t,
      scriptExpr: string,
    )
    : Js.Promise.t(Belt.Result.t(option(Tezos_Expression.t), string)) =>
  Common.fetchOpt(
    ~decode=decodeBigMapValue,
    ~identifier="Tezos_RPC.getBigMapValue",
    ~url=toRoute(baseUrl, BigMap(chainId, blockId, bigMapId, scriptExpr)),
    ~requestInit=
      Fetch.RequestInit.make(
        ~method_=Get,
        ~mode=NoCORS,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    ~service=Service.TezosNode,
    (),
  );

let getCheckpoint =
    (baseUrl, chainId)
    : Js.Promise.t(Belt.Result.t(Tezos_Checkpoint.t, string)) =>
  Common.fetch(
    ~decode=Tezos_Checkpoint.decode,
    ~identifier="Tezos_RPC.getCheckpoint",
    ~url=toRoute(baseUrl, Checkpoint(chainId)),
    ~requestInit=
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~mode=NoCORS,
        ~credentials=Omit,
        (),
      ),
    ~service=Service.TezosNode,
    (),
  );

let getFirst = (xss: list(list(string))): option(string) => {
  switch (Belt.List.head(xss)) {
  | Some(xs) => Belt.List.head(xs)
  | None => None
  };
};

let decodeHeadBlockId = json => {
  switch (Json.Decode.(list(a => list(string, a), json))) {
  | blocks =>
    switch (getFirst(blocks)) {
    | Some(block) => Belt.Result.Ok(block)
    | None =>
      Belt.Result.Error("Tezos_RPC.getHeadBlockId return an empty list.")
    }

  | exception (Json.Decode.DecodeError(err)) => Belt.Result.Error(err)
  };
};

let getHeadBlockId =
    (baseUrl, chainId): Js.Promise.t(Belt.Result.t(string, string)) =>
  Common.fetch(
    ~decode=decodeHeadBlockId,
    ~identifier="Tezos_RPC.getHeadBlockId",
    ~url=toRoute(baseUrl, HeadBlockId(chainId)),
    ~requestInit=
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    ~service=Service.TezosNode,
    (),
  );

let getBlock =
    (baseUrl, chainId, blockId)
    : Js.Promise.t(Belt.Result.t(Tezos_Block.t, string)) =>
  Common.fetch(
    ~decode=Tezos_Block.decode,
    ~identifier="Tezos_RPC.getBlock",
    ~url=toRoute(baseUrl, Block(chainId, blockId)),
    ~requestInit=
      Fetch.RequestInit.make(
        ~method_=Get,
        ~mode=NoCORS,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    ~service=Service.TezosNode,
    (),
  );

let getHeadBlock =
    (baseUrl, chainId): Js.Promise.t(Belt.Result.t(Tezos_Block.t, string)) => {
  getHeadBlockId(baseUrl, chainId)
  |> Js.Promise.then_(result =>
       switch (result) {
       | Belt.Result.Ok(blockId) => getBlock(baseUrl, chainId, blockId)
       | Belt.Result.Error(err) =>
         Belt.Result.Error(err) |> Js.Promise.resolve
       }
     );
};

let getTimestamp =
    (baseUrl): Js.Promise.t(Belt.Result.t(Tezos_Timestamp.t, string)) => {
  getHeadBlock(baseUrl, "main")
  |> Js.Promise.then_(result =>
       switch (result) {
       | Belt.Result.Ok((block: Tezos_Block.t)) =>
         Belt.Result.Ok(block.header.timestamp) |> Js.Promise.resolve
       | Belt.Result.Error(err) =>
         Belt.Result.Error(err) |> Js.Promise.resolve
       }
     );
};

let getStorage =
    (baseUrl, chainId, blockId, contract)
    : Js.Promise.t(Belt.Result.t(Tezos_Expression.t, string)) =>
  Common.fetch(
    ~decode=Tezos_Expression.decode,
    ~identifier="Tezos_RPC.getStorage",
    ~url=toRoute(baseUrl, Storage(chainId, blockId, contract)),
    ~requestInit=
      Fetch.RequestInit.make(
        ~method_=Get,
        ~mode=NoCORS,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    ~service=Service.TezosNode,
    (),
  );

let decodeDelegate = json =>
  switch (Tezos_Address.decode(json)) {
  | Belt.Result.Ok(ok) => Belt.Result.Ok(Some(ok))
  | Belt.Result.Error(error) => Belt.Result.Error(error)
  };

let getDelegate =
    (baseUrl, chainId, blockId, contract)
    : Js.Promise.t(Belt.Result.t(option(Tezos_Address.t), string)) =>
  Common.fetchOpt(
    ~decode=decodeDelegate,
    ~identifier="Tezos_RPC.getDelegate",
    ~url=toRoute(baseUrl, Delegate(chainId, blockId, contract)),
    ~requestInit=
      Fetch.RequestInit.make(
        ~method_=Get,
        ~mode=NoCORS,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    ~service=Service.TezosNode,
    (),
  );

let decodeNetworkVersion = json => {
  Json.Decode.(
    switch (field("chain_name", string, json)) {
    | chainName => Belt.Result.Ok(chainName)
    | exception (DecodeError(error)) => Belt.Result.Error(error)
    }
  );
};

let getNetworkVersion =
    (baseUrl): Js.Promise.t(Belt.Result.t(string, string)) =>
  Common.fetch(
    ~decode=decodeNetworkVersion,
    ~identifier="Tezos_RPC.getNetworkVersion",
    ~url=toRoute(baseUrl, NetworkVersion),
    ~requestInit=
      Fetch.RequestInit.make(
        ~method_=Get,
        ~mode=NoCORS,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    ~service=Service.TezosNode,
    (),
  );
