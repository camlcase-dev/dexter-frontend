type t =
  | Production
  | Development
  | Next;

let ofRaw: unit => string = [%bs.raw
  {|
     function () {
        return "production";
        if (PROD) {
          return "production";
        } else if (NEXT) {
          return "next"
        } else {
          return "development";
        }
      }
    |}
];

let get = () => 
  switch (ofRaw()) {
    | "production" => Production
    | "next" => Next
    | _ => Development
  };
