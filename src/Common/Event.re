type dom;
type listener = Dom.event => unit;

external keyboardEvent: Dom.event => ReactEvent.Keyboard.t = "%identity";

[@bs.val] external document: dom = "document";

[@bs.send] external jsCreateEvent: (dom, string) => Dom.event = "createEvent";

[@bs.send]
external jsInitEvent: (Dom.event, string, bool, bool) => unit = "initEvent";

[@bs.send]
external jsAddEventListener: (dom, string, listener, bool) => listener =
  "addEventListener";

[@bs.send]
external jsRemoveEventListener: (dom, string, listener) => unit =
  "removeEventListener";

[@bs.send]
external jsDispatchEvent: (dom, Dom.event) => unit = "dispatchEvent";

[@bs.set] external jsAttachPayload: (Dom.event, Js.Json.t) => unit = "payload";

[@bs.get]
external jsUnsafeGetEventPayload: Dom.event => Js.null_undefined(Js.Json.t) =
  "payload";

let emit = (event: Dom.event): unit => jsDispatchEvent(document, event);

/* We return the same listener to easily remove it later using `off` */
let on = (eventType: string, listener: listener): listener => {
  jsAddEventListener(document, eventType, listener, false) |> ignore;
  listener;
};

let off = (eventType: string, listener: listener): unit =>
  jsRemoveEventListener(document, eventType, listener);

let createGlobalListener =
    (eventType: string, listener: listener): (unit => unit) => {
  jsAddEventListener(document, eventType, listener, false) |> ignore;
  () => jsRemoveEventListener(document, eventType, listener);
};

let createEvent = (eventName: string, payload: option(Js.Json.t)) => {
  let ev = jsCreateEvent(document, "Event");
  jsInitEvent(ev, eventName, true, true);
  switch (payload) {
  | None => ev
  | Some(p) =>
    jsAttachPayload(ev, p);
    ev;
  };
};

let getPayload = (event: Dom.event, parseFunc: Js.Json.t => 'a): option('a) => {
  let payload = jsUnsafeGetEventPayload(event) |> Js.toOption;
  let result =
    switch (payload) {
    | None => None
    | Some(json) =>
      switch (parseFunc(json)) {
      | obj => Some(obj)
      | exception _ => None
      }
    };
  result;
};
