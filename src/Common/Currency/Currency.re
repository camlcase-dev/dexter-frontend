type t = {
  decimals: int,
  name: string,
  prefixed: bool,
  rate: float,
  sign: option(string),
  symbol: string,
};

let default = "USD";

let popular: array(string) = [|
  "USD",
  "EUR",
  "GBP",
  "JPY",
  "RUB",
  "BTC",
  "ETH",
|];

let toStringWithCommas = (currency: t, tezFloat: float): string => {
  let symbol = currency.sign |> Utils.orDefault(" " ++ currency.symbol);
  let valueInCurrency = tezFloat *. currency.rate;
  let lowestValueWithDecimals =
    Common.getLowestValueWithDecimalsFloat(currency.decimals);

  (
    valueInCurrency > 0. && valueInCurrency < lowestValueWithDecimals ? "<" : ""
  )
  ++ (currency.prefixed ? symbol : "")
  ++ (
    valueInCurrency
    |> Format.truncateDecimals(~sign=false, ~round=false, currency.decimals)
    |> Common.addCommas
  )
  ++ (!currency.prefixed ? symbol : "");
};

let findBySymbolOrDefault = (symbol: string, currencies: list(t)): option(t) =>
  switch (
    currencies |> List.find_opt((currency: t) => currency.symbol === symbol)
  ) {
  | Some(current) => Some(current)
  | _ =>
    currencies |> List.find_opt((currency: t) => currency.symbol === default)
  };
