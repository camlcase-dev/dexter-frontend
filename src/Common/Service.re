exception DecodeError(string);

type t =
  | BakingBad
  | CoinGecko
  | TezosNode
  | TZKT;

let toString = (t: t) =>
  switch (t) {
  | BakingBad => "BakingBad"
  | CoinGecko => "CoinGecko"
  | TezosNode => "TezosNode"
  | TZKT => "TZKT"
  };

let encode = (t: t): Js.Json.t => Json.Encode.string(t |> toString);

let decode = (json: Js.Json.t): t => {
  switch (json |> Json.Decode.string) {
  | "BakingBad" => BakingBad
  | "CoinGecko" => CoinGecko
  | "TezosNode" => TezosNode
  | "TZKT" => TZKT
  | service => raise @@ DecodeError("Expected Service, got " ++ service)
  };
};
