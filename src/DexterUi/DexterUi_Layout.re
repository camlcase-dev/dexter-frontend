module Style = {
  open Css;

  let message = style([display(`flex), alignItems(`center), justifyContent(`center), paddingBottom(`px(10))]);
};
                

[@react.component]
let make = () => {
  let {isMd, isSm} = DexterUiContext.Responsive.useDevice();
  let {darkMode} = DexterUiContext.Theme.useContext();

  <Flex
    flexDirection=`column
    alignItems=`center
    background={darkMode ? Colors.offBlack : Colors.offWhite}
    minHeight={`vh(100.)}>
    <DexterUi_NavBar />
    <div className=Style.message>
      <DexterUi_Icon size=29 name="alert" />
      {React.string("Dexter is no longer maintained by camlCase. Please remove your liquidity as soon as possible. Do not add more liquidity.")}
    </div>
    <Flex
      maxWidth={`px(1024)}
      width={`percent(100.)}
      pt={darkMode ? `px(20) : `zero}
      pb={`px(isMd ? 12 : 24)}
      px={`px(isSm ? 16 : 20)}
      flexShrink=0.
      overflowY=`auto
      flexDirection={isMd ? `column : `row}>
      <Flex
        flexDirection={isMd ? `row : `column}
        width={isMd ? `percent(100.) : `px(328)}>
        <DexterUi_Wallet />
        {(isMd && !isSm) |> Utils.renderIf(<Flex width={`px(16)} />)}
        {!isSm |> Utils.renderIf(<DexterUi_Transactions />)}
        {!isMd |> Utils.renderIf(<DexterUi_Version />)}
      </Flex>
      <Flex
        flexDirection=`column
        width={isMd ? `percent(100.) : `px(640)}
        flexGrow=1.
        ml={isMd ? `zero : `px(16)}
        borderRadius={`px(16)}>
        <DexterUi_Content />
        {isSm |> Utils.renderIf(<DexterUi_Transactions />)}
        <DexterUi_Footer />
      </Flex>
    </Flex>
  </Flex>;
};
