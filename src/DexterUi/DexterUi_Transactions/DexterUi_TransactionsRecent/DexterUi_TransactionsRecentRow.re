let getTransactionDescription =
    (account: Dexter_Account.t, transaction: Dexter_Transaction.t)
    : React.element => {
  let transactionTitle =
    switch (transaction.transactionType) {
    | Exchange(transactionType) =>
      transactionType.destination |> Tezos.Address.isEqual(account.address)
        ? "Exchange" : "Exchange & Send"
    | AddLiquidity(_) => "Add"
    | RemoveLiquidity(_) => "Remove"
    };

  let transactionStatus =
    switch (transaction.status) {
    | Pending => " (Pending)"
    | Failed(_) => " (Failed)"
    | _ => ""
    };

  let transactionDetails =
    switch (transaction.transactionType) {
    | Exchange(transactionType) =>
      (
        transactionType.valueIn
        |> InputType.toFloat
        |> Format.truncateDecimals(6)
        |> Common.addCommas
      )
      ++ " "
      ++ transactionType.symbolIn
      ++ " for "
      ++ (
        transactionType.valueOut
        |> InputType.toFloat
        |> Format.truncateDecimals(6)
        |> Common.addCommas
      )
      ++ " "
      ++ transactionType.symbolOut
    | AddLiquidity(transactionType) =>
      (
        transactionType.xtzIn
        |> Tezos.Mutez.toTezFloat
        |> Format.truncateDecimals(6)
        |> Common.addCommas
      )
      ++ " XTZ + "
      ++ (
        transactionType.tokenIn
        |> Tezos.Token.toFloatWithDecimal
        |> Format.truncateDecimals(6)
        |> Common.addCommas
      )
      ++ " "
      ++ transactionType.symbol
    | RemoveLiquidity(transactionType) =>
      (transactionType.lqtBurned |> Tezos.Token.toStringWithCommas)
      ++ " "
      ++ "XTZ/"
      ++ transactionType.symbol
      ++ " pool tokens"
    };

  let fontStyle = transaction.status === Pending ? Some(`italic) : None;

  <>
    <Text
      ?fontStyle
      textStyle=TextStyles.bold
      lightModeColor=Colors.darkGrey2
      spaceAfter=true
      whiteSpace=`nowrap>
      {transactionTitle ++ transactionStatus ++ ":" |> React.string}
    </Text>
    <Text ?fontStyle ellipsis=true whiteSpace=`nowrap>
      {transactionDetails |> React.string}
    </Text>
  </>;
};

[@react.component]
let make = (~account: Dexter_Account.t, ~transaction: Dexter_Transaction.t) =>
  <Flex
    className=Css.(
      style([
        opacity(
          switch (transaction.status) {
          | Failed(_) => 0.5
          | _ => 1.
          },
        ),
      ])
    )
    pb={`px(12)}
    mr={`px(4)}
    flexShrink=0.>
    <a
      className=Css.(
        style([
          maxWidth(`percent(100.)),
          textDecoration(`none),
          flexGrow(1.),
        ])
      )
      target="_blank"
      href={Dexter_Settings.transactionExplorerUrl ++ transaction.hash}>
      <Flex alignItems=`center>
        <DexterUi_TransactionIcon
          transactionType={
            switch (transaction.transactionType) {
            | Exchange(transactionType) =>
              transactionType.destination
              |> Tezos.Address.isEqual(account.address)
                ? Exchange : ExchangeAndSend
            | AddLiquidity(_) => AddLiquidity
            | RemoveLiquidity(_) => RemoveLiquidity
            }
          }
        />
        <Flex flexShrink=0. width={`px(12)} />
        {getTransactionDescription(account, transaction)}
        <Flex ml={`px(18)} pb={`px(2)} flexGrow=1. justifyContent=`flexEnd>
          <DexterUi_Icon size=11 name="external-link" />
        </Flex>
      </Flex>
    </a>
  </Flex>;
