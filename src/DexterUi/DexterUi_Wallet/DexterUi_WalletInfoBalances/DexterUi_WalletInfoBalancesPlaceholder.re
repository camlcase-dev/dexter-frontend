type t = {
  symbol: string,
  amount: string,
};

[@react.component]
let make = () => {
  // let {isXxs} = DexterUiContext.Responsive.useDevice();
  // Use, if we decide to make it possible to connect wallet on mobile
  // mt={`px(isXxs ? 63 : 26)}
  <Flex
    mt={`px(26)}
    className=Css.(style([opacity(0.25)]))
    width={`percent(100.)}
    flexDirection=`column>
    <Text lightModeColor=Colors.blackish1 textStyle=TextStyles.bold>
      {"Token balances" |> React.string}
    </Text>
    <Flex
      mb={`px(-16)}
      pb={`px(3)}
      flexDirection=`column
      overflowY=`auto
      height={`px(76)}>
      {Belt.Array.range(0, 2)
       |> Array.map((i: int) =>
            <DexterUi_WalletInfoBalancesRow
              key={i |> string_of_int}
              withBorder={i < 2}
              symbol={Common.nbsp |> React.string}
              amount={Common.nbsp |> React.string}
            />
          )
       |> React.array}
    </Flex>
  </Flex>;
};
