[@react.component]
let make = () => {
  let {isSm} = DexterUiContext.Responsive.useDevice();
  let {balances} = DexterUiContext.Account.useContext();
  let {route, setRoute} = DexterUiContext.Route.useContext();
  let areTransactionsDisable =
    DexterUiContext.Services.areTransactionsDisable();
  let unavailableServices = (
    DexterUiContext.Services.isUnavailable(TezosNode),
    DexterUiContext.Services.isUnavailable(BakingBad),
  );

  <DexterUi_Box
    minHeight={isSm ? `px(314) : `px(514)}
    p={`px(isSm ? 16 : 20)}
    mb={isSm ? `px(16) : `zero}
    overflowY=`visible
    tabs={
      <Flex>
        <DexterUi_Tab
          isActive={route === Dexter_Route.Exchange}
          onClick={_ => setRoute(Dexter_Route.Exchange, None)}
          tabText="Exchange"
          iconName="exchange-tab"
        />
        <DexterUi_Tab
          isActive={
            switch (route) {
            | LiquidityPool(_) => true
            | _ => false
            }
          }
          onClick={_ =>
            setRoute(Dexter_Route.LiquidityPool(AddLiquidity), None)
          }
          tabText="Liquidity Pool"
          iconName="pool-tab"
        />
      </Flex>
    }>
    {switch (areTransactionsDisable || balances |> List.length === 0, route) {
     | (false, Exchange) => <DexterUi_Exchange />
     | (false, LiquidityPool(_)) => <DexterUi_LiquidityPool />
     | _ =>
       <Flex justifyContent=`center alignItems=`center full=true flexGrow=1.>
         <DexterUi_Loader
           text=?{
             switch (unavailableServices) {
             | (true, _) => Some("Tezos node unavailable")
             | (_, true) => Some("Baking Bad api unavailable")
             | _ => None
             }
           }
           subText=?{
             areTransactionsDisable
               ? Some("Attempting to reconnect...") : None
           }
         />
       </Flex>
     }}
  </DexterUi_Box>;
};
