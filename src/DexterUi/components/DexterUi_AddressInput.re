[@react.component]
let make =
    (
      ~isDisabled: bool=false,
      ~isValid: bool=true,
      ~value: string,
      ~setValue: string => unit,
    ) => {
  let {darkMode} = DexterUiContext.Theme.useContext();

  let isInvalid =
    !isValid
    || value !== ""
    && value
    |> Tezos.Address.ofString
    |> Belt.Result.isError;

  <DexterUi_SecondaryInput
    disabled=isDisabled
    icon={
      <Text
        fontSize={`px(11)} color={darkMode ? Colors.white : Colors.lightGrey}>
        <Flex
          onClick={_ =>
            value === ""
              ? Common.paste()
                |> Js.Promise.then_(t => setValue(t) |> Js.Promise.resolve)
                |> ignore
              : setValue("")
          }>
          {value == ""
             ? <i className="icon-paste" /> : <i className="icon-cancel" />}
        </Flex>
      </Text>
    }
    inputBorderColor=?{isInvalid ? Some(Colors.red) : None}
    placeholder="Paste address here"
    onChange={event => {
      let newAddress = Common.eventToValue(event);
      setValue(newAddress);
    }}
    pr={`px(22)}
    value
    width={`px(244)}
  />;
};
