[@react.component]
let make = () =>
  <Flex mt={`px(6)} justifyContent=`center>
    <DexterUi_Panel variant=Blue>
      {"A wallet balance of "
       ++ Tezos.Mutez.(requiredXtzReserve |> toTezStringWithCommas)
       ++ " XTZ is needed to cover network fees for this operation."
       |> React.string}
    </DexterUi_Panel>
  </Flex>;
