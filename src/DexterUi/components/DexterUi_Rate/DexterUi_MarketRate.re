/**
 * Display the market rate (does not fluctuate with the trade size)
 * Applicable for Add and Remove liquidity.
 */

let display =
    (xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t, symbol: string) =>
  if (Tezos.Mutez.leqZero(xtzPool) || Tezos.Token.leqZero(tokenPool)) {
    "No exchange rate";
  } else {
    let rate =
      Dexter_Exchange.xtzToTokenMarketRateForDisplay(xtzPool, tokenPool);

    let lowestValue = Common.getLowestValueWithDecimalsFloat(6);
    let sign = rate < lowestValue ? " < " : " = ";
    let rate = rate |> Format.truncateDecimals(~sign=false, 6);

    rate ++ " " ++ symbol ++ sign ++ "1 XTZ";
  };

[@react.component]
let make =
    (~xtzPool: Tezos.Mutez.t, ~tokenPool: Tezos.Token.t, ~symbol: string) =>
  <DexterUi_Rate>
    {display(xtzPool, tokenPool, symbol) |> React.string}
  </DexterUi_Rate>;
