[@react.component]
let make = (~children: React.element) =>
  <Flex justifyContent=`center mt={`px(12)}>
    <Flex
      className=Css.(style([border(px(1), `solid, Colors.lightGrey)]))
      px={`px(12)}
      py={`px(4)}
      borderRadius={`px(12)}>
      <Text
        textStyle=TextStyles.bold
        darkModeColor=Colors.offWhite
        lightModeColor=Colors.darkGrey2>
        children
      </Text>
    </Flex>
  </Flex>;
