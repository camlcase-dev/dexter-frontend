[@react.component]
let make = (~popular: bool=false, ~value: string) => {
  let {currencies} = DexterUiContext.Currencies.useContext();
  let value = value |> Js.String.toLowerCase;

  let filteredCurrencies =
    currencies
    |> List.filter((currency: Currency.t) =>
         Currency.popular
         |> Js.Array.includes(currency.symbol) === popular
         && (
           value === ""
           || currency.symbol
           |> Js.String.toLowerCase
           |> Js.String.includes(value)
           || currency.name
           |> Js.String.toLowerCase
           |> Js.String.includes(value)
         )
       );

  filteredCurrencies
  |> List.length > 0
  |> Utils.renderIf(
       <>
         <Flex ml={`px(5)} mb={`px(5)} mt={`px(6)} flexShrink=0.>
           <Text color=Colors.blackish1 textStyle=TextStyles.bold>
             {(popular ? "Popular currencies" : "Other currencies (A-Z)")
              |> React.string}
           </Text>
         </Flex>
         {filteredCurrencies
          |> List.map(currency =>
               <DexterUi_CurrencySwitchListOption
                 key={currency.symbol}
                 currency
               />
             )
          |> Array.of_list
          |> React.array}
       </>,
     );
};
