[@react.component]
let make = () => {
  let (showModal, setShowModal) = React.useState(_ => false);

  DexterUiContext.Services.exist()
  |> Utils.renderIf(
       <>
         {showModal
          |> Utils.renderIf(
               <DexterUi_Dialog
                 onClose={_ => setShowModal(_ => false)}
                 px={`px(24)}
                 width={`px(366)}
                 withPaddingBottom=true>
                 <DexterUi_Message
                   title={"Cannot reach connected service" |> React.string}
                   subtitle={
                     "The Dexter front-end cannot communicate with an important connected service. We hope to restore access shortly."
                     |> React.string
                   }
                 />
               </DexterUi_Dialog>,
             )}
         <Flex
           ml={`px(12)}
           alignItems=`center
           onClick={_ => setShowModal(_ => true)}>
           <DexterUi_Icon size=29 name="alert" />
         </Flex>
       </>,
     );
};
