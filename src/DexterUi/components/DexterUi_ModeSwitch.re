let inactiveStyles =
  Css.[selector("> *", [filter([`brightness(100.), `grayscale(100.)])])];

[@react.component]
let make = () => {
  let {isSm} = DexterUiContext.Responsive.useDevice();
  let {darkMode, setDarkMode} = DexterUiContext.Theme.useContext();

  <Flex
    background=Colors.lightGrey1
    height={`px(28)}
    width={`px(55)}
    borderRadius={`px(14)}
    alignItems=`center
    justifyContent=`spaceBetween
    px={`px(6)}
    ml={isSm ? `zero : `px(20)}
    onClick={_ => setDarkMode(!darkMode)}
    position=`relative>
    <Flex
      background=Colors.offWhite
      width={`px(22)}
      height={`px(22)}
      borderRadius={`px(14)}
      position=`absolute
      left={darkMode ? `px(30) : `px(3)}
      top={`px(3)}
    />
    <Flex className=Css.(style(darkMode ? inactiveStyles : [])) zIndex=2>
      <DexterUi_Icon name="light" size=16 />
    </Flex>
    <Flex className=Css.(style(darkMode ? [] : inactiveStyles)) zIndex=2>
      <DexterUi_Icon name="dark" size=16 />
    </Flex>
  </Flex>;
};
