[@react.component]
let make = (~blue: bool=false) => {
  let {isSm} = DexterUiContext.Responsive.useDevice();

  <Flex
    justifyContent=`center
    pt={isSm ? `px(16) : `zero}
    pb={isSm ? `px(4) : `zero}
    width={isSm ? `percent(100.) : `px(28)}
    minHeight={isSm ? `initial : `percent(100.)}
    position=`relative>
    <Flex position={isSm ? `initial : `absolute} top={`px(isSm ? 0 : 128)}>
      <DexterUi_Icon
        darkModeSuffix="-d"
        size={isSm ? 28 : 18}
        name={blue ? "and-blue" : "and"}
      />
    </Flex>
  </Flex>;
};
