/**
 * An input box for XTZ or tokens that belong to the logged in account.
 * The current value may be invalid.
 */

[@react.component]
let make =
    (
      ~aboveLimitNote: string="Insufficient balance.",
      ~currencyValue: option(React.element)=?,
      ~disabled: bool=false,
      ~limit: option(InputType.t)=?,
      ~onClickMax: option(unit => unit)=?,
      ~token: Dexter_Balance.t, /* all blockchain details about the account */
      ~updateValue: Valid.t(InputType.t) => unit,
      ~value: Valid.t(InputType.t),
      ~withCurrencyValue: bool=true,
      ~xtzReserveNote: option(string)=?,
    ) => {
  let {account}: DexterUiContext.Account.t =
    DexterUiContext.Account.useContext();
  let isLoggedIn = DexterUi_Hooks.useIsLoggedIn();

  let decimals =
    limit
    |> Utils.map(limit => limit |> InputType.getDecimals)
    |> Utils.orDefault(token |> Dexter_Balance.getDecimals);

  let currencyValue =
    switch (currencyValue, value) {
    | (Some(currencyValue), _) => currencyValue
    | (_, Valid(value, _)) =>
      <DexterUi_CurrencyValue
        token=?{
          switch (token) {
          | ExchangeBalance(token) => Some(token)
          | _ => None
          }
        }
        value
      />
    | _ => <DexterUi_CurrencyValue />
    };

  let isValid =
    switch (value, token) {
    | (Valid(Mutez(value), _), XtzBalance(xtz)) =>
      switch (limit, isLoggedIn) {
      | (Some(Mutez(limit)), _) => Tezos.Mutez.leq(value, limit)
      | (_, true) => Tezos.Mutez.leq(value, Tezos.Mutez.userMax(xtz))
      | _ => true
      }
    | (Valid(Token(value), _), ExchangeBalance(token)) =>
      switch (limit, isLoggedIn) {
      | (Some(Token(limit)), _) => Tezos.Token.leq(value, limit)
      | (_, true) => Tezos.Token.leq(value, token.tokenBalance)
      | _ => true
      }
    | (InvalidInitialState, _) => true
    | _ => false
    };

  let note =
    xtzReserveNote
    |> Utils.flatMap(xtzReserveNote =>
         switch (account, value) {
         | (Some(account), Valid(Mutez(xtzValue), _))
             when
               Tezos.Mutez.(
                 geq(xtzValue, userMax(account.xtz))
                 && leq(xtzValue, account.xtz)
                 && gtZero(xtzValue)
               ) =>
           Some(xtzReserveNote)
         | _ => None
         }
       );

  <>
    <TokenInput
      currencyValue=?{withCurrencyValue ? Some(currencyValue) : None}
      disabled
      isValid
      onChange={v =>
        switch (v) {
        | "" => updateValue(InvalidInitialState)
        | value =>
          let value = value |> Format.truncateDecimalsFloat(decimals);

          switch (token) {
          | XtzBalance(_) =>
            switch (Tezos.Mutez.ofTezString(value)) {
            | Belt.Result.Ok(i) => updateValue(Valid(Mutez(i), value))
            | Belt.Result.Error(_) => updateValue(Invalid(value))
            }
          | ExchangeBalance(_) =>
            switch (Tezos.Token.ofString(value, decimals)) {
            | Belt.Result.Ok(i) => updateValue(Valid(Token(i), value))
            | Belt.Result.Error(_) => updateValue(Invalid(value))
            }
          };
        }
      }
      ?onClickMax
      value={
        switch (value) {
        | Valid(_, valueString) => valueString
        | Invalid(err) => err
        | InvalidInitialState => ""
        }
      }
    />
    {switch (note, aboveLimitNote, isValid, value) {
     | (Some(note), _, _, _)
     | (_, note, false, Valid(_)) =>
       <Flex position=`relative height={`px(12)}>
         <Flex mt={`px(5)} ml={`px(4)} position=`absolute>
           <Text color=?{isValid ? None : Some(Colors.red)}>
             {note |> React.string}
           </Text>
         </Flex>
       </Flex>
     | _ => React.null
     }}
  </>;
};
