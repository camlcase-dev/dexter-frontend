[@react.component]
let make =
    (
      ~background: option(Flex.backgroundType)=?,
      ~children: React.element,
      ~height: option(Flex.heightType)=?,
      ~mb: option(Flex.marginType)=?,
      ~minHeight: option(Flex.heightType)=?,
      ~maxHeight: option(Flex.maxHeightType)=?,
      ~overflowY: Css.Types.Overflow.t=`auto,
      ~p: Css.Types.Length.t=`px(16),
      ~tabs: React.element=React.null,
      ~width: option(Flex.widthType)=?,
    ) => {
  let {darkMode} = DexterUiContext.Theme.useContext();

  <Flex
    background={
      background |> Utils.orDefault(Colors.boxBackground(darkMode))
    }
    ?height
    ?minHeight
    ?maxHeight
    ?mb
    ?width
    flexGrow=1.
    flexDirection=`column
    borderRadius={`px(16)}
    position=`relative>
    <Flex
      full=true
      flexDirection=`column
      overflow=`hidden
      flexShrink=0.
      className=Css.(
        style([
          borderTopLeftRadius(`px(16)),
          borderTopRightRadius(`px(16)),
        ])
      )>
      tabs
    </Flex>
    <Flex p overflowY width={`percent(100.)} flexGrow=1.> children </Flex>
  </Flex>;
};
