module Style = DexterUi_Button_Style;

type t =
  | Primary
  | Secondary;

[@react.component]
let make =
    (
      ~children: React.element,
      ~darkModeEnabled: bool=true,
      ~disabled: bool=false,
      ~onClick=?,
      ~px: option(Css.Types.Length.t)=?,
      ~small: bool=false,
      ~variant: t=Primary,
      ~width: option(Flex.widthType)=?,
    ) => {
  let {darkMode} = DexterUiContext.Theme.useContext();

  <Flex
    disabled
    justifyContent=`center
    ?onClick
    ?px
    ?width
    className={
      switch (variant) {
      | Primary => Style.primary(darkMode && darkModeEnabled, small)
      | Secondary => Style.secondary
      }
    }>
    children
  </Flex>;
};
