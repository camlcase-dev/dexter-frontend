[@react.component]
let make = (~className: option(string)=?, ~src: string) => {
  let baseUrl = Utils.useBaseUrl();

  <img ?className src={baseUrl ++ "img/" ++ src} />;
};
