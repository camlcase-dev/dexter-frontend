[@react.component]
let make =
    (
      ~children: React.element,
      ~disabled: bool,
      ~onClick: _ => unit,
      ~px: Css.Types.Length.t,
    ) => {
  let isTransactionPending = DexterUi_Hooks.useIsTransactionPending();
  let hasInsufficientXtz = DexterUi_Hooks.useHasInsufficientXtz();
  let transactionsDisabled = DexterUiContext.Services.areTransactionsDisable();

  let disabled =
    disabled
    || isTransactionPending
    || hasInsufficientXtz
    || transactionsDisabled;

  <Flex flexGrow=1. justifyContent=`center mt={`px(10)}>
    <DexterUi_Button px disabled onClick=?{disabled ? None : Some(onClick)}>
      children
    </DexterUi_Button>
  </Flex>;
};
