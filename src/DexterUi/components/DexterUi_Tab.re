type t =
  | Primary
  | Secondary;

[@react.component]
let make =
    (
      ~iconName: option(string)=?,
      ~isActive: bool=false,
      ~onClick: _ => unit,
      ~tabText: string,
      ~variant: t=Primary,
      ~withBorder: bool=false,
    ) => {
  let {isXxs} = DexterUiContext.Responsive.useDevice();
  let {darkMode} = DexterUiContext.Theme.useContext();
  let color = darkMode ? Colors.white : Colors.blackish1;
  let tabBorderColor = darkMode ? Colors.tabInactiveDark : Colors.darkBlueGrey;
  let contentOpacity = isActive ? 1. : 0.5;
  let imgFilter =
    isActive
      ? []
      : darkMode
          ? [`brightness(0.), `invert(100.)]
          : [`brightness(30.), `grayscale(100.)];

  let tabBackground =
    switch (variant) {
    | Primary =>
      switch (isActive, darkMode) {
      | (true, true) => Colors.boxBackgroundDark
      | (true, false) => Colors.white
      | (false, true) => Colors.tabInactiveDark
      | (false, false) => Colors.blueGrey
      }
    | Secondary =>
      switch (isActive, darkMode) {
      | (true, true) => Colors.secondaryTabActiveDark
      | (true, false) => Colors.white
      | (false, true) => Colors.blackish3
      | (false, false) => Colors.blueGrey
      }
    };

  <Flex
    className=Css.(
      merge([
        style([
          selector("> *", [opacity(contentOpacity)]),
          selector("img", [filter(imgFilter)]),
          hover([selector("> *", [opacity(1.)])]),
          borderRight(px(1), withBorder ? `solid : `none, tabBorderColor),
          borderLeft(px(1), withBorder ? `solid : `none, tabBorderColor),
        ]),
      ])
    )
    flexGrow=1.
    flexShrink=1.
    flexBasis=`zero
    height={`px(36)}
    alignItems=`center
    justifyContent=`center
    background=tabBackground
    onClick>
    {switch (isXxs, iconName) {
     | (false, Some(iconName)) =>
       <> <DexterUi_Icon name=iconName size=14 /> <Flex width={`px(7)} /> </>
     | _ => React.null
     }}
    <Text
      className=Css.(
        style([
          before([
            display(`block),
            contentRule(`text(tabText)),
            fontWeight(`bold),
            height(`zero),
            overflow(`hidden),
            visibility(`hidden),
          ]),
        ])
      )
      textStyle=TextStyles.h4
      color
      fontWeight={isActive ? `bold : `normal}>
      {tabText |> React.string}
    </Text>
  </Flex>;
};
