[@react.component]
let make = (~darkModeSuffix: option(string)=?, ~size: int=12, ~name: string) => {
  let baseUrl = Utils.useBaseUrl();
  let {darkMode} = DexterUiContext.Theme.useContext();

  <img
    className={Css.style([Css.width(`px(size)), Css.height(`px(size))])}
    src={
      baseUrl
      ++ "icons/"
      ++ name
      ++ (darkMode ? darkModeSuffix |> Utils.orDefault("") : "")
      ++ ".svg"
    }
  />;
};
