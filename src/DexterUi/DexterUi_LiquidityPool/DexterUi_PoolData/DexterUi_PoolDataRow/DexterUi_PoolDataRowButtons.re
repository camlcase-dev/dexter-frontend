[@react.component]
let make = (~token: Dexter_ExchangeBalance.t) => {
  let {account}: DexterUiContext.Account.t =
    DexterUiContext.Account.useContext();

  let hasXtzBalance =
    switch (account) {
    | Some(account) => account.xtz |> Tezos_Mutez.gtZero
    | _ => false
    };

  let hasLqtBalance = token.lqtBalance |> Tezos_Token.gtZero;
  let hasTokenBalance = token.tokenBalance |> Tezos_Token.gtZero;

  <>
    <DexterUi_PoolDataRowButton
      disabled={!hasTokenBalance || !hasXtzBalance}
      hash={Dexter_Route.getTokensHash(
        Dexter_Balance.XtzBalance(Tezos_Mutez.zero),
        ExchangeBalance(token),
      )}
      route={Dexter_Route.LiquidityPool(AddLiquidity)}
    />
    <DexterUi_PoolDataRowButton
      disabled={!hasLqtBalance}
      hash={Dexter_Route.getTokensHash(
        Dexter_Balance.XtzBalance(Tezos_Mutez.zero),
        ExchangeBalance(token),
      )}
      route={Dexter_Route.LiquidityPool(RemoveLiquidity)}
    />
    <DexterUi_PoolDataRowButton
      disabled={!hasXtzBalance && !hasTokenBalance}
      hash={
        hasXtzBalance
          ? Dexter_Route.getTokensHash(
              Dexter_Balance.XtzBalance(Tezos_Mutez.zero),
              ExchangeBalance(token),
            )
          : Dexter_Route.getTokensHash(
              ExchangeBalance(token),
              Dexter_Balance.XtzBalance(Tezos_Mutez.zero),
            )
      }
      route=Dexter_Route.Exchange
    />
  </>;
};
