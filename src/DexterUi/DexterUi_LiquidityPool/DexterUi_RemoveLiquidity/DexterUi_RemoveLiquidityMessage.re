[@react.component]
let make = () => {
  let isLoggedIn = DexterUi_Hooks.useIsLoggedIn();
  let hasPositiveLqt = DexterUi_Hooks.useHasPositiveLqt();

  <>
    <Flex height={`px(10)} />
    {switch (!isLoggedIn, !hasPositiveLqt) {
     | (true, _) =>
       <DexterUi_Message title={"No wallet connected" |> React.string} />
     | (_, true) =>
       <DexterUi_Message
         title={"You do not have any Dexter pool tokens" |> React.string}
         subtitle=
           {<>
              {"Click the \'" |> React.string}
              <i
                className=Css.(
                  merge([
                    "icon-info",
                    style([
                      before([important(margin(`zero))]),
                      fontSize(px(12)),
                    ]),
                  ])
                )
              />
              {"\' icon above for more information." |> React.string}
            </>}
       />
     | _ => React.null
     }}
  </>;
};
