let calculateFromXtzValue =
    (token: Dexter_ExchangeBalance.t, xtzValue: Valid.t(Tezos.Mutez.t))
    : (Valid.t(Tezos.Token.t), Tezos.Token.t) => {
  switch (xtzValue) {
  | Valid(xtzValueValue, _) =>
    switch (
      Dexter_AddLiquidity.addLiquidityTokenIn(
        xtzValueValue,
        token.exchangeXtz,
        token.exchangeTokenBalance,
      )
    ) {
    | Some(tokenValue) => (
        Dexter_Value.getTokenValue(tokenValue),
        Dexter_AddLiquidity.lqtMinted(
          xtzValueValue,
          token.exchangeXtz,
          token.exchangeTotalLqt,
        ),
      )
    | _ => (InvalidInitialState, Tezos_Token.mkToken(Bigint.zero, 0))
    }

  | _ => (InvalidInitialState, Tezos_Token.mkToken(Bigint.zero, 0))
  };
};

let calculateFromTokenValue =
    (token: Dexter_ExchangeBalance.t, tokenValue: Valid.t(Tezos.Token.t))
    : (Valid.t(Tezos.Mutez.t), Tezos.Token.t) => {
  switch (tokenValue) {
  | Valid(tokenValueValue, _) =>
    switch (
      Dexter_AddLiquidity.addLiquidityXtzIn(
        tokenValueValue,
        token.exchangeXtz,
        token.exchangeTokenBalance,
      )
    ) {
    | Some(xtzValue) => (
        Valid(xtzValue, Tezos.Mutez.toTezString(xtzValue)),
        Dexter_AddLiquidity.lqtMinted(
          xtzValue,
          token.exchangeXtz,
          token.exchangeTotalLqt,
        ),
      )
    | _ => (InvalidInitialState, Tezos_Token.mkToken(Bigint.zero, 0))
    }
  | _ => (InvalidInitialState, Tezos_Token.mkToken(Bigint.zero, 0))
  };
};
