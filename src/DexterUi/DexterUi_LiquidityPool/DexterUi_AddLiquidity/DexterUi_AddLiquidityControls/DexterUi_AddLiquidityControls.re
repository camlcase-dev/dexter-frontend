[@react.component]
let make =
    (
      ~disabled: bool,
      ~hasPositiveLiquidity: bool,
      ~resetInputs: unit => unit,
      ~state: DexterUi_AddLiquidity_Reducer.state,
      ~xtz: Tezos.Mutez.t,
    ) => {
  let hasInsufficientXtz = DexterUi_Hooks.useHasInsufficientXtz();

  let isInvalid =
    DexterUi_AddLiquidity_Utils.checkIsInvalid(
      xtz,
      state.xtzValue,
      state.tokenValue,
      state.token,
    );

  <>
    {(!hasPositiveLiquidity || hasInsufficientXtz)
     |> Utils.renderIf(
          hasInsufficientXtz
            ? <DexterUi_InsufficientXtzMessage />
            : <DexterUi_AddLiquidityFirstAddMessage />,
        )}
    <DexterUi_AddLiquidityDescription disabled isInvalid state />
    <DexterUi_AddLiquidityButton
      disabled={disabled || isInvalid}
      resetInputs
      state
    />
  </>;
};
