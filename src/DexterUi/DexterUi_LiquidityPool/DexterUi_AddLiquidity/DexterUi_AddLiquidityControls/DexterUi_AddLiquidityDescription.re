[@react.component]
let make =
    (
      ~disabled: bool=false,
      ~isInvalid: bool,
      ~state: DexterUi_AddLiquidity_Reducer.state,
    ) =>
  <Flex flexDirection=`column alignItems=`center mt={`px(12)}>
    <DexterUi_Description>
      {switch (disabled, isInvalid, state.token) {
       | (true, _, _) =>
         <>
           {"You need two different types of Tezos tokens in your wallet to add liquidity."
            |> React.string}
           <br />
           {"Click the \'" |> React.string}
           <i
             className=Css.(
               merge([
                 "icon-info",
                 style([
                   before([important(margin(`zero))]),
                   fontSize(px(12)),
                 ]),
               ])
             )
           />
           {"\' icon above for more information." |> React.string}
         </>
       | (_, false, Some(token)) =>
         <>
           {"You are about to add "
            ++ (
              switch (state.xtzValue) {
              | Valid(m, _) => Tezos.Mutez.toTezStringWithCommas(m)
              | _ => "0"
              }
            )
            ++ " XTZ  and "
            ++ (
              switch (state.tokenValue) {
              | Valid(m, _) => Tezos.Token.toStringWithCommas(m)
              | _ => "0"
              }
            )
            ++ " "
            ++ token.symbol
            ++ " to the liquidity pool to receive "
            ++ Tezos.Token.toStringWithCommas(state.liquidity)
            ++ " "
            |> React.string}
           <DexterUi_PoolTokenSymbol token />
           {" pool tokens. You may redeem your pool tokens later by removing liquidity from the pool to receive your share of XTZ and "
            ++ token.symbol
            ++ " tokens."
            |> React.string}
         </>
       | _ => React.null
       }}
    </DexterUi_Description>
  </Flex>;
