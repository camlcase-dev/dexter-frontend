[@react.component]
let make =
    (
      ~slippage: Dexter_Slippage.t,
      ~setSlippage: (Dexter_Slippage.t => Dexter_Slippage.t) => unit,
    ) => {
  let (showAdvancedOptions, setShowAdvancedOptions) =
    React.useState(_ => false);

  <Flex mt={`px(10)} justifyContent=`center position=`relative>
    <Flex onClick={_ => setShowAdvancedOptions(_ => true)}>
      <Text
        darkModeColor=Colors.primaryDark
        lightModeColor=Colors.blue
        textDecoration=`underline>
        {"Advanced options" |> React.string}
      </Text>
    </Flex>
    {showAdvancedOptions
     |> Utils.renderIf(
          <DexterUi_ExchangeAdvancedOptionsModal
            slippage
            setSlippage
            setShowAdvancedOptions
          />,
        )}
  </Flex>;
};
