[@react.component]
let make = (~title: React.element, ~tooltipText: React.element) => {
  <Flex alignItems=`center>
    <Text color=Colors.blackish1 textStyle=TextStyles.bold> title </Text>
    <Flex ml={`px(4)}>
      <DexterUi_Tooltip text=tooltipText width={`px(240)}>
        <Flex mb={`px(-1)}>
          <Text fontSize={`px(10)} color=Colors.darkGrey2>
            <i className="icon-info" />
          </Text>
        </Flex>
      </DexterUi_Tooltip>
    </Flex>
  </Flex>;
};
