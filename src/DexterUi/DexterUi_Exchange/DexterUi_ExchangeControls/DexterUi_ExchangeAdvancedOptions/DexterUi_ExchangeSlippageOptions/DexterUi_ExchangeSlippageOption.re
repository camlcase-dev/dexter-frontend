[@react.component]
let make =
    (
      ~option: Dexter_Slippage.t,
      ~slippage: Dexter_Slippage.t,
      ~setSlippage: (Dexter_Slippage.t => Dexter_Slippage.t) => unit,
    ) => {
  let isSelected = option === slippage;
  let optionText =
    (option |> Dexter_Slippage.toString)
    ++ (option === Half ? " (default)" : "");

  <Flex
    onClick={_ => setSlippage(_ => option)}
    background={isSelected ? Colors.white : Colors.lightGrey1}
    height={`px(22)}
    alignItems=`center
    px={`px(8)}
    mr={`px(8)}
    borderRadius={`px(11)}>
    <Text
      className=Css.(
        style([
          before([
            display(`block),
            contentRule(`text(optionText)),
            fontWeight(`bold),
            height(`zero),
            overflow(`hidden),
            visibility(`hidden),
          ]),
        ])
      )
      color={isSelected ? Colors.blue : Colors.grey}
      fontWeight={isSelected ? `semiBold : `normal}
      whiteSpace=`nowrap>
      {optionText |> React.string}
    </Text>
  </Flex>;
};
