open DexterUi_ExchangeControls_Utils;

[@react.component]
let make =
    (~state: DexterUi_Exchange_Reducer.state, ~resetInputs: unit => unit) => {
  let {account}: DexterUiContext.Account.t =
    DexterUiContext.Account.useContext();
  let (sendTo, setSendTo) = React.useState(_ => false);
  let (sendToAddress, setSendToAddress) = React.useState(_ => "");
  let (slippage, setSlippage) =
    React.useState(_ => Dexter_LocalStorage.MaximumSlippage.getSlippage());
  let (priceImpact, setPriceImpact) = React.useState(_ => 0.);
  let (priceImpactConfirmed, setPriceImpactConfirmed) =
    React.useState(_ => false);

  let setSendTo =
    React.useCallback0(sendTo => {
      !sendTo ? setSendToAddress(_ => "") : ();
      setSendTo(_ => sendTo);
    });

  let resetInputs =
    React.useCallback0(() => {
      setSendTo(false);
      resetInputs();
    });

  /* monitor slippage choice */
  React.useEffect1(
    () => {
      Dexter_LocalStorage.MaximumSlippage.setFromSlippage(slippage);
      None;
    },
    [|slippage|],
  );

  /* monitor input values to calculate the slippage rate */
  React.useEffect3(
    () => {
      setPriceImpact(_ =>
        getPriceImpact(
          state.inputValue,
          state.inputToken,
          state.outputValue,
          state.outputToken,
        )
      );

      setPriceImpactConfirmed(_ => false);

      None;
    },
    (state.inputValue, state.inputToken, state.outputToken),
  );

  let inputsInvalid =
    checkInputsInvalid(
      account,
      state.inputValue,
      state.inputToken,
      state.outputValue,
      state.outputToken,
    );

  let accountWalletAddress =
    account
    |> Utils.map((account: Dexter_Account.t) =>
         Tezos.Address.toString(account.address)
       )
    |> Utils.orDefault("");

  let invalidAddress =
    sendTo
    && (
      sendToAddress
      |> Tezos.Address.ofString
      |> Belt.Result.isError
      || sendToAddress === accountWalletAddress
    );

  let hasInsufficientXtz = DexterUi_Hooks.useHasInsufficientXtz();

  let descriptionHidden =
    inputsInvalid || invalidAddress || hasInsufficientXtz || priceImpact > 50.0;

  let exceedsMaximumPriceImpact =
    priceImpact > 5. && !priceImpactConfirmed || priceImpact > 50.0;

  let disabled = exceedsMaximumPriceImpact || inputsInvalid || invalidAddress;

  let priceImpactHidden =
    switch (state.inputValue) {
    | Valid(value, _) when InputType.gtZero(value) => false
    | _ => true
    };

  <Flex flexGrow=1. flexDirection=`column alignItems=`center>
    <DexterUi_ExchangeRate
      inputValue={state.inputValue}
      inputToken={state.inputToken}
      outputToken={state.outputToken}
    />
    <DexterUi_ExchangeAdvancedOptions slippage setSlippage />
    {Dexter_Settings.enableExchangeAndSend
     |> Utils.renderIf(
          <DexterUi_ExchangeSendToAddress
            invalidAddress
            outputToken={state.outputToken}
            sendTo
            setSendTo
            sendToAddress
            setSendToAddress
          />,
        )}
    <Flex mt={`px(8)} />
    {hasInsufficientXtz
       ? <DexterUi_InsufficientXtzMessage />
       : <DexterUi_ExchangePriceImpact
           isHidden=priceImpactHidden
           priceImpact
           priceImpactConfirmed
           setPriceImpactConfirmed
         />}
    <DexterUi_ExchangeDescription descriptionHidden slippage state />
    <DexterUi_ExchangeButton disabled state sendTo sendToAddress resetInputs />
  </Flex>;
};
