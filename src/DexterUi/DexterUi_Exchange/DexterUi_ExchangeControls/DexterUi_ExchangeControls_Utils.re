let checkInputsInvalid =
    (
      account: option(Dexter_Account.t),
      inputValue: Valid.t(InputType.t),
      inputToken: Dexter_Balance.t,
      outputValue: Valid.t(InputType.t),
      outputToken: Dexter_Balance.t,
    ) => {
  switch (account, inputValue, outputValue) {
  | (Some(account), Valid(inputValue, _), Valid(outputValue, _)) =>
    switch (inputValue, inputToken, outputValue, outputToken) {
    | (
        Mutez(inputValue),
        XtzBalance(_),
        Token(outputValue),
        ExchangeBalance(outputToken),
      ) =>
      Tezos.Mutez.leqZero(inputValue)
      || Tezos.Token.leqZero(outputValue)
      || Tezos.Mutez.(gt(inputValue, userMax(account.xtz)))
      || Tezos.Token.gt(outputValue, outputToken.exchangeTokenBalance)
    /* maxXto token */
    | (
        Token(inputValue),
        ExchangeBalance(inputToken),
        Mutez(outputValue),
        XtzBalance(_),
      ) =>
      Tezos.Token.leqZero(inputValue)
      || Tezos.Mutez.leqZero(outputValue)
      || Tezos.Token.gt(inputValue, inputToken.tokenBalance)
      || Tezos.Mutez.gt(outputValue, inputToken.exchangeXtz)
    /* token to maxX*/
    | (
        Token(inputValue),
        ExchangeBalance(inputToken),
        Token(outputValue),
        ExchangeBalance(outputToken),
      ) =>
      Tezos.Token.leqZero(inputValue)
      || Tezos.Token.leqZero(outputValue)
      || Tezos.Token.(gt(inputValue, inputToken.tokenBalance))
      || Tezos.Token.gt(outputValue, outputToken.exchangeTokenBalance)
    | _ => true /* you cannot trade when both are XTZ */
    }

  | _ => true
  };
};

let getPriceImpact =
    (
      inputValue: Valid.t(InputType.t),
      inputToken: Dexter_Balance.t,
      outputValue: Valid.t(InputType.t),
      outputToken: Dexter_Balance.t,
    ) =>
  (
    switch (inputValue, outputValue) {
    | (Valid(Mutez(inputValue), _), Valid(Token(_), _)) =>
      switch (outputToken) {
      | ExchangeBalance(outputToken) =>
        Dexter_Exchange.xtzToTokenPriceImpactForDisplay(
          inputValue,
          outputToken.exchangeXtz,
          outputToken.exchangeTokenBalance,
        )
      | _ => None
      }

    | (Valid(Token(inputValue), _), Valid(Mutez(_), _)) =>
      switch (inputToken) {
      | ExchangeBalance(inputToken) =>
        Dexter_Exchange.tokenToXtzPriceImpactForDisplay(
          inputValue,
          inputToken.exchangeXtz,
          inputToken.exchangeTokenBalance,
        )
      | _ => None
      }
    | (Valid(Token(inputValue), _), Valid(Token(_), _)) =>
      switch (inputToken, outputToken) {
      | (ExchangeBalance(inputToken), ExchangeBalance(outputToken)) =>
        Dexter_Exchange.tokenToTokenPriceImpactForDisplay(
          inputValue,
          inputToken.exchangeXtz,
          inputToken.exchangeTokenBalance,
          outputToken.exchangeXtz,
          outputToken.exchangeTokenBalance,
        )
      | _ => None
      }
    | _ => None
    }
  )
  |> Utils.orDefault(0.);
