/**
 * When the user provides an input value (what they sell to Dexter),
 * this calculates the output value (what they receive/buy from Dexter).
 */

let calculateInput =
    (
      inputToken: Dexter.Balance.t,
      outputToken: Dexter.Balance.t,
      outputValue: Valid.t(InputType.t),
    )
    : Valid.t(InputType.t) => {
  switch (inputToken, outputToken, outputValue) {
  // input is XTZ, output is token
  | (
      XtzBalance(_),
      ExchangeBalance(outputToken),
      Valid(Token(outputValue), _),
    ) =>
    let {exchangeXtz, exchangeTokenBalance}: Dexter.ExchangeBalance.t = outputToken;

    switch (
      Dexter.Exchange.xtzToTokenXtzInput(
        outputValue,
        exchangeXtz,
        exchangeTokenBalance,
      )
    ) {
    | Some(inputValue) => Dexter_Value.getMutezInputValue(inputValue)
    | _ => InvalidInitialState
    };
  | (ExchangeBalance(inputToken), _, Valid(Mutez(xtzOut), _)) =>
    // input is token, output is XTZ
    let {exchangeXtz, exchangeTokenBalance}: Dexter.ExchangeBalance.t = inputToken;
    switch (
      Dexter.Exchange.tokenToXtzTokenInput(
        xtzOut,
        exchangeXtz,
        exchangeTokenBalance,
      )
    ) {
    | Some(inputValue) => Dexter_Value.getTokenInputValue(inputValue)
    | _ => InvalidInitialState
    };
  | (
      ExchangeBalance(inputToken),
      ExchangeBalance(outputToken),
      Valid(Token(tokenOut), _),
    ) =>
    // input is token, output is token
    switch (
      Dexter.Exchange.tokenToTokenTokenInput(
        tokenOut,
        inputToken.exchangeXtz,
        inputToken.exchangeTokenBalance,
        outputToken.exchangeXtz,
        outputToken.exchangeTokenBalance,
      )
    ) {
    | Some(inputValue) => Dexter_Value.getTokenInputValue(inputValue)
    | _ => InvalidInitialState
    }

  | _ => InvalidInitialState
  };
};

let calculateOutput =
    (
      inputToken: Dexter.Balance.t,
      inputValue: Valid.t(InputType.t),
      outputToken: Dexter.Balance.t,
    )
    : Valid.t(InputType.t) => {
  switch (inputToken, inputValue, outputToken) {
  // input is XTZ, output is token
  | (
      XtzBalance(_),
      Valid(Mutez(inputValue), _),
      ExchangeBalance(outputToken),
    ) =>
    let {exchangeXtz, exchangeTokenBalance}: Dexter.ExchangeBalance.t = outputToken;

    switch (
      Dexter.Exchange.xtzToTokenTokenOutput(
        inputValue,
        exchangeXtz,
        exchangeTokenBalance,
      )
    ) {
    | Some(outputValue) => Dexter_Value.getTokenInputValue(outputValue)
    | _ => InvalidInitialState
    };
  // input is token, output is XTZ
  | (
      ExchangeBalance(inputToken),
      Valid(Token(inputValue), _),
      XtzBalance(_),
    ) =>
    let {exchangeXtz, exchangeTokenBalance}: Dexter.ExchangeBalance.t = inputToken;

    switch (
      Dexter.Exchange.tokenToXtzXtzOutput(
        inputValue,
        exchangeXtz,
        exchangeTokenBalance,
      )
    ) {
    | Some(outputValue) => Dexter_Value.getMutezInputValue(outputValue)
    | _ => InvalidInitialState
    };
  | (
      ExchangeBalance(inputToken),
      Valid(Token(inputValue), _),
      ExchangeBalance(outputToken),
    ) =>
    switch (
      Dexter.Exchange.tokenToTokenTokenOutput(
        inputValue,
        inputToken.exchangeXtz,
        inputToken.exchangeTokenBalance,
        outputToken.exchangeXtz,
        outputToken.exchangeTokenBalance,
      )
    ) {
    | Some(outputValue) => Dexter_Value.getTokenInputValue(outputValue)
    | _ => InvalidInitialState
    }

  | _ => InvalidInitialState
  };
};

let useInitialTokens = (balances: list(Dexter_Balance.t)) => {
  let (t1, t2) = DexterUi_Hooks.useTokensFromLocalStorage(balances);
  let xtzBalance = balances |> Dexter.Balance.getXtz;
  let firstTokenBalance = balances |> Dexter.Balance.getFirstToken;

  switch (t1, t2) {
  | (Some(t1), Some(t2)) =>
    switch (t1, t2) {
    | (ExchangeBalance(eb1), ExchangeBalance(eb2)) when eb1.name === eb2.name => (
        t1,
        xtzBalance,
      )
    | (XtzBalance(_), XtzBalance(_)) => (t1, firstTokenBalance)
    | _ => (t1, t2)
    }
  | (Some(t1), None) => (
      t1,
      switch (t1) {
      | ExchangeBalance(_) => xtzBalance
      | XtzBalance(_) => firstTokenBalance
      },
    )
  | (None, Some(t2)) => (
      switch (t2) {
      | ExchangeBalance(_) => xtzBalance
      | XtzBalance(_) => firstTokenBalance
      },
      t2,
    )
  | _ => (
      t1 |> Utils.orDefault(xtzBalance),
      t2 |> Utils.orDefault(firstTokenBalance),
    )
  };
};
