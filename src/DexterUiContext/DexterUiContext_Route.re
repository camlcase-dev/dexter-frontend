type t = {
  route: Dexter_Route.t,
  setRoute: (Dexter_Route.t, option(string)) => unit,
};

let defaultValue = {route: Dexter_Route.Exchange, setRoute: (_, _) => ()};

let ctx = React.createContext(defaultValue);

module ProviderInternal = {
  let makeProps = (~value, ~children, ()) => {
    "value": value,
    "children": children,
  };

  let make = React.Context.provider(ctx);
};

module Provider = {
  [@react.component]
  let make = (~children: React.element) => {
    let baseUrl = Utils.useBaseUrl();
    let (path, hash) = Utils.useUrl();
    let (route, setRoute) =
      React.useState(_ => path |> Dexter_Route.fromPath);

    React.useEffect1(
      () => {
        if (path |> Dexter_Route.fromPath !== route) {
          setRoute(_ => path |> Dexter_Route.fromPath);
        };
        None;
      },
      [|path |> Dexter_Route.fromPath |> Dexter_Route.toUrl|],
    );

    React.useEffect1(
      () => {
        if (hash !== "") {
          Dexter_LocalStorage.SelectedTokens.set(hash);
        };
        None;
      },
      [|hash|],
    );

    let setRoute =
      React.useCallback((route, hash: option(string)) => {
        baseUrl
        ++ (route |> Dexter_Route.toUrl)
        ++ {
          switch (hash) {
          | Some(hash) => {j|#$hash|j}
          | _ => ""
          };
        }
        |> ReasonReactRouter.push
      });

    <ProviderInternal value={route, setRoute}> children </ProviderInternal>;
  };
};

let useContext = (): t => React.useContext(ctx);
