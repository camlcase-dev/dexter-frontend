type t = {
  currencies: list(Currency.t),
  currentCurrency: option(Currency.t),
  setCurrentCurrency: Currency.t => unit,
};

let defaultValue: t = {
  currencies: [],
  currentCurrency: None,
  setCurrentCurrency: _ => (),
};

let ctx = React.createContext(defaultValue);

module ProviderInternal = {
  let makeProps = (~value, ~children, ()) => {
    "value": value,
    "children": children,
  };

  let make = React.Context.provider(ctx);
};

module Provider = {
  [@react.component]
  let make = (~children: React.element) => {
    let (currencies, setCurrencies) = React.useState(_ => []);
    let (currentCurrency, setCurrentCurrency) =
      React.useState(_ =>
        Currency_Dict.list
        |> Currency.findBySymbolOrDefault(Dexter_LocalStorage.Currency.get())
      );

    React.useEffect0(() => {
      CoinGecko.fetch()
      |> Js.Promise.then_(res => {
           switch (res) {
           | Belt.Result.Ok((currencies: list(Currency.t))) =>
             setCurrencies(_ => currencies);
             setCurrentCurrency(_ =>
               currencies
               |> Currency.findBySymbolOrDefault(
                    Dexter_LocalStorage.Currency.get(),
                  )
             );
             Js.Promise.resolve();
           | Belt.Result.Error(msg) =>
             DexterUiContext_Services.emitEvent(
               ~service=Service.CoinGecko,
               (),
             );
             Js.log("Unable to retrieve exchange rate from CoinGecko");
             Js.log(msg);
             Js.Promise.resolve();
           }
         })
      |> ignore;
      None;
    });

    let setCurrentCurrency = (currency: Currency.t) => {
      Dexter_LocalStorage.Currency.set(currency.symbol);
      setCurrentCurrency(_ => Some(currency));
    };

    <ProviderInternal value={currencies, currentCurrency, setCurrentCurrency}>
      children
    </ProviderInternal>;
  };
};

let useContext = () => React.useContext(ctx);
