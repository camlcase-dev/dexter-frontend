let transactionStatusPollingTimeoutId = ref(None);

// poll the latest transaction status and update transactionType
let useTransactionStatusPolling =
    (
      account: option(Dexter_Account.t),
      refetchAccount: bool => unit,
      transactions: option(list(Dexter_Transaction.t)),
      setTransactions: option(list(Dexter_Transaction.t)) => unit,
      setTransactionStatus: TZKT.Status.t => unit,
    )
    : unit => {
  React.useEffect1(
    () => {
      switch (transactionStatusPollingTimeoutId^) {
      | Some(timeoutId) => timeoutId |> Js.Global.clearTimeout
      | _ => ()
      };

      switch (transactions) {
      | Some(transactions) when transactions |> List.length > 0 =>
        let latestTransaction = List.nth(transactions, 0);

        if (latestTransaction.status === Pending) {
          setTransactionStatus(latestTransaction.status);
        };

        let rec getLatestTransactionStatusRec = () =>
          latestTransaction
          |> Dexter_Transaction.getTransactionStatus
          |> Js.Promise.then_(((status, oTransactionType)) => {
               setTransactionStatus(status);
               if (latestTransaction.status !== status) {
                 let transactionType =
                   Belt.Option.getWithDefault(
                     oTransactionType,
                     latestTransaction.transactionType,
                   );

                 setTransactions(
                   Some([
                     {...latestTransaction, status, transactionType},
                     ...transactions
                        |> Array.of_list
                        |> Js.Array.sliceFrom(1)
                        |> Array.to_list,
                   ]),
                 );
                 if (status !== Pending) {
                   if (status === Applied) {
                     refetchAccount(true);
                   };
                   Dexter_LocalStorage.Transaction.removeByHash(
                     latestTransaction.hash,
                   );
                 };
               };

               if (status === Pending) {
                 transactionStatusPollingTimeoutId :=
                   Some(
                     Js.Global.setTimeout(
                       getLatestTransactionStatusRec,
                       10000,
                     ),
                   );
               };
               Js.Promise.resolve();
             })
          |> Js.Promise.catch(error => {
               Js.log(error);
               Js.Promise.resolve();
             })
          |> ignore;

        if (latestTransaction.status === Pending) {
          getLatestTransactionStatusRec();
        };
      | _ => ()
      };

      transactionStatusPollingTimeoutId^
      |> Utils.map((timeoutId, ()) => timeoutId |> Js.Global.clearTimeout);
    },
    [|transactions|],
  );

  let hasAccount = account |> Belt.Option.isSome;

  React.useEffect1(
    () => {
      switch (account) {
      | Some(account) =>
        TZKT.Operation.fetch(account.address)
        |> Js.Promise.then_(result =>
             switch (result) {
             | Belt.Result.Ok(operations) =>
               let transactions =
                 Dexter_Transaction.ofQuery(
                   account.address,
                   Dexter_Settings.exchanges,
                   operations,
                 )
                 |> List.sort(
                      (tr0: Dexter_Transaction.t, tr1: Dexter_Transaction.t) =>
                      tr0.timestamp < tr1.timestamp
                        ? 1 : tr0.timestamp > tr1.timestamp ? (-1) : 0
                    );

               let latestTransaction =
                 Dexter_LocalStorage.Transaction.getByWalletAddress(
                   account.address,
                 );

               switch (latestTransaction) {
               | Some(latestTransaction) =>
                 switch (
                   transactions
                   |> List.find_opt((transaction: Dexter_Transaction.t) =>
                        transaction.hash === latestTransaction.hash
                      )
                 ) {
                 | Some(_) =>
                   let latestTransactionFromTzkt = List.nth(transactions, 0);
                   if (latestTransactionFromTzkt.hash
                       === latestTransaction.hash) {
                     switch (latestTransactionFromTzkt.timestamp) {
                     | Timestamp(moment)
                         when
                           MomentRe.diff(
                             MomentRe.momentNow(),
                             moment,
                             `minutes,
                           )
                           <= 60. =>
                       setTransactionStatus(latestTransactionFromTzkt.status)
                     | _ => ()
                     };
                   };
                   setTransactions(Some(transactions));
                   Dexter_LocalStorage.Transaction.removeByHash(
                     latestTransaction.hash,
                   );
                 | _ =>
                   setTransactions(
                     Some([latestTransaction, ...transactions]),
                   )
                 }
               | _ => setTransactions(Some(transactions))
               };

               Js.Promise.resolve();
             | Belt.Result.Error(msg) =>
               Js.log("Unable to retrieve operations from TZKT");
               Js.log(msg);
               Js.Promise.resolve();
             }
           )
        |> ignore
      | None =>
        setTransactions(None);
        setTransactionStatus(Ready);
      };
      None;
    },
    [|hasAccount|],
  );
};
