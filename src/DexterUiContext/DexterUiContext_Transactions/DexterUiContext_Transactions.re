type t = {
  transactions: option(list(Dexter_Transaction.t)),
  pushTransaction: Dexter_Transaction.t => unit,
  transactionStatus: TZKT.Status.t,
  setTransactionStatus: TZKT.Status.t => unit,
  transactionTimeout: int,
  setTransactionTimeout: int => unit,
};

let defaultValue = {
  transactions: None,
  pushTransaction: _ => (),
  transactionStatus: Ready,
  setTransactionStatus: _ => (),
  transactionTimeout: 20,
  setTransactionTimeout: _ => (),
};

let ctx = React.createContext(defaultValue);

module ProviderInternal = {
  let makeProps = (~value, ~children, ()) => {
    "value": value,
    "children": children,
  };

  let make = React.Context.provider(ctx);
};

module Provider = {
  [@react.component]
  let make = (~children: React.element) => {
    let {account, refetchAccount} = DexterUiContext_Account.useContext();
    let (transactions, setTransactions) = React.useState(_ => None);
    let (transactionStatus, setTransactionStatus) =
      React.useState(_ => TZKT.Status.Ready);
    let (transactionTimeout, setTransactionTimeout) =
      React.useState(_ => Dexter_LocalStorage.TransactionTimeout.get());

    let setTransactions =
      React.useCallback(transactions => setTransactions(_ => transactions));

    // necessary check?
    let setTransactionStatus =
      React.useCallback(newTransactionStatus =>
        if (newTransactionStatus !== transactionStatus) {
          setTransactionStatus(_ => newTransactionStatus);
        }
      );

    let pushTransaction =
      React.useCallback((transaction: Dexter_Transaction.t) => {
        Dexter_LocalStorage.Transaction.push(transaction);
        setTransactionStatus(Pending);
        setTransactions(
          switch (transactions) {
          | Some(transactions) => Some([transaction, ...transactions])
          | _ => Some([transaction])
          },
        );
      });

    let setTransactionTimeout =
      React.useCallback(transactionTimeout => {
        Dexter_LocalStorage.TransactionTimeout.set(transactionTimeout);
        setTransactionTimeout(_ => transactionTimeout);
      });

    DexterUiContext_Transactions_Utils.useTransactionStatusPolling(
      account,
      refetchAccount,
      transactions,
      setTransactions,
      setTransactionStatus,
    );

    <ProviderInternal
      value={
        transactions,
        pushTransaction,
        transactionStatus,
        setTransactionStatus,
        transactionTimeout,
        setTransactionTimeout,
      }>
      children
    </ProviderInternal>;
  };
};

let useContext = (): t => React.useContext(ctx);
