let eventName = "serviceUnavailable";

let encodeEventPayload = (service: Service.t, serviceUnavailable: bool) =>
  (service, serviceUnavailable)
  |> Json.Encode.tuple2(Service.encode, Json.Encode.bool);

let decodeEventPayload = json =>
  switch (json |> Json.Decode.tuple2(Service.decode, Json.Decode.bool)) {
  | (service, isUnavailable) => (service, isUnavailable)
  };

let emitEvent = (~service: Service.t, ~isUnavailable: bool=true, ()) =>
  Event.createEvent(
    eventName,
    Some(encodeEventPayload(service, isUnavailable)),
  )
  |> Event.emit;

type t = array(Service.t);

let ctx = React.createContext([||]);

module ProviderInternal = {
  let makeProps = (~value, ~children, ()) => {
    "value": value,
    "children": children,
  };

  let make = React.Context.provider(ctx);
};

module Provider = {
  [@react.component]
  let make = (~children: React.element) => {
    let (unavailableServices, setServices) = React.useState(_ => [||]);

    React.useEffect0(() => {
      let listener =
        Event.on(eventName, ev =>
          setServices(unavailableServices =>
            switch (Event.getPayload(ev, decodeEventPayload)) {
            | Some((service, true))
                when !(unavailableServices |> Js.Array.includes(service)) =>
              Array.concat([unavailableServices, [|service|]])
            | Some((service, false))
                when unavailableServices |> Js.Array.includes(service) =>
              unavailableServices
              |> Js.Array.filter(unavailableService =>
                   unavailableService !== service
                 )
            | _ => unavailableServices
            }
          )
        );

      Some(() => Event.off(eventName, listener));
    });

    <ProviderInternal value=unavailableServices> children </ProviderInternal>;
  };
};

let useContext = (): t => React.useContext(ctx);

let exist = () => {
  let unavailableServices = useContext();

  React.useMemo1(
    () => unavailableServices |> Js.Array.length > 0,
    [|unavailableServices|],
  );
};

let isUnavailable = (service: Service.t) => {
  let unavailableServices = useContext();

  React.useMemo1(
    () => unavailableServices |> Js.Array.includes(service),
    [|unavailableServices|],
  );
};

let areTransactionsDisable = () => {
  let unavailableServices = useContext();

  React.useMemo1(
    () =>
      unavailableServices
      |> Js.Array.includes(Service.BakingBad)
      || unavailableServices
      |> Js.Array.includes(Service.TezosNode),
    [|unavailableServices|],
  );
};
