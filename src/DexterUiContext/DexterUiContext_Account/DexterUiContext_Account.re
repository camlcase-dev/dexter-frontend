type t = {
  connectToBeacon: unit => unit,
  account: option(Dexter_Account.t),
  refetchAccount: bool => unit,
  disconnect: unit => unit,
  balances: list(Dexter_Balance.t),
};

let defaultValue = {
  connectToBeacon: () => (),
  account: None,
  refetchAccount: _ => (),
  disconnect: () => (),
  balances: [],
};

let ctx = React.createContext(defaultValue);

module ProviderInternal = {
  let makeProps = (~value, ~children, ()) => {
    "value": value,
    "children": children,
  };

  let make = React.Context.provider(ctx);
};

module Provider = {
  [@react.component]
  let make = (~children: React.element) => {
    let (account, setAccount) = React.useState(_ => None);
    let (balances, setBalances) = React.useState(_ => []);

    let setAccount = React.useCallback(account => setAccount(_ => account));

    let setBalances =
      React.useCallback(balances => setBalances(_ => balances));

    let connectToBeacon =
      React.useCallback(() =>
        DexterUiContext_Account_Utils.connectToBeacon(setAccount, setBalances)
        |> ignore
      );

    let disconnect = () => {
      switch (account) {
      | Some(account) =>
        switch (account.wallet) {
        | Beacon(dappClient) => Beacon.disconnect(dappClient) |> ignore
        }
      | _ => ()
      };

      Dexter_LocalStorage.WalletType.remove();
      setAccount(None);

      let disconnectedBalances: list(Dexter_Balance.t) =
        balances
        |> List.map(balance =>
             switch ((balance: Dexter_Balance.t)) {
             | XtzBalance(_) => Dexter_Balance.XtzBalance(Tezos.Mutez.zero)
             | ExchangeBalance(token) =>
               Dexter_Balance.ExchangeBalance({
                 ...token,
                 tokenBalance: Tezos.Token.zero,
                 lqtBalance: Tezos.Token.zero,
               })
             }
           );
      setBalances(disconnectedBalances);
    };

    DexterUiContext_Account_Utils.useSetup(
      connectToBeacon,
      setBalances,
    );

    let refetchAccount =
      React.useCallback1(
        bypassCache => {
          DexterUiContext_Account_Utils.refetchAccount(
            ~account,
            ~setAccount,
            ~setBalances,
            ~bypassCache,
            (),
          )
        },
        [|account|],
      );

    DexterUiContext_Account_Utils.useAccountPolling(account, refetchAccount);

    <ProviderInternal
      value={
        connectToBeacon,
        account,
        refetchAccount,
        disconnect,
        balances,
      }>
      children
    </ProviderInternal>;
  };
};

let useContext = (): t => React.useContext(ctx);
